<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Post;

class BlogController extends Controller
{
    public function getBlog(){
        $data['posts'] = Post::orderBy('id', 'ASC')->paginate(6);
        return view('user.elements.blog.index', $data);
    }

    public function getDetail($id){
        $data['post'] = Post::findOrFail($id);
        return view('user.elements.blog.detail', $data);
    }
}
