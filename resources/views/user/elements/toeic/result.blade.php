@extends('user.master')

@section('css')
    <link rel="stylesheet" href="assets/css/test.css"/>
@endsection


@section('content')
    
<main class="">
    <!--? slider Area Start-->
    <section class="slider-area slider-area2">
        <div class="slider-active">
            <!-- Single Slider -->
            <div class="single-slider slider-height2">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-8 col-lg-11 col-md-12">
                            <div class="hero__caption hero__caption2">
                                <h1 data-animation="bounceIn" data-delay="0.2s">Result</h1>
                                <!-- breadcrumb Start-->
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                                        <li class="breadcrumb-item"><a href="/toeic">Toeic</a></li>
                                        <li class="breadcrumb-item"><a href="/#">Test</a></li>
                                    </ol>
                                </nav>
                                <!-- breadcrumb End -->
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--? Blog Area Start -->
    <div class="section-top-border container result" style="text-align: center">      
            <h2>Chúc mừng bạn đã hoàn thành bài thi</h2>
            Hiện tại bọn mình đang trong quá trình phát triển, các bạn có thể giành 3 phút cho bọn mình xin ý kiến đánh giá được không ạ <br>
            <a href="https://forms.gle/ttb7P1AiQog3p5Qh7" style="color: red; font-size: 23px"> <i class="fas fa-hand-point-right"></i> --Link đánh giá --  </a>
            <br>
            <table  class="progress-table progress-table-wrap">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Number</th>
                        <th>Point</th>
                        <th>Total</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Listening</td>
                        <td>{{$ans_ls}}</td>
                        <td>{{$point_ls}}</td>
                        <td colspan="2">{{$point_ls + $point_re}}</td>
                    </tr>
                    <tr>
                        <td>Reading</td>
                        <td>{{$ans_re}}</td>
                        <td>{{$point_re}}</td>
                    </tr>
                </tbody>
            </table>
            
     
    </div>
</main>

@endsection


@section('script')
    <script src="https://cdn.jsdelivr.net/gh/mathusummut/confetti.js/confetti.min.js"></script>
    <script>
        confetti.start(8000, 50, 150);

    </script>
@endsection
