<?php

namespace App\Repositories\Admin\Aptis;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Models\Aptis\Test;


/**
 * Class TestRepository.
 */
class TestRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Test::class;
    }

    public function create($data){
        Test::create($data);
        return true;
    }

    public function update($data, $id){
        $test = self::getById($id);
        $test->update($data);
        return true;
    }
}
