<?php

namespace App\Models\Aptis;

use Illuminate\Database\Eloquent\Model;

class Read extends Model
{
    protected $table = 'aptis_read';

    protected $guarded = [
        'id'
    ];

    public function test() {
    	return $this -> hasMany('App\Models\Aptis\Test','read_id','id');
    }
}
