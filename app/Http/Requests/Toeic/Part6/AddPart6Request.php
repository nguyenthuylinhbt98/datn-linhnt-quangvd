<?php

namespace App\Http\Requests\Toeic\Part6;

use Illuminate\Foundation\Http\FormRequest;

class AddPart6Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'name'=>'required|unique:toeic_part6,name',
        ];
    }
    public function messages()
    {
        return [
            'name.required'=>'Tên Part6 không được để trống!',
            'name.unique'=>'Tên Part6 đã tồn tại!',
        ];
    }
}
