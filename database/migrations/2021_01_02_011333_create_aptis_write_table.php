<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAptisWriteTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aptis_write', function (Blueprint $table) {
            $table->id();
            $table->string('name', 50)->nullable();
            $table->longText('p1_ques1')->nullable();
            $table->longText('p1_ques2')->nullable();
            $table->longText('p1_ques3')->nullable();
            $table->longText('p1_ques4')->nullable();
            $table->longText('p1_ques5')->nullable();
            $table->longText('p2')->nullable();
            $table->longText('p3_subject')->nullable();
            $table->longText('p3_ques1')->nullable();
            $table->longText('p3_ques2')->nullable();
            $table->longText('p3_ques3')->nullable();
            $table->longText('p4_subject')->nullable();
            $table->longText('p4_ques1')->nullable();
            $table->longText('p4_ques2')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aptis_write_');
    }
}
