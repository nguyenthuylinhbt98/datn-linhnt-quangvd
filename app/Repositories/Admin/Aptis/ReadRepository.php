<?php

namespace App\Repositories\Admin\Aptis;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Models\Aptis\Read;


/**
 * Class ReadRepository.
 */
class ReadRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Read::class;
    }

    public function createRead($data){
        for ($i=1; $i < 9; $i++) {
            if(!empty($data['p1_ques'.$i])){
                $data['p1_ques'.$i]=implode('; ;',(array)$data['p1_ques'.$i]);
            };
        }
        for ($i=1; $i < 3; $i++) {
            if(!empty($data['p2_ques'.$i])){
                $data['p2_ques'.$i]=implode('; ;',(array)$data['p2_ques'.$i]);
            };
        }
        $data['p3_ans']=implode('; ;',(array)$data['p3_ans']);
        $data['p3_ques']=implode('; ;',(array)$data['p3_ques']);
        $data['p4_ans']=implode('; ;',(array)$data['p4_ans']);
        Read::create($data);        
        return true;
    }

    public function editRead($id)
    {
        $read = self::getById($id);
        for ($i=1; $i < 9; $i++) {
            (array)$read['p1_ques'.$i]=explode('; ;',$read->{'p1_ques'.$i});
        }
        for ($i=1; $i < 3; $i++) {
            (array)$read['p2_ques'.$i]=explode('; ;',$read->{'p2_ques'.$i});
        }
        (array)$read['p3_ans']=explode('; ;',$read->{'p3_ans'});
        (array)$read['p3_ques']=explode('; ;',$read->{'p3_ques'});
        (array)$read['p4_ans']=explode('; ;',$read->{'p4_ans'});
        return $read;
    }

    public function updateRead($data, $id)
    {
        $read = self::getById($id);
        for ($i=1; $i < 9; $i++) {
            if(!empty($data['p1_ques'.$i])){
                $data['p1_ques'.$i]=implode('; ;',(array)$data['p1_ques'.$i]);
            };
        }

        for ($i=1; $i < 3; $i++) {
            if(!empty($data['p2_ques'.$i])){
                $data['p2_ques'.$i]=implode('; ;',(array)$data['p2_ques'.$i]);
            };
        }
        $data['p3_ans']=implode('; ;',(array)$data['p3_ans']);
        $data['p3_ques']=implode('; ;',(array)$data['p3_ques']);
        $data['p4_ans']=implode('; ;',(array)$data['p4_ans']);
        $read->update($data);      
        return true;
    }
}
