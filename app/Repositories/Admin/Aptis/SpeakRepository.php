<?php

namespace App\Repositories\Admin\Aptis;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Models\Aptis\Speak;


/**
 * Class SpeakRepository.
 */
class SpeakRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Speak::class;
    }

    public function createSpeak($data){
        for ($i=1; $i < 4; $i++) {
            if(!empty($data['p1_ques'.$i])){
                $data['p1_ques'.$i]=implode('; ;',(array)$data['p1_ques'.$i]);
                $data['p2_ques'.$i]=implode('; ;',(array)$data['p2_ques'.$i]);
                $data['p3_ques'.$i]=implode('; ;',(array)$data['p3_ques'.$i]);
            };
        }
        for ($i=2; $i < 5; $i++) {
            if(isset($data['img_p'.$i])){
                $data['img_p'.$i] = UploadFile($data['img_p'.$i], 'img/aptis_speaking');
            }
        }
        $data['p4_ques']=implode('; ;',(array)$data['p4_ques']);
        Speak::create($data);        
        return true;
        
    }
    public function editSpeak($id){
        $speak = self::getById($id);
        for ($i=1; $i < 4; $i++) {
            (array)$speak['p1_ques'.$i]=explode('; ;',$speak->{'p1_ques'.$i});
            (array)$speak['p2_ques'.$i]=explode('; ;',$speak->{'p2_ques'.$i});
            (array)$speak['p3_ques'.$i]=explode('; ;',$speak->{'p3_ques'.$i});
        }
        (array)$speak['p4_ques']=explode('; ;',$speak->{'p4_ques'});
        return $speak;
    }

    public function updateSpeak($data, $id)
    {
        $speak = self::getById($id);
        for ($i=1; $i < 4; $i++) {
            if(!empty($data['p1_ques'.$i])){
                $data['p1_ques'.$i]=implode('; ;',(array)$data['p1_ques'.$i]);
                $data['p2_ques'.$i]=implode('; ;',(array)$data['p2_ques'.$i]);
                $data['p3_ques'.$i]=implode('; ;',(array)$data['p3_ques'.$i]);
            }
        }
        for ($i=2; $i < 5; $i++) {
            if(isset($data['img_p'.$i])){
                $data['img_p'.$i] = UploadFile($data['img_p'.$i], 'img/aptis_speaking');
            }else{
                $data['img_p'.$i] = $speak->{'img_p'.$i};
            }
        }
        $data['p4_ques']=implode('; ;',(array)$data['p4_ques']);
        $speak->update($data);
        return true;
    }
}
