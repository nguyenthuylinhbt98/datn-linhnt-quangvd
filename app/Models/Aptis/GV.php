<?php

namespace App\Models\Aptis;

use Illuminate\Database\Eloquent\Model;

class GV extends Model
{
    protected $table = 'aptis_gv';

    protected $guarded = [
        'id'
    ];

    public function test() {
    	return $this -> hasMany('App\Models\Aptis\Test','gv_id','id');
    }
}
