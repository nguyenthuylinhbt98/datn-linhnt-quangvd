<?php

namespace App\Models\Toeic;

use Illuminate\Database\Eloquent\Model;

class Part5 extends Model
{
    protected $table = 'toeic_part5';

    protected $guarded = [
        'id'
    ];
}
