<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class User_And_Aptis extends Model
{
    protected $table = 'user_and_aptis';

    protected $guarded = [
        'id'
    ];
    public function test() {
    	return $this -> belongsTo('App\Models\Aptis\Test','aptis_id','id');
    }
    public function user() {
    	return $this -> belongsTo('App\Models\Aptis\Test','user_id','id');
    }
}