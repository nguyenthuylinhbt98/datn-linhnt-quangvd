<?php

namespace App\Repositories\Admin\Toeic;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Models\Toeic\Part1;
use Illuminate\Support\Str;
//use Your Model

/**
 * Class Part1Repository.
 */
class Part1Repository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Part1::class;
    }

    public function createPart1(array $data)
    {
    	$part_1['example_test1'] = $data['example_test1'];
        $part_1['name'] = $data['name'];
        $part_1['number_ques'] = $data['number_ques'];
        
        if(isset($data['void_part1'])){
            $part_1['void_part1'] = UploadFile($data['void_part1'], 'mp3/toeic_part1');
        }

        if(isset($data['example_img'])){
            $part_1['example_img'] = UploadFile($data['example_img'], 'img/toeic_part1');
        }else{
            $part_1['example_img']='upload/img/toeic_part1/choice.png';
        }

        for ($i=1; $i <=10; $i++) { 
            if(isset($data['img_question_'.$i])){
                $part_1['img_question_'.$i] = UploadFile($data['img_question_'.$i], 'img/toeic_part1');
            }else{
                $part_1['img_question_'.$i]='upload/img/toeic_part1/choice.png';
            }
            if(isset($data['answer_question_'.$i])){
                $part_1['answer_question_'.$i]=$data['answer_question_'.$i];
            }else{
                $part_1['answer_question_'.$i]='null';
            }
        }

        Part1::create($part_1);        
        return true;
    }

    public function updatePart1(array $data, int $id)
    {
        $part1 = self::getById($id);
        $part_1['example_test1'] = $data['example_test1'];
        $part_1['name'] = $data['name'];
        $part_1['number_ques'] = $data['number_ques'];

        if(isset($data['void_part1'])){
            $part_1['void_part1'] = UploadFile($data['void_part1'], 'mp3/toeic_part1');
        }

        if(isset($data['example_img'])){
            $part_1['example_img'] = UploadFile($data['example_img'], 'img/toeic_part1');
        }

        for ($i=1; $i <=10; $i++) { 
            if(isset($data['img_question_'.$i])){
                $part_1['img_question_'.$i] = UploadFile($data['img_question_'.$i], 'img/toeic_part1');
            }
            
            if(isset($data['answer_question_'.$i])){
                $part_1['answer_question_'.$i]=$data['answer_question_'.$i];
            }else{
                $part_1['answer_question_'.$i]='null';
            }
        }
        $part1->update($part_1);        
        
        return true;
    }
}
