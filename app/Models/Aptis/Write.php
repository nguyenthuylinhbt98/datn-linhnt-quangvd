<?php

namespace App\Models\Aptis;

use Illuminate\Database\Eloquent\Model;

class Write extends Model
{
    protected $table = 'aptis_write';

    protected $guarded = [
        'id'
    ];

    public function test() {
    	return $this -> hasMany('App\Models\Aptis\Test','write_id','id');
    }
}
