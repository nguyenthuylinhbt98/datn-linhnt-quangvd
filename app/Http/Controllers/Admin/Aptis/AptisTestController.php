<?php

namespace App\Http\Controllers\Admin\Aptis;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Aptis\{Test, Listen, GV, Read, Speak, Write};
use App\Repositories\Admin\Aptis\TestRepository;

class AptisTestController extends Controller
{
    private $testRepository;

    public function __construct()
    {
        $this->testRepository = new TestRepository();
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['test'] = Test::paginate(10);
        return view('admin.elements.aptis.test.index', $data);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['listening'] = Listen::all();
        $data['reading'] = Read::all();
        $data['speaking'] = Speak::all();
        $data['writing'] = Write::all();
        $data['gv'] = GV::all();
        return view('admin.elements.aptis.test.add', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        $test = $this->testRepository->create($r->all()); 
        if($test){   
             return redirect('/admin/aptis/test')->with('thongbao', 'Success create new aptis test'); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['listening'] = Listen::all();
        $data['reading'] = Read::all();
        $data['speaking'] = Speak::all();
        $data['writing'] = Write::all();
        $data['gv'] = GV::all();
        $data['test'] = Test::findOrFail($id);
        return view('admin.elements.aptis.test.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
        $test = $this->testRepository->update($r->all(), $id); 
        if($test){   
             return redirect('/admin/aptis/test')->with('thongbao', 'Success create edit aptis test'); 
        }
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Test::destroy($id);
        return redirect()->back()->with('thongbao', 'Đã xóa thành công!!!');
    }
}
