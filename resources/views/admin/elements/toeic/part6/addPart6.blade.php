@extends('admin.master')
@section('content')
<!--main-->
<div class="container main exam">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Cập nhật đề thi</h1>
        </div>
    </div> 
    @if(session('thongbao'))
    <div class="alert bg-success" role="alert">
        <svg class="glyph stroked checkmark">
            <use xlink:href="#stroked-checkmark"></use>
        </svg>
        {{session('thongbao')}} <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
    </div>
    @endif
    <!--/.row-->
    <form action="/admin/toeic/part_6" method="post" accept-charset="utf-8" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><i class="fas fa-user"></i>Thêm đề thi mới</div>
                    <div class="panel-body">
                        <div class="row justify-content-center" style="margin-bottom:40px">
                            <div class="col-md-8 col-lg-8 col-lg-offset-2">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input value="{{old('name')}}" type="text" name="name" placeholder="Name test" class="form-control" required>
                                    {{ ShowErrors($errors, 'name')}}
                                </div>
                                <div class="container">
                                    <h2>Exam Toeic</h2>
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#part1">Part VI</a></li>
                                        
                                    </ul>
                                    <div class="tab-content">
                                        {{-- Part_6 --}}
                                        <div id="part1" class="tab-pane fade in active">
                                            <h3><b>PART VI</b></h3>
                                            <p> <b>Directions:</b> Read the texts that follow. A word or phrase is missing in some of the sentences. Four answer choices are given below each of the sentences. Select the best answer to complete the text. Then mark the letter (A), (B), (C) or (D) on the Answer Sheet. </p>
                                            
                                            <label for="num_para">
                                                Total paragraph: <input type="text" id="num_para" name="num_para" value="4">
                                            </label><br>
                                            <label for="begin">
                                                Question begin: <input type="text" id="begin" name="begin" value="131">
                                            </label><br>
                                            <label for="end">
                                                Question end: <input type="text" id="end" name="end" value="146">
                                            </label><br>
                                            
                                            
                                            <hr>
                                            @for($i=0; $i<5; $i++)
                                                <b>Paragraph {{$i+1}}</b>
                                                <textarea name="paragraph_{{$i}}[]" id="summary-ckeditor-{{$i}}" ></textarea>
                                                <p>Number question: <input id="num_paragraph_{{$i}}" type="number" name="paragraph_{{$i}}[]" value="4"></p>
                                                <div class="question_paragraph_{{$i}}">

                                                </div>
                                            @endfor

                                        </div>
                                        
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-lg-8 col-lg-offset-2 text-right"  style="margin-top: 20px;">
                                    <button class="btn btn-success"  type="submit">Cập nhật đề</button>
                                    <button class="btn btn-danger" type="reset">Huỷ bỏ</button>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!--/.row-->
</div>
<!--end main-->
@endsection

@section('script')
    <script>
        var begin = $("#begin").val();
        var num_para = $("#num_para").val();

        const addQues = (para, num_begin, total)=>{
            for(let i=0; i<total; i++){
                $(`.question_paragraph_${para}`).append(`
                    <b>Question ${num_begin+i}:</b><br>
                    <div>
                        <input class="form-control" type="text" name="question_${num_begin+i-begin}[]" placeholder="Answer A"><br>
                        <input class="form-control" type="text" name="question_${num_begin+i-begin}[]" placeholder="Answer B"><br>
                        <input class="form-control" type="text" name="question_${num_begin+i-begin}[]" placeholder="Answer C"><br>
                        <input class="form-control" type="text" name="question_${num_begin+i-begin}[]" placeholder="Answer D">

                        <b>Answer choice:</b>
                        <div class="row">
                            <div class="col-md-1">
                                <p style="float: left; margin-right: 5px;">A</p> <input type="radio" name="question_${num_begin+i-begin}[]" value="A">
                            </div>
                            <div class="col-md-1">
                                <p style="float: left; margin-right: 5px;">B</p> <input type="radio" name="question_${num_begin+i-begin}[]" value="B">
                            </div>
                            <div class="col-md-1">
                                <p style="float: left; margin-right: 5px;">C</p> <input type="radio" name="question_${num_begin+i-begin}[]" value="C">
                            </div>
                                <div class="col-md-1">
                                <p style="float: left; margin-right: 5px;">D</p> <input type="radio" name="question_${num_begin+i-begin}[]" value="D">
                            </div>
                        </div>
                        
                    </div> <hr>
                `)
            }
        }
        
        function Show(para){
            for(let i=para; i<num_para; i++){
                $(`.question_paragraph_${i}`).children().remove();
                let num_ques = $(`#num_paragraph_${i}`).val();
                let num_begin = Number($('#begin').val()) ;
                for(let j=1; j<=i; j++){
                    var a = $(`#num_paragraph_${j-1}`).val();
                    num_begin += Number(a);
                }
                addQues(i, num_begin, num_ques);
            }
        }
        Show(0);
        for(let i=0; i<num_para; i++){
            $(`#num_paragraph_${i}`).on("change", function(){
                Show(i);
            })
        }
        
    </script>
    <script src="./../ckeditor/ckeditor.js"></script>
    <script>
        for(let i = 0; i < 5; i++ )
        CKEDITOR.replace( 'summary-ckeditor-'+i );
    </script>
@endsection