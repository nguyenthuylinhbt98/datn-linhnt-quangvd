<?php

namespace App\Repositories\Admin\Toeic;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Models\Toeic\Transcript;
use Illuminate\Support\Str;
//use Your Model

/**
 * Class TranscriptRepository.
 */
class TranscriptRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */ 
    public function model()
    {
        return Transcript::class;
    }

    public function createTranscript($data){    	
        $data['diem_nghe']=implode('; ;',(array)$data['diem_nghe']);
        $data['diem_doc']=implode('; ;',(array)$data['diem_doc']);
        Transcript::create($data);
        return true;
    }

    public function updateTranscript($data, $id){
        $transcript = self::getById($id);    	
        $data['diem_nghe']=implode('; ;',(array)$data['diem_nghe']);
        $data['diem_doc']=implode('; ;',(array)$data['diem_doc']);
        $transcript->update($data);
        return true;
    }

    public function getShowTran($data){
        (array)$data['diem_nghe']=explode('; ;',$data['diem_nghe']);
        (array)$data['diem_doc']=explode('; ;',$data['diem_doc']);
        return $data;

    }
}
