<?php

namespace App\Http\Controllers\Admin\Aptis;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Aptis\GV;
use App\Repositories\Admin\Aptis\GVRepository;

class GVController extends Controller
{
    private $gvRepository;

    
    public function __construct()
    {
        $this->gvRepository = new GVRepository();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['gv'] = GV::paginate(10);
        return view('admin.elements.aptis.gv.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.elements.aptis.gv.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        $data = $r->all();
        $gv = $this->gvRepository->create($data); 
        if($gv){   
             return redirect('/admin/aptis/gv')->with('thongbao', 'Success create new Grammar & Vocabulary test'); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['gv'] = $this->gvRepository->edit($id); 
        return view('admin.elements.aptis.gv.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
        $data = $r->all();
        $gv = $this->gvRepository->update($data, $id); 
        if($gv){   
             return redirect('/admin/aptis/gv')->with('thongbao', 'Success edit Grammar & Vocabulary test'); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        GV::destroy($id);
        return redirect()->back()->with('thongbao', 'Success delete');
    }
}
