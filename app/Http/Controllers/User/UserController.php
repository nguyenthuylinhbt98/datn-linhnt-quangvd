<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Users\User;
use App\Http\Requests\Users\{UserRequest, EditUserRequest};
use App\Mail\NotifyCreateAccount;
use Mail;
use Auth;
use Hash;

class UserController extends Controller
{
    private $user;

    public function __construct() {
        $this->user = new User();
    }

    // ----Login-----
    public function getLogin(){
        return view("user.form.login");
    }

    public function postLogin(Request $r){
        $email = $r->email;
    	$password = $r->password;
    	if(Auth::attempt(['email'=>$email, 'password' => $password])){
    		return redirect('/');
    	}else{
    		return redirect()->back()->withErrors(['email'=>'Tài khoản hoặc mật khẩu không chính xác!!'])->withInput();
    	}	
    }

    // Register
    public function getRegister(){
        return view("user.form.register");
    }

    public function postRegister(UserRequest $r){
        $data = $r->all();
        $data['avatar'] = 'upload/user_avatar/no_avatar.png';
        if($data['password'] == $data['comfirm_password']){
            $data['password'] = Hash::make($r->password);
        }else{
            return redirect()->back()->withErrors(['password'=>'Mật khẩu đã nhập không khớp!!'])->withInput();
        }
        $data['level'] = 0;
        $this->user->create($data);
        Mail::to($r->email)->send(new NotifyCreateAccount());
        // ---------------
        $email = $r->email;
        $password = $r->password;
        if(Auth::attempt(['email'=>$email, 'password' => $password])){
            return redirect('/');
        }
    }

    public function getLogout(){
        Auth::logout();
        return  redirect('/');
    }
}
