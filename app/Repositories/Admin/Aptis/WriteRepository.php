<?php

namespace App\Repositories\Admin\Aptis;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Models\Aptis\Write;


/**
 * Class WriteRepository.
 */
class WriteRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Write::class;
    }

    public function createWrite($data){
        for ($i=1; $i < 6; $i++) {
            if(!empty($data['p1_ques'.$i])){
                $data['p1_ques'.$i]=implode('; ;',(array)$data['p1_ques'.$i]);
            };
        }
        if(!empty($data['p2'])){
            $data['p2']=implode('; ;',(array)$data['p2']);
        };

        for ($i=1; $i < 4; $i++) {
            if(!empty($data['p3_ques'.$i])){
                $data['p3_ques'.$i]=implode('; ;',(array)$data['p3_ques'.$i]);
            };
        }

        Write::create($data);        
        return true;
    }

    public function editWrite($id){
        $write = self::getById($id);
        for ($i=1; $i < 6; $i++) {
            (array)$write['p1_ques'.$i]=explode('; ;',$write->{'p1_ques'.$i});
        }
        (array)$write['p2']=explode('; ;',$write->{'p2'});
        for ($i=1; $i < 4; $i++) {
            (array)$write['p3_ques'.$i]=explode('; ;',$write->{'p3_ques'.$i});
        }
        return $write;

    }

    public function updateWrite($data, $id){
        $write = self::getById($id);
        for ($i=1; $i < 6; $i++) {
            if(!empty($data['p1_ques'.$i])){
                $data['p1_ques'.$i]=implode('; ;',(array)$data['p1_ques'.$i]);
            };
        }
        if(!empty($data['p2'])){
            $data['p2']=implode('; ;',(array)$data['p2']);
        };

        for ($i=1; $i < 4; $i++) {
            if(!empty($data['p3_ques'.$i])){
                $data['p3_ques'.$i]=implode('; ;',(array)$data['p3_ques'.$i]);
            };
        }
        $write->update($data);
        return true;
    }
}
