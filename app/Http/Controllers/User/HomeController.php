<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use App\Models\Toeic\Test as ToeicTest;
use App\Models\Aptis\Test as AptisTest;
use App\Models\Note;
use Auth;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function getIndex(){
        $data['test'] = ToeicTest::orderBy('id', 'ASC')->get();
    	return view('user.elements.index', $data);
    }

    public function saveNote(Request $r){
        $data = $r->all();
        $data['user_id'] = Auth::user()->id;
        Note::create($data);
        return redirect()->back();
    }
}
