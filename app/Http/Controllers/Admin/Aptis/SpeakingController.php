<?php

namespace App\Http\Controllers\Admin\Aptis;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Aptis\Speak;
use App\Repositories\Admin\Aptis\SpeakRepository;

class SpeakingController extends Controller
{
    private $speakRepository;

    public function __construct()
    {
        $this->speakRepository = new SpeakRepository();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['speak'] = Speak::paginate(10);
        return view('admin.elements.aptis.speak.index', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.elements.aptis.speak.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        $data = $r->all();
        $speak = $this->speakRepository->createSpeak($data); 
        if($speak){   
             return redirect('/admin/aptis/speaking')->with('thongbao', 'Success create new speaking test'); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['speak'] = $this->speakRepository->editSpeak($id); 
        return view('admin.elements.aptis.speak.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
        $data = $r->all();
        $speak = $this->speakRepository->updateSpeak($data, $id); 
        if($speak){   
             return redirect('/admin/aptis/speaking')->with('thongbao', 'Success edit speaking test'); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Speak::destroy($id);
        return redirect()->back()->with('thongbao', 'Success delete');
    }
}
