@extends('admin.master')

@section('content')
<!--main-->
    <div class="container main">

        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">List Part V</h1>
            </div>
        </div>
        <!--/.row-->

        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12">

                <div class="panel panel-primary">

                    <div class="panel-body">
                        <div class="bootstrap-table">
                            <div class="table-responsive">
                                @if(session('thongbao'))
                                   <div class="alert alert-success">
                                        {{ session('thongbao') }}
                                    </div>

                                @endif
                                <a href="/admin/toeic/part_5/create" class="btn btn-primary">Cập nhật đề mới</a>
                                <table class="table table-bordered" style="margin-top:20px;">

                                    <thead>
                                        <tr class="bg-primary">
                                            <th>ID</th>
                                            <th>Tên</th>
                                            <th>Ngày cập nhật</th>
                                            <th width='30%'>Tùy chọn</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($part5 as $item)
                                            <tr>
                                                <td>{{$item->id}}</td>
                                                <td>{{$item->name}}</td>
                                                <td>{{$item->updated_at}}</td>
                                                <td>
                                                    
                                                    <a href="/admin/toeic/part_5/{{$item->id}}/edit" class="btn btn-warning"><i class="fa fa-pencil" aria-hidden="true"></i> Sửa</a>
                                                    <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal{{$item->id}}"><i class="fa fa-trash" aria-hidden="true"></i> Xóa</a>
                                                    <div class="modal fade" id="exampleModal{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                        <div class="modal-dialog" role="document">
                                                            <div class="modal-content">
                                                                <div class="modal-header">
                                                                    <h5 class="modal-title" id="exampleModalLabel">Xóa đề Part V</h5>
                                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                    <span aria-hidden="true">&times;</span>
                                                                    </button>
                                                                </div>
                                                                <form action="/admin/toeic/part_5/{{$item->id}}" method="POST" accept-charset="utf-8">
                                                                    <div class="modal-body">
                                                                        @csrf
                                                                        <input type="hidden" name="_method" value="delete">
                                                                        <h4>Bạn có chắc chắn muốn xóa {{$item->name}}</h4>
                                                                    </div>
                                                                    <div class="modal-footer">
                                                                        <button type="button" class="btn btn-default" data-dismiss="modal">Quay lại</button>
                                                                        <button type="submit" class="btn btn-danger">Xóa</button>
                                                                    </div>
                                                                </form>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </td>
                                            </tr>
                                        @endforeach
                                        
                                
                                    </tbody>
                                </table>
                                <div align='right'>
                                    <ul class="pagination">
                                        {{$part5->links()}}

                                    </ul>
                                </div>
                            </div>
                            <div class="clearfix"></div>
                        </div>

                    </div>
                </div>
                <!--/.row-->


            </div>
        </div>
    </div>      
<!--end main-->
@endsection
