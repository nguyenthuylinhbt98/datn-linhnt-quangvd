@extends('admin.master')
@section('content')
<!--main-->
<div class="container main exam">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Cập nhật đề thi</h1>
        </div>
    </div> 
    @if(session('thongbao'))
       <div class="alert alert-success">
            {{ session('thongbao') }}
        </div>

    @endif
    <!--/.row-->
    <form action="/admin/part6/{{$part_6->id}}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><i class="fas fa-user"></i>Thêm đề thi mới</div>
                    <div class="panel-body">
                        <div class="row justify-content-center" style="margin-bottom:40px">
                            <div class="col-md-8 col-lg-8 col-lg-offset-2">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input value="{{$part_6->name}}" type="text" name="name" placeholder="Name test" class="form-control" required>
                                    {{ ShowErrors($errors, 'name')}}
                                </div>
                                <div class="container">
                                    <h2>Exam Toeic</h2>
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#part1">Part VI</a></li>
                                        
                                    </ul>
                                    <div class="tab-content">
                                        {{-- Part_6 --}}
                                        <div id="part1" class="tab-pane fade in active">
                                            <h3><b>PART VI</b></h3>
                                            <p> <b>Directions:</b> Read the texts that follow. A word or phrase is missing in some of the sentences. Four answer choices are given below each of the sentences. Select the best answer to complete the text. Then mark the letter (A), (B), (C) or (D) on the Answer Sheet. </p>
                                           
                                            @for($i=141; $i<153; $i+=3)
                                                <b>Question {{$i}} - {{$i+2}}: refer to the following text</b><br>
                                                @php
                                                    $a=($i-141)/3;
                                                @endphp
                                                <p>{!! $paragraph[$a] !!}</p>

                                                @for($j=0; $j<3; $j++)
                                                    <b>Question {{$i+$j}}:</b><br>
                                                    <div>
                                                        <p>A.{{$question[$i+$j-141][0]}}</p>
                                                        <p>B.{{$question[$i+$j-141][1]}}</p>
                                                        <p>C.{{$question[$i+$j-141][2]}}</p>
                                                        <p>D.{{$question[$i+$j-141][3]}}</p>
                                                        
                                                    </div>
                                                @endfor                                          
                                                
                                                <hr>
                                            @endfor    
                                        </div>
                                        
                                       
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!--/.row-->
</div>
<!--end main-->
@endsection

