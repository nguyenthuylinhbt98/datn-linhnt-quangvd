@extends('admin.master')
@section('content')
<!--main-->
<div class="container main exam">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Cập nhật đề thi</h1>
        </div>
    </div> 
    @if(session('thongbao'))
    <div class="alert bg-success" role="alert">
        <svg class="glyph stroked checkmark">
            <use xlink:href="#stroked-checkmark"></use>
        </svg>
        {{session('thongbao')}} <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
    </div>
    @endif
    <!--/.row-->

        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><i class="fa fa-book" style="padding-right: 10px;"></i>{{$test->name_test}}</div>
                    <div class="panel-body">
                        <div class="row justify-content-center" style="margin-bottom:40px">
                            <div class="col-md-8 col-lg-8 col-lg-offset-2">
                                <div class="container">
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#part1">Part I</a></li>
                                        <li><a data-toggle="tab" href="#part2">Part II</a></li>
                                        <li><a data-toggle="tab" href="#part3">Part III</a></li>
                                        <li><a data-toggle="tab" href="#part4">Part IV</a></li>
                                        <li><a data-toggle="tab" href="#part5">Part V</a></li>
                                        <li><a data-toggle="tab" href="#part6">Part VI</a></li>
                                        <li><a data-toggle="tab" href="#part7">Part VII</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        {{-- Part_1 --}}
                                        <div id="part1" class="tab-pane fade in active">
                                            <h3><b>PART I</b></h3>
                                            <p> <b>Directions:</b> For each in this part, you will hear four statements about a picture in your test book. 
                                                When you hear the statements, you must select the one statement that best describes what you see in the picture.
                                                Then find the number of the on your answer sheet and mark your answer. The statements will not be printed in your test
                                                book and will be spoken only one time. Look at the example below.</p>
                                            <p><b>Example:</b></p>
                                            <div class="example">
                                               <img id="img_example" src="../{{$part1->example_img}}" width="40%">
                                            </div>
                                            <p>{!! $part1->example_test1 !!}</p>
                                            @for($i=1; $i<=$part1->number_ques; $i++)
                                                <p><b>{{$i}}.</b></p>
                                                <div class="row">
                                                    <div class=" col-md-8 image">
                                                        <div class="example">
                                                            <img id="img_{{$i}}" src="../{{ $part1->{'img_question_'.$i} }}" width="40%">
                                                        </div>
                                                    </div>
                                                    <div class="col-md-4 answer">
                                                        <div class="choices">
                                                            <ul class="list-question">
                                                                <li>
                                                                    <span class="lq-number">A</span>
                                                                    <span class="my-radio">
                                                                        <input type="radio" id="" name="answer_question_{{$i}}" value="A">
                                                                    </span>
                                                                </li>
                                                                <li>
                                                                    <span class="lq-number">B</span>
                                                                    <span class="my-radio">
                                                                        <input type="radio" id="" name="answer_question_{{$i}}" value="B">
                                                                    </span>
                                                                </li>
                                                                <li>
                                                                    <span class="lq-number">C</span>
                                                                    <span class="my-radio">
                                                                        <input type="radio" id="" name="answer_question_{{$i}}" value="C">
                                                                    </span>
                                                                </li>
                                                                <li>
                                                                    <span class="lq-number">D</span>
                                                                    <span class="my-radio">
                                                                        <input type="radio" id="" name="answer_question_{{$i}}" value="D">
                                                                    </span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>

                                            @endfor
                                        </div>
                                        

                                        {{-- Part_2 --}}
                                        <div id="part2" class="tab-pane fade">
                                            <h3><b>PART II</b></h3>
                                            <p> <b>Directions:</b> Listen to these questions and statements. After each or statement, you will hear three responses. Select the most appropriate response. Mark your answer by clicking (A), (B), or (C). You will hear each or statement, and the responses, only once.</p>
                                            <p><b>Example:</b></p>
                                            <p>{!! $part2->example_test2 !!}</p>
                                            @for($i=0; $i<=$part2->end - $part2->begin; $i++)
                                                <b>{{$i + $part2->begin}}.</b>
                                                <div class="row">
                                                    <div class="col-md-1">
                                                        <p style="float: left; margin-right: 5px;">A</p> <input @if($part2['answer_question_'.$i]=='A') checked @endif type="radio" name="answer_question_{{$i}}" value="A">
                                                    </div>
                                                    <div class="col-md-1">
                                                        <p style="float: left; margin-right: 5px;">B</p> <input @if($part2['answer_question_'.$i]=='B') checked @endif type="radio" name="answer_question_{{$i}}" value="B">
                                                    </div>
                                                    <div class="col-md-1">
                                                        <p style="float: left; margin-right: 5px;">C</p> <input @if($part2['answer_question_'.$i]=='C') checked @endif type="radio" name="answer_question_{{$i}}" value="C">
                                                    </div>
                                                    
                                                </div>
                                                <hr>
                                            @endfor
                                        </div>

                                        {{-- Part_3 --}}
                                        <div id="part3" class="tab-pane fade">
                                            <h3><b>PART III</b></h3>
                                            <p> <b>Directions:</b> You will hear some conversations between two people. 
                                                You will be asked to answer three questions about what the speakers say in each conversation. 
                                                Select the best response to each and mark the letter (A), (B), (C), or (D) on your answer sheet. 
                                                The conversations will not be printed in your test book and will be spoken only one time.</p>
                                            
                                            @for($i=0; $i<=$part3->end - $part3->begin; $i++)
                                                <b>{{$i + $part3->begin}}.</b>{{$part3->{"question_".$i}[0]}}<br>
                                                <p>A.{{$part3->{"question_".$i}[1]}}</p>
                                                <p>B.{{$part3->{"question_".$i}[2]}}</p>
                                                <p>C.{{$part3->{"question_".$i}[3]}}</p>
                                                <p>D.{{$part3->{"question_".$i}[4]}}</p>

                                                <hr>
                                            @endfor
                                        </div>

                                        {{-- Part_4 --}}
                                        <div id="part4" class="tab-pane fade">
                                            <h3><b>PART IV</b></h3>
                                            <p> <b>Directions:</b> You will hear some talks given by a single speaker.
                                                You will be asked to answer three questions about what the speaker says in each talk. 
                                                Select the best response to each and mark the letter (A), (B), (C), or (D) on your answer sheet. 
                                                The talks will not be printed in your test book and will be spoken only one time.</p>

                                            @for($i=0; $i<= 100 - $part4->begin; $i++)
                                                <b>{{$i + $part4->begin}}.</b>{{$part4->{"question_".$i}[0]}}<br>
                                                <p>A.{{$part4->{"question_".$i}[1]}}</p>
                                                <p>B.{{$part4->{"question_".$i}[2]}}</p>
                                                <p>C.{{$part4->{"question_".$i}[3]}}</p>
                                                <p>D.{{$part4->{"question_".$i}[4]}}</p>

                                                <hr>
                                            @endfor
                                        </div>

                                        {{-- Part_5 --}}
                                        <div id="part5" class="tab-pane fade">
                                            <h3><b>PART V</b></h3>
                                            <p> <b>Directions:</b> A word or phrase is missing in each of the sentences below. 
                                                Four answer choices are given below each sentence. Select the best answer to complete the sentence.
                                                Then mark the letter (A), (B), (C), or (D) on your answer sheet.</p>                            
                                            @for($i=0; $i<= $part5->end - 101; $i++)

                                                <b>{{$i + 100}}.</b>{{$part5->{"question_".$i}[0]}}<br>
                                                <p>A.{{$part5->{"question_".$i}[1]}}</p>
                                                <p>B.{{$part5->{"question_".$i}[2]}}</p>
                                                <p>C.{{$part5->{"question_".$i}[3]}}</p>
                                                <p>D.{{$part5->{"question_".$i}[4]}}</p>

                                                <hr>
                                            @endfor

                                        </div>

                                                                                
                                        {{-- Part_6 --}}
                                        <div id="part6" class="tab-pane fade">
                                            <h3><b>PART VI</b></h3>
                                            <p> <b>Directions:</b> Read the texts that follow. A word or phrase is missing in some of the sentences. 
                                                Four answer choices are given below each of the sentences. Select the best answer to complete the text. 
                                                Then mark the letter (A), (B), (C) or (D) on the Answer Sheet. </p>

                                                @php $ques = $part6->begin @endphp
                                                @php $a=0 @endphp
                                                @for($i=0; $i<$part6->num_para; $i++)
                                                    <b>Paragraph {{$i+1}}</b>
                                                    <p>{!! $part6->{'paragraph_'.$i}[0] !!}</p>
                                                    <div class="question_paragraph_{{$i}}">
                                                        @for($j=0; $j<$part6->{'paragraph_'.$i}[1]; $j++)
                                                            <b>Question {{$ques + $j}}:</b><br>
                                                            <div>
                                                                <p>A.{{$part6->{"question_".$a}[0]}}</p>
                                                                <p>A.{{$part6->{"question_".$a}[1]}}</p>
                                                                <p>B.{{$part6->{"question_".$a}[2]}}</p>
                                                                <p>C.{{$part6->{"question_".$a}[3]}}</p>
                                                                
                                                            </div> <hr>
    
                                                           @php $a++ @endphp
                                                        @endfor
                                                    </div>
                                                    @php $ques+= $part6->{'paragraph_'.$i}[1]  @endphp
                                                @endfor
                                        </div>
                                        
                                        {{-- Part_7 --}}
                                        <div id="part7" class="tab-pane fade">
                                            <h3><b>PART VII</b></h3>
                                            <p> <b>Directions:</b> Read the texts. You will notice that each text is followed by several questions. 
                                                For each question, decide which of the four answer choices: (A), (B), (C), or (D), best answers the question. 
                                                Then mark your answer on the Answer Sheet. </p>

                                                @php $ques = $part7->begin @endphp
                                                @php $a=0 @endphp
                                                @for($i=0; $i<$part7->num_para; $i++)
                                                    <b>Paragraph {{$i+1}}</b>
                                                    <p>{!! $part7->{'paragraph_'.$i}[0] !!}</p>
                                                    <div class="question_paragraph_{{$i}}">
                                                        @for($j=0; $j<$part7->{'paragraph_'.$i}[1]; $j++)
                                                            <b>Question {{$ques + $j}}: {{$part7->{"question_".$a}[0]}}</b><br>
                                                            <div>
                                                                <p>A.{{$part7->{"question_".$a}[1]}}</p>
                                                                <p>B.{{$part7->{"question_".$a}[2]}}</p>
                                                                <p>C.{{$part7->{"question_".$a}[3]}}</p>
                                                                <p>C.{{$part7->{"question_".$a}[4]}}</p>
                                                                
                                                            </div> <hr>
    
                                                           @php $a++ @endphp
                                                        @endfor
                                                    </div>
                                                    @php $ques+= $part7->{'paragraph_'.$i}[1]  @endphp
                                                @endfor
                                            
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

    <!--/.row-->
</div>
<!--end main-->
@endsection

@section('script')
   
@endsection