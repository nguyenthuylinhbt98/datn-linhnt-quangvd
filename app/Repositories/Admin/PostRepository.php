<?php

namespace App\Repositories\Admin;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
//use Your Model
use App\Models\Post;


/**
 * Class PostRepository.
 */
class PostRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Post::class;
    }
    public function create($data){
        if(empty($data['image'])) {
            $data['image'] = "";
        } else {
            $data['image'] = UploadFile($data['image'], 'img/post');
        }
        Post::create($data);
        return true;
    }
    public function update($data, $id){
        $post = self::getById($id);
        if(isset($data['image'])) {
            $data['image'] = UploadFile($data['image'], 'img/post');
        }
        $post->update($data);
        return true;
    }
}
