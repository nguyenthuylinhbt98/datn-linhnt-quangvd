<?php

namespace App\Models\Toeic;

use Illuminate\Database\Eloquent\Model;

class Part2 extends Model
{
    protected $table = 'toeic_part2';

    protected $guarded = [
        'id'
    ];

}
