<?php

namespace App\Repositories\Admin\Toeic;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Models\Toeic\Part4;

//use Your Model

/**
 * Class Part4Repository.
 */
class Part4Repository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Part4::class;
    }

    public function createPart4(array $data)
    {
        $part_4['name'] = $data['name'];
        $part_4['begin'] = $data['begin'];

        if(isset($data['void_part4'])){
            $part_4['void_part4'] = UploadFile($data['void_part4'], 'mp3/toeic_part4');
        }

        for ($i=0; $i < 40; $i++) {
            if(!empty($data['question_'.$i])){
                $part_4['question_'.$i]=implode('; ;',(array)$data['question_'.$i]);
            }
        }


        Part4::create($part_4);        
        return true;
    }

    // 
    // 
    // -----------------show edit----------
    // 
    // 

    public function editPart4(int $id)
    {
        $part_4 = self::getById($id);
        
        for ($i=0; $i < 40; $i++) {
            (array)$part_4['question_'.$i]=explode('; ;',$part_4->{'question_'.$i});
        }
        return $part_4;
    }
    // 
    // 
    // ------------update-------
    // 
    // 

    public function updatePart4(array $data, int $id)
    {
        $part4 = self::getById($id);
      
        $part_4['name'] = $data['name'];
        $part_4['begin'] = $data['begin'];

        if(isset($data['void_part4'])){
            $part_4['void_part4'] = UploadFile($data['void_part4'], 'mp3/toeic_part4');
        }

        for ($i=0; $i < 40; $i++) {
            if(!empty($data['question_'.$i])){
                $part_4['question_'.$i]=implode('; ;',(array)$data['question_'.$i]);
            }
        }
        $part4->update($part_4);        
        return true;
    }
}
