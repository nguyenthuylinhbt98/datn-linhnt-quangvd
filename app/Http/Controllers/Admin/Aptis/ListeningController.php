<?php

namespace App\Http\Controllers\Admin\Aptis;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Aptis\Listen;
use App\Repositories\Admin\Aptis\ListenRepository;

class ListeningController extends Controller
{
    private $listenRepository;

    
    public function __construct()
    {
        $this->listenRepository = new ListenRepository();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['listening'] = Listen::paginate(10);
        return view('admin.elements.aptis.listen.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.elements.aptis.listen.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        $data = $r->all();
        $listen = $this->listenRepository->create($data); 
        if($listen){   
             return redirect('/admin/aptis/listening')->with('thongbao', 'Success create new Listening test'); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['listen'] = $this->listenRepository->edit($id); 
        return view('admin.elements.aptis.listen.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
        $data = $r->all();
        $listen = $this->listenRepository->update($data, $id); 
        if($listen){   
             return redirect('/admin/aptis/listening')->with('thongbao', 'Success edit Listening test'); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Listen::destroy($id);
        return redirect()->back()->with('thongbao', 'Success delete');
    }
}
