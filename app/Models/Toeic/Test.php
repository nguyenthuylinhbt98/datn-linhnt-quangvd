<?php

namespace App\Models\Toeic;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    protected $table = 'toeic_test';

    protected $guarded = [
        'id'
    ];
}
