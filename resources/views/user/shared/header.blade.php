<header>
    <!-- Header Start -->
    <div class="header-area header-transparent">
        <div class="main-header ">
            <div class="header-bottom  header-sticky">
                <div class="container-fluid">
                    <div class="row align-items-center">
                        <!-- Logo -->
                        <div class="col-xl-2 col-lg-2">
                            <div class="logo">
                                <a href="/"><img src="assets/img/logo/logo.png" alt="" class="img-fluid"></a>
                            </div>
                        </div>
                        <div class="col-xl-10 col-lg-10">
                            <div class="menu-wrapper d-flex align-items-center justify-content-end">
                                <!-- Main-menu -->
                                <div class="main-menu d-none d-lg-block">
                                    <nav>
                                        <ul id="navigation">                                                                                          
                                            <li class="active" ><a href="/">Home</a></li>
                                            <li><a href="/about">About</a></li>
                                            {{-- <li><a href="">Courses</a></li> --}}
                                            <li><a href="/#">Courses</a>
                                                <ul class="submenu">
                                                    <li><a href="/toeic">Toeic</a></li>
                                                    <li><a href="/aptis">Aptis</a></li>
                                                </ul>
                                            </li>
                                            <li><a href="/q&a">Q&A</a></li>
                                            @if(!empty(Auth::user()))
                                                <li><a href="/for-teacher">For Teacher</a></li>
                                            @endif
                                            <li><a href="/contact">Contact</a></li>
                                           

                                            @if(empty(Auth::user()))
                                                <li class="button-header margin-left "><a href="/singup" class="btn">Join</a></li>
                                                <li class="button-header"><a href="/singin" class="btn btn3">Log in</a></li>
                                            @else
                                                <li class="button-header">
                                                    <a href="/profile/{{Auth::user()->id}}" class="btn btn3">{{Auth::user()->full}}</a>
                                                </li>
                                            @endif
                                           
                                           
                                        </ul>
                                    </nav>
                                </div>
                            </div>
                        </div> 
                        <!-- Mobile Menu -->
                        <div class="col-12">
                            <div class="mobile_menu d-block d-lg-none"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Header End -->
</header>