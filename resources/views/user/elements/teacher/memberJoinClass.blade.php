@extends('user.master')

@section('css')
     <style>
        .blog_left_sidebar img{
             height: 310px;
             object-fit: cover;
             
         }
        .form-group label {
            font-size: 20px;
            font-weight: 700;
            width: 200px;
             padding-top: 8px;
        }
        .form {
            background: url('../../default/create.png')
        }
     </style>
    
@endsection

@section('content')
<main>
    <!--? slider Area Start-->
    <section class="slider-area slider-area2">
        <div class="slider-active">
            <!-- Single Slider -->
            <div class="single-slider slider-height2">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-8 col-lg-11 col-md-12">
                            <div class="hero__caption hero__caption2">
                                <h1 data-animation="bounceIn" data-delay="0.2s">Category for teachers</h1>
                                <!-- breadcrumb Start-->
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                                        <li class="breadcrumb-item"><a>For Teacher</a></li> 
                                    </ol>
                                </nav>
                                <!-- breadcrumb End -->
                            </div>
                        </div>
                    </div>
                </div>          
            </div>
        </div>
    </section>
    <div class="courses-area section-padding40 fix">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-4 col-lg-4">
                    <div class="section-tittle text-center mb-55">
                        <h2>List Members <span>({{ count($members) }})</span></h2>
                    </div>
                    <div class="blog_right_sidebar" style="max-height: 810px; overflow: auto;">
                        <aside class="single_sidebar_widget search_widget">
                            @foreach($members as $key=>$member)
                                <p>
                                    {{ $key+1 }}. {{ $member->user }}
                                </p>
                            @endforeach
                        </aside>
                    </div>
                </div>
                <div class="col-xl-8 col-lg-8">
                    <div class="section-tittle text-center mb-55">
                        <h2>Group content</h2>
                        @if(session('thongbao'))
                            <div class="alert bg-success" role="alert" style="font-size: 20px; color: #ffffff">
                                {!!  session('thongbao') !!} 
                            </div>
                        @endif
                    </div>
                    <div class="section-tittle text-center mb-55">
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</main>
@endsection

@section('script')
    <script>
        $('.button-contactForm').on('click', function(){
            $('form').submit();
        })
    </script>
@endsection