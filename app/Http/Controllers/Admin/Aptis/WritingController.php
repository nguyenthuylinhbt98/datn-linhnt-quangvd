<?php

namespace App\Http\Controllers\Admin\Aptis;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Aptis\Write;
use App\Repositories\Admin\Aptis\WriteRepository;

class WritingController extends Controller
{
    private $writeRepository;

    public function __construct()
    {
        $this->writeRepository = new WriteRepository();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['write'] = Write::paginate(10);
        return view('admin.elements.aptis.write.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.elements.aptis.write.add');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        $data = $r->all();
        $write = $this->writeRepository->createWrite($data); 
        if($write){   
             return redirect('/admin/aptis/writing')->with('thongbao', 'Success create new writing test'); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['write'] = $this->writeRepository->editWrite($id); 
        return view('admin.elements.aptis.write.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
        $data = $r->all();
        $write = $this->writeRepository->updateWrite($data, $id); 
        if($write){   
             return redirect('/admin/aptis/writing')->with('thongbao', 'Success edit writing test'); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Write::destroy($id);
        return redirect()->back()->with('thongbao', 'Success delete');
    }
}
