@extends('user.master')

@section('css')
	<link rel="stylesheet" href="assets/css/profile.css">
@endsection

@section('content')
<main>
    <!--? slider Area Start-->
    <section class="slider-area slider-area2">
        <div class="slider-active">
            <!-- Single Slider -->
            <div class="single-slider slider-height2">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-8 col-lg-11 col-md-12">
                            <div class="hero__caption hero__caption2">
                                <h1 data-animation="bounceIn" data-delay="0.2s">{{ Auth::user()->full }} Profile</h1>
                                <!-- breadcrumb Start-->
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                                        <li class="breadcrumb-item"><a href="/profile/{{ Auth::user()->id }}">Profile</a></li> 
                                    </ol>
                                </nav>
                                <!-- breadcrumb End -->
                            </div>
                        </div>
                    </div>
                </div>          
            </div>
        </div>
    </section>

    {{-- c------------ --}}
    <div class="container emp-profile">
            <div class="row">
                <div class="col-md-4">
                    <div class="profile-img">
                        @if(Auth::user()->avatar)
                            <img src="../{{Auth::user()->avatar}}" alt="">
                        @else
                            <img src="../upload/user_avatar/no_avatar.png" alt={{ Auth::user()->full }} >
                        @endif  
                       
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="profile-head">
                                <h5>
                                    {{Auth::user()->full}}
                                </h5>
                                <h6>
                                    User
                                </h6>
                                <p class="proile-rating">RANKINGS : <span>8/10</span></p>
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role="tab" aria-controls="home" aria-selected="true">About</a>
                            </li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-4">
                    @if(Auth::user()->level == 1 || Auth::user()->level == 2)
                        <a href="/admin" class="profile-edit-btn">Admin page</a>
                    @endif
                    <a href="/profile/{{ Auth::user()->id }}/edit" class="profile-edit-btn">Edit Profile</a>
                    <a href="/logout" class="profile-edit-btn">Logout</a>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="profile-work">
                        <p>About {{ Auth::user()->full }}</p>
                        <ul>
                            <li>{{ Auth::user()->email }}</li>
                            <li>{{ Auth::user()->date_of_birth }}</li>
                            <li>{{ Auth::user()->phone }}</li>
                            <li>{{ Auth::user()->address }}</li>
                        </ul>
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="tab-content profile-tab" id="myTabContent">
                        <div class="tab-pane fade show active" id="home" role="tabpanel" aria-labelledby="home-tab">
                            <table class="table">
                                <thead class="thead-dark">
                                  <tr>
                                    <th scope="col">#</th>
                                    <th scope="col">Content</th>
                                    <th scope="col">Created at</th>
                                  </tr>
                                </thead>
                                <tbody>
                                    @foreach($note as $item)
                                        <tr>
                                            <th scope="row">{{ $item->id }}</th>
                                            <td>{{ $item->note }}</td>
                                            <td>{{ $item->created_at }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                              </table>
                        </div>
                        {{-- <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                                   
                        </div> --}}
                    </div>
                </div>
            </div>
    </div>
	
</main>

@endsection

