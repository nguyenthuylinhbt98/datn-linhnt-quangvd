@extends('admin.master')
@section('content')
<!--main-->
<div class="container main exam">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Cập nhật đề thi</h1>
        </div>
    </div> 
    @if(session('thongbao'))
       <div class="alert alert-success">
            {{ session('thongbao') }}
        </div>

    @endif
    <!--/.row-->

    <form action="/admin/toeic/part_7/{{$part7->id}}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="_method" value="put">
        <input type="hidden" name="id" value="{{$part7->id}}">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><i class="fas fa-user"></i>Thêm đề thi mới</div>
                    <div class="panel-body">
                        <div class="row justify-content-center" style="margin-bottom:40px">
                            <div class="col-md-8 col-lg-8 col-lg-offset-2">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input value="{{$part7->name}}" type="text" name="name" placeholder="Name test" class="form-control" required>
                                    {{ ShowErrors($errors, 'name')}}
                                </div>
                                <div class="container">
                                    <h2>Exam Toeic</h2>
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#part1">Part VII</a></li>
                                       
                                    </ul>
                                    <div class="tab-content">
                                                                               
                                        {{-- Part_7 --}}
                                        <div id="part1" class="tab-pane fade in active">
                                            
                                            <h3><b>PART VII</b></h3>
                                            <p> <b>Directions:</b> Read the texts. You will notice that each text is followed by several questions. For each question, decide which of the four answer choices: (A), (B), (C), or (D), best answers the question. Then mark your answer on the Answer Sheet. </p>
                                            <label for="num_para">
                                                Total paragraph: <input type="text" id="num_para" name="num_para" value="4">
                                            </label><br>
                                            <label for="begin">
                                                Question begin: <input type="text" id="begin" name="begin" value={{$part7->begin}}>
                                            </label><br>

                                            @php $ques = $part7->begin @endphp
                                            @php $a=0 @endphp
                                            @for($i=0; $i<15; $i++)
                                                <b>Paragraph {{$i+1}}</b>
                                                <textarea name="paragraph_{{$i}}[]" id="summary-ckeditor-{{$i}}" >{!! $part7->{'paragraph_'.$i}[0] !!}</textarea><br>
                                                <p>Number question: <input id="num_paragraph_{{$i}}" type="number" name="paragraph_{{$i}}[]" value={{ $part7->{'paragraph_'.$i}[1] }}></p>
                                                
                                                <div class="question_paragraph_{{$i}}">
                                                    @for($j=0; $j< $part7->{'paragraph_'.$i}[1]; $j++)
                                                        <b>Question {{$ques + $j}}:</b><br>
                                                        <div>

                                                            @for($k=0; $k<5; $k++)
                                                                @if(isset($part7->{'question_'.$a}[$k]))
                                                                    <input class="form-control" type="text" name="question_{{$a}}[]" value="{{ $part7->{'question_'.$a}[$k] }}"><br>
                                                                @else
                                                                    <input class="form-control" type="text" name="question_{{$a}}[]"><br>
                                                                @endif
                                                            @endfor
                                    
                                                            <b>Answer choice:</b>
                                                            <div class="row">
                                                                @if(isset($part7->{'question_'.$a}[5]))
                                                                    <div class="col-md-1">
                                                                        <p style="float: left; margin-right: 5px;">A</p> <input @if($part7->{'question_'.$a}[5] == "A") checked @endif type="radio" name="question_{{$a}}[]" value="A">
                                                                    </div>
                                                                    <div class="col-md-1">
                                                                        <p style="float: left; margin-right: 5px;">B</p> <input @if($part7->{'question_'.$a}[5] == "B") checked @endif type="radio" name="question_{{$a}}[]" value="B">
                                                                    </div>
                                                                    <div class="col-md-1">
                                                                        <p style="float: left; margin-right: 5px;">C</p> <input @if($part7->{'question_'.$a}[5] == "C") checked @endif type="radio" name="question_{{$a}}[]" value="C">
                                                                    </div>
                                                                        <div class="col-md-1">
                                                                        <p style="float: left; margin-right: 5px;">D</p> <input @if($part7->{'question_'.$a}[5] == "D") checked @endif type="radio" name="question_{{$a}}[]" value="D">
                                                                    </div>
                                                                @else  
                                                                    <div class="col-md-1">
                                                                        <p style="float: left; margin-right: 5px;">A</p> <input type="radio" name="question_{{$a}}[]" value="A">
                                                                    </div>
                                                                    <div class="col-md-1">
                                                                        <p style="float: left; margin-right: 5px;">B</p> <input type="radio" name="question_{{$a}}[]" value="B">
                                                                    </div>
                                                                    <div class="col-md-1">
                                                                        <p style="float: left; margin-right: 5px;">C</p> <input type="radio" name="question_{{$a}}[]" value="C">
                                                                    </div>
                                                                        <div class="col-md-1">
                                                                        <p style="float: left; margin-right: 5px;">D</p> <input type="radio" name="question_{{$a}}[]" value="D">
                                                                    </div>
                                                                @endif
                                                            </div>
                                                            
                                                        </div> <hr>

                                                       @php $a++ @endphp
                                                    @endfor
                                                </div>

                                                @php $ques+= (int)$part7->{'paragraph_'.$i}[1]  @endphp
                                            @endfor                                            
                                        
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-lg-8 col-lg-offset-2 text-right"  style="margin-top: 20px;">
                                    <button class="btn btn-success"  type="submit">Edit</button>
                                    <button class="btn btn-danger" type="reset">Cancel</button>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!--/.row-->
</div>
<!--end main-->
@endsection

@section('script')
<script src="./../ckeditor/ckeditor.js"></script>
<script>
    for(let i = 0; i < 15; i++ )
    CKEDITOR.replace( 'summary-ckeditor-'+i );
</script>
    
@endsection