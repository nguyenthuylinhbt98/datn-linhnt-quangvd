@extends('admin.master')
@section('content')
<!--main-->
<div class="container main exam">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Cập nhật đề thi</h1>
        </div>
    </div> 
    @if(session('thongbao'))
       <div class="alert alert-success">
            {{ session('thongbao') }}
        </div>

    @endif
    <!--/.row-->
    <form action="/admin/toeic/part_6/{{$part6->id}}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="_method" value="put">
        <input type="hidden" name="id" value="{{$part6->id}}">

        {{-- @dd($part6) --}}
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><i class="fas fa-user"></i>Thêm đề thi mới</div>
                    <div class="panel-body">
                        <div class="row justify-content-center" style="margin-bottom:40px">
                            <div class="col-md-8 col-lg-8 col-lg-offset-2">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input value="{{$part6->name}}" type="text" name="name" placeholder="Name test" class="form-control" required>
                                    {{ ShowErrors($errors, 'name')}}
                                </div>
                                <div class="container">
                                    <h2>Exam Toeic</h2>
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#part6">Part VI</a></li>
                                        
                                    </ul>
                                    <div class="tab-content">
                                        {{-- Part6 --}}
                                        <div id="part6" class="tab-pane fade in active">
                                            <h3><b>PART VI</b></h3>
                                            <p> <b>Directions:</b> Read the texts that follow. A word or phrase is missing in some of the sentences. Four answer choices are given below each of the sentences. Select the best answer to complete the text. Then mark the letter (A), (B), (C) or (D) on the Answer Sheet. </p>
                                            
                                            <label for="num_para">
                                                Total paragraph: <input type="text" id="num_para" name="num_para" value="4">
                                            </label><br>
                                            <label for="begin">
                                                Question begin: <input type="text" id="begin" name="begin" value={{$part6->begin}}>
                                            </label><br>
                                            <label for="end">
                                                Question end: <input type="text" id="end" name="end" value={{$part6->end}}>
                                            </label><br><hr>
                                            @php $ques = $part6->begin @endphp
                                            @php $a=0 @endphp
                                            @for($i=0; $i<5; $i++)
                                                <b>Paragraph {{$i+1}}</b>
                                                <textarea name="paragraph_{{$i}}[]" id="summary-ckeditor-{{$i}}" >{!! $part6->{'paragraph_'.$i}[0] !!}</textarea><br>
                                                <p>Number question: <input id="num_paragraph_{{$i}}" type="number" name="paragraph_{{$i}}[]" value={{ $part6->{'paragraph_'.$i}[1] }}></p>
                                                <div class="question_paragraph_{{$i}}">
                                                    @for($j=0; $j<$part6->{'paragraph_'.$i}[1]; $j++)
                                                        <b>Question {{$ques + $j}}:</b><br>
                                                        <div>
                                                            @for($k=0; $k<4; $k++)
                                                                @if(isset($part6->{'question_'.$a}[$k]))
                                                                    <input class="form-control" type="text" name="question_{{$a}}[]" value="{{ $part6->{'question_'.$a}[$k] }}"><br>
                                                                @else
                                                                    <input class="form-control" type="text" name="question_{{$a}}[]"><br>
                                                                @endif
                                                            @endfor
                                    
                                                            <b>Answer choice:</b>
                                                            <div class="row">
                                                                @if(isset($part6->{'question_'.$a}[4]))
                                                                    <div class="col-md-1">
                                                                        <p style="float: left; margin-right: 5px;">A</p> <input @if($part6->{'question_'.$a}[4] == "A") checked @endif type="radio" name="question_{{$a}}[]" value="A">
                                                                    </div>
                                                                    <div class="col-md-1">
                                                                        <p style="float: left; margin-right: 5px;">B</p> <input @if($part6->{'question_'.$a}[4] == "B") checked @endif type="radio" name="question_{{$a}}[]" value="B">
                                                                    </div>
                                                                    <div class="col-md-1">
                                                                        <p style="float: left; margin-right: 5px;">C</p> <input @if($part6->{'question_'.$a}[4] == "C") checked @endif type="radio" name="question_{{$a}}[]" value="C">
                                                                    </div>
                                                                        <div class="col-md-1">
                                                                        <p style="float: left; margin-right: 5px;">D</p> <input @if($part6->{'question_'.$a}[4] == "D") checked @endif type="radio" name="question_{{$a}}[]" value="D">
                                                                    </div>
                                                                @else                                                                    
                                                                    <div class="col-md-1">
                                                                        <p style="float: left; margin-right: 5px;">A</p> <input type="radio" name="question_{{$a}}[]" value="A">
                                                                    </div>
                                                                    <div class="col-md-1">
                                                                        <p style="float: left; margin-right: 5px;">B</p> <input type="radio" name="question_{{$a}}[]" value="B">
                                                                    </div>
                                                                    <div class="col-md-1">
                                                                        <p style="float: left; margin-right: 5px;">C</p> <input type="radio" name="question_{{$a}}[]" value="C">
                                                                    </div>
                                                                        <div class="col-md-1">
                                                                        <p style="float: left; margin-right: 5px;">D</p> <input type="radio" name="question_{{$a}}[]" value="D">
                                                                    </div>
                                                                @endif
                                                            </div>
                                                            
                                                        </div> <hr>

                                                       @php $a++ @endphp
                                                    @endfor
                                                </div>


                                                @php $ques+= $part6->{'paragraph_'.$i}[1]  @endphp
                                            @endfor
                                        </div>
                                        
                                       
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-lg-8 col-lg-offset-2 text-right"  style="margin-top: 20px;">
                                    <button class="btn btn-success"  type="submit">Edit</button>
                                    <button class="btn btn-danger" type="reset">Cancel</button>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!--/.row-->
</div>
<!--end main-->
@endsection

@section('script')

<script src="./../ckeditor/ckeditor.js"></script>
<script>
    for(let i = 0; i < 5; i++ )
    CKEDITOR.replace( 'summary-ckeditor-'+i );
</script>
@endsection