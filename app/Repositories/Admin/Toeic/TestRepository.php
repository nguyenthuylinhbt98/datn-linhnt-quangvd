<?php

namespace App\Repositories\Admin\Toeic;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Models\Toeic\{Part1, Part2, Part3, Part4, Part5, Part6, Part7, Test};
use DB;
/**
 * Class TestRepository.
 */
class TestRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Test::class;
    }

    public function createTest(array $data)
    {
    	$data['id_part']=implode('; ;',(array)$data['id_part']);
        Test::create($data);        
        return true;
    }
    // ----------------------

    public function editTest(int $id)
    {
        $test = self::getById($id);              
        (array)$test['id_part']=explode('; ;',$test->{'id_part'});
        return $test;
    }

    // ----------------------
    public function updateTest(array $data, int $id)
    {
        $test = self::getById($id);
        $data['id_part']=implode('; ;',(array)$data['id_part']);
        $test->update($data); 
        return true;
    }

    // ----------------------
    public function showTest($id){
        $test = self::getById($id);
        (array)$test['id_part']=explode('; ;',$test->{'id_part'});
        $data['test'] = $test; 
        $data['part1'] = Part1::find($test['id_part'][0]);
        $data['part2'] = Part2::find($test['id_part'][1]);
        $data['part3'] = Part3::find($test['id_part'][2]);
        $data['part4'] = Part4::find($test['id_part'][3]);
        $data['part5'] = Part5::find($test['id_part'][4]);
        $data['part6'] = Part6::find($test['id_part'][5]);
        $data['part7'] = Part7::find($test['id_part'][6]);
        // ---

        for ($i=0; $i < 40; $i++) {
            (array)$data['part3']['question_'.$i]=explode('; ;',$data['part3']->{'question_'.$i});
            (array)$data['part4']['question_'.$i]=explode('; ;',$data['part4']->{'question_'.$i});
            (array)$data['part5']['question_'.$i]=explode('; ;',$data['part5']->{'question_'.$i});
        }
        // ---
        for ($i=0; $i<5; $i++) {
            (array)$data["part6"]['paragraph_'.$i]=explode('; ;',$data["part6"]->{'paragraph_'.$i});
        };
        
        for ($i=0; $i<20; $i++){
            (array)$data["part6"]['question_'.$i]=explode('; ;',$data['part6']->{'question_'.$i});
        }
        // ---
        for ($i=0; $i<15; $i++) {
            (array)$data['part7']['paragraph_'.$i]=explode('; ;',$data['part7']->{'paragraph_'.$i});
        };
        
        for ($i=0; $i<55; $i++){
            (array)$data['part7']['question_'.$i]=explode('; ;',$data['part7']->{'question_'.$i});
        }

        return $data;
    }

    public function getCorrectAns($id){
        $test = self::getById($id);
        (array)$test['id_part']=explode('; ;',$test->{'id_part'});
        $part1 = Part1::find($test['id_part'][0]);
        $part2 = Part2::find($test['id_part'][1]);
        $part3 = Part3::find($test['id_part'][2]);
        $part4 = Part4::find($test['id_part'][3]);
        $part5 = Part5::find($test['id_part'][4]);
        $part6 = Part6::find($test['id_part'][5]);
        $part7 = Part7::find($test['id_part'][6]);
        // -----------------------------------
        for ($i=0; $i < 40; $i++) {
            (array)$part3['question_'.$i]=explode('; ;',$part3->{'question_'.$i});
            (array)$part4['question_'.$i]=explode('; ;',$part4->{'question_'.$i});
            (array)$part5['question_'.$i]=explode('; ;',$part5->{'question_'.$i});
        }

        // ----------------------------------------
       
        for ($i=0; $i<20; $i++){
            (array)$part6['question_'.$i]=explode('; ;',$part6->{'question_'.$i});
        }
        // // ---
        for ($i=0; $i<55; $i++){
            (array)$part7['question_'.$i]=explode('; ;',$part7->{'question_'.$i});
        }

        // ------------------
        $data['correctAnsLs'] = array();
        for ($i=1; $i<= $part1->number_ques; $i++) {
            array_push($data['correctAnsLs'], $part1['answer_question_'.$i]);
        };

        for($i=0; $i <= $part2->end - $part2->begin; $i++){
            array_push($data['correctAnsLs'], $part2['answer_question_'.$i]);
        }

        for($i=0; $i<= $part3->end-$part3->begin; $i++){
            if (isset($part3['question_'.$i][5])) {
                array_push($data['correctAnsLs'], $part3['question_'.$i][5]);
            }else{
                array_push($data['correctAnsLs'], '0');
            }   

        }

        for($i=0; $i <= 100-$part4->begin; $i++){
            if (isset($part4['question_'.$i][5])) {
                array_push($data['correctAnsLs'], $part4['question_'.$i][5]);
            }else{
                array_push($data['correctAnsLs'], '0');
            }   

        }

        // ------------------------
        $data['correctAnsRe'] = array();
        for($i=0; $i<=$part5->end-101; $i++){
            if (isset($part5['question_'.$i][5])) {
                array_push($data['correctAnsRe'], $part5['question_'.$i][5]);
            }else{
                array_push($data['correctAnsRe'], '0');
            }   

        }

        for($i=0; $i<=$part6->end-$part6->begin; $i++){
            if (isset($part6['question_'.$i][4])) {
                array_push($data['correctAnsRe'], $part6['question_'.$i][4]);
            }else{
                array_push($data['correctAnsRe'], '0');
            }   
        }

        for($i=0; $i<=200-$part7->begin; $i++){
            if (isset($part7['question_'.$i][5])) {
                array_push($data['correctAnsRe'], $part7['question_'.$i][5]);
            }else{
                array_push($data['correctAnsRe'], '0');
            }   
        }
        return $data;

    }

    public function getUserAns(array $ans){
        $data['UserAnsLs'] = array();
        $data['UserAnsRe'] = array();

        for($i=1; $i<101; $i++){
            if (isset($ans['ans-ls-'.$i])) {
                array_push($data['UserAnsLs'], $ans['ans-ls-'.$i]);
            }else{
                array_push($data['UserAnsLs'], 'k');
            } 
        }

        for($i=101; $i<201; $i++){
            if (isset($ans['ans-re-'.$i])) {
                array_push($data['UserAnsRe'], $ans['ans-re-'.$i]);
            }else{
                array_push($data['UserAnsRe'], 'k');
            } 
        }

        return $data;
    }

    public function checkPoint(array $correct, array $ans)
    {
        $point['ls'] =0;
        $point['re'] =0;
        for($i=0; $i<100; $i++){
            if($correct['correctAnsLs'][$i] == $ans['UserAnsLs'][$i]){
                $point['ls'] ++;
            }
            if($correct['correctAnsRe'][$i] == $ans['UserAnsRe'][$i]){
                $point['re'] ++;
            }
        }
        return $point;
    }
}
