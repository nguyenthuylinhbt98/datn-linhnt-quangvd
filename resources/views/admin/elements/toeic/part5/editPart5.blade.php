@extends('admin.master')
@section('content')
<!--main-->
<div class="container main exam">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Cập nhật đề thi</h1>
        </div>
    </div> 
    @if(session('thongbao'))
       <div class="alert alert-success">
            {{ session('thongbao') }}
        </div>

    @endif
    <!--/.row-->
    <form action="/admin/toeic/part_5/{{$part5->id}}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="_method" value="put">
        <input type="hidden" name="id" value="{{$part5->id}}">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><i class="fas fa-user"></i>Thêm đề thi mới</div>
                    <div class="panel-body">
                        <div class="row justify-content-center" style="margin-bottom:40px">
                            <div class="col-md-8 col-lg-8 col-lg-offset-2">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input value="{{$part5->name}}" type="text" name="name" placeholder="Name test" class="form-control" required>
                                    {{ ShowErrors($errors, 'name')}}
                                </div>
                                <div class="container">
                                    <h2>Exam Toeic</h2>
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#part1">Part V</a></li>
                                        
                                    </ul>
                                    <div class="tab-content">

                                        {{-- Part5 --}}
                                        <div id="part1" class="tab-pane fade in active">
                                            <h3><b>PART V</b></h3>
                                            <p> <b>Directions:</b> A word or phrase is missing in each of the sentences below. Four answer choices are given below each sentence. Select the best answer to complete the sentence. Then mark the letter (A), (B), (C), or (D) on your answer sheet.</p>
                                            <label for="begin">
                                                Question begin: <input type="text" id="begin" name="begin" value="101">
                                            </label><br>
                                            <label for="end">
                                                Question end: <input type="text" id="end" name="end" value="130">
                                            </label><br><hr>
                                            
                                            @for($i=0; $i<=$part5->end-101; $i++)
                                                <b>Question {{$i+101}}:</b>
                                                <br>
                                                <input class="form-control" type="text" value="{{$part5->{'question_'.$i}[0]}}" name="question_{{$i}}[]" placeholder="Question {{$i+101}}"><br>
                                                <input class="form-control" type="text" value="{{$part5->{'question_'.$i}[1]}}" name="question_{{$i}}[]" placeholder="Answer A"><br>
                                                <input class="form-control" type="text" value="{{$part5->{'question_'.$i}[2]}}" name="question_{{$i}}[]" placeholder="Answer B"><br>
                                                <input class="form-control" type="text" value="{{$part5->{'question_'.$i}[3]}}" name="question_{{$i}}[]" placeholder="Answer C"><br>
                                                <input class="form-control" type="text" value="{{$part5->{'question_'.$i}[4]}}" name="question_{{$i}}[]" placeholder="Answer D">

                                                <b>Answer choice:</b>
                                                @if(isset($part5->{'question_'.$i}[5]))
	                                                <div class="row">
	                                                    <div class="col-md-1">
	                                                        <p style="float: left; margin-right: 5px;">A</p> <input @if($part5->{'question_'.$i}[5]=='A') checked @endif type="radio" name="question_{{$i}}[]" value="A">
	                                                    </div>
	                                                    <div class="col-md-1">
	                                                        <p style="float: left; margin-right: 5px;">B</p> <input @if($part5->{'question_'.$i}[5]=='B') checked @endif type="radio" name="question_{{$i}}[]" value="B">
	                                                    </div>
	                                                    <div class="col-md-1">
	                                                        <p style="float: left; margin-right: 5px;">C</p> <input @if($part5->{'question_'.$i}[5]=='C') checked @endif type="radio" name="question_{{$i}}[]" value="C">
	                                                    </div>
	                                                     <div class="col-md-1">
	                                                        <p style="float: left; margin-right: 5px;">D</p> <input @if($part5->{'question_'.$i}[5]=='D') checked @endif type="radio" name="question_{{$i}}[]" value="D">
	                                                    </div>
	                                                </div>
	                                            @else
	                                            	<div class="row">
	                                                    <div class="col-md-1">
	                                                        <p style="float: left; margin-right: 5px;">A</p> <input type="radio" name="question_{{$i}}[]" value="A">
	                                                    </div>
	                                                    <div class="col-md-1">
	                                                        <p style="float: left; margin-right: 5px;">B</p> <input type="radio" name="question_{{$i}}[]" value="B">
	                                                    </div>
	                                                    <div class="col-md-1">
	                                                        <p style="float: left; margin-right: 5px;">C</p> <input type="radio" name="question_{{$i}}[]" value="C">
	                                                    </div>
	                                                     <div class="col-md-1">
	                                                        <p style="float: left; margin-right: 5px;">D</p> <input type="radio" name="question_{{$i}}[]" value="D">
	                                                    </div>
	                                                </div>
	                                            @endif    
                                                <hr>
                                            @endfor
                                        </div>
                                        
                                        
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-lg-8 col-lg-offset-2 text-right"  style="margin-top: 20px;">
                                    <button class="btn btn-success"  type="submit">Cập nhật đề</button>
                                    <button class="btn btn-danger" type="reset">Huỷ bỏ</button>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!--/.row-->
</div>
<!--end main-->
@endsection

@section('script')
   
    
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
    <script>
        $('textarea').ckeditor();
    </script>
@endsection