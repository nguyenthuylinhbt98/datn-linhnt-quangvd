<?php

namespace App\Http\Controllers\Admin\Toeic;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Toeic\Part5;
use App\Http\Requests\Toeic\Part5\{AddPart5Request, EditPart5Request};
use App\Repositories\Admin\Toeic\Part5Repository;

class Part5Controller extends Controller
{
    private $part5Repository;

    public function __construct()
    {
        $this->part5Repository = new Part5Repository();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['part5'] = Part5::paginate(10);
        return view('admin.elements.toeic.part5.listPart5', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.elements.toeic.part5.addPart5');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddPart5Request $r)
    {
        $data = $r->all();
        $part_5 = $this->part5Repository->createPart5($data); 
        if($part_5){   
             return redirect('/admin/toeic/part_5')->with('thongbao', 'Success create new part V test'); 
        }  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['part5'] = $this->part5Repository->editPart5($id); 
        return view('admin.elements.toeic.part5.editPart5', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditPart5Request $r, $id)
    {
        $data = $r->all();
        $part_5 = $this->part5Repository->updatePart5($data, $id); 
        if($part_5){   
             return redirect('/admin/toeic/part_5')->with('thongbao', 'Success edit part V'); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Part5::destroy($id);
        return redirect()->back()->with('thongbao', 'Đã xóa thành công!!!');
    }
}
