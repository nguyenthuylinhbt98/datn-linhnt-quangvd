<?php 

namespace App\Http\Controllers\Admin\Users;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Users\User;
use App\Http\Requests\Users\{UserRequest, EditUserRequest};
use Hash;
use Illuminate\Support\Str;


class UserController extends Controller
{
    private $user;
    

    public function __construct() {
        $this->user = new User();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['admins'] = User::where('level', '1')->get();
        $data['users'] = User::where('level', '0')->orderBy('id','desc')->paginate(15);
        $data['teacher'] = User::where('level', '2')->orderBy('id','desc')->paginate(15);
        return view('admin.elements.user.list', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.elements.user.add');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UserRequest $r)
    {
        $data = $r->all();
        $data['avatar'] = 'upload/user_avatar/no_avatar.png';
        $data['password'] = Hash::make($r->password);
        $this->user->create($data);    
        return redirect('/admin/user/')->with('thongbao', 'Bạn vừa thêm một thành viên!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['user'] = User::find($id);
        return view('admin.elements.user.edit', $data);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditUserRequest $r, $id)
    {
        $user = User::find($id);
        $data = $r->all();

        if(isset($r->password)){
            $data['password'] = Hash::make($r->password);
        }else{
            $data['password'] = $user->password;
        }
        
        if($r->hasFile('avatar')){
            $data['avatar'] = UploadFile($r->avatar, 'user_avatar');
        }else{
            $data['avatar'] = $user->avatar;
        }
        $user->update($data);

        return redirect()->back()->with('thongbao', 'Bạn vừa chỉnh sửa thông tin cá nhân của '.$user->name);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user= User::find($id);
        $user->delete();
        return redirect('/admin/user/')->with('thongbao', 'Bạn vừa xóa một thành viên!');

    }
}
