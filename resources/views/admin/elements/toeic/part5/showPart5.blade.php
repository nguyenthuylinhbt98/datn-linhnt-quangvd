@extends('admin.master')
@section('content')
<!--main-->
<div class="container main exam">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Cập nhật đề thi</h1>
        </div>
    </div> 
    @if(session('thongbao'))
       <div class="alert alert-success">
            {{ session('thongbao') }}
        </div>

    @endif
    <!--/.row-->
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><i class="fas fa-user"></i>{{$part_5->name}}</div>
                    <div class="panel-body">
                        <div class="row justify-content-center" style="margin-bottom:40px">
                            <div class="col-md-8 col-lg-8 col-lg-offset-2">
                               
                                <div class="container">
                                    
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#part1">Part V</a></li>
                                        
                                    </ul>
                                    <div class="tab-content">

                                        {{-- Part_3 --}}
                                        <div id="part1" class="tab-pane fade in active">
                                            <h3><b>PART V</b></h3>
                                            <p> <b>Directions:</b> A word or phrase is missing in each of the sentences below. Four answer choices are given below each sentence. Select the best answer to complete the sentence. Then mark the letter (A), (B), (C), or (D) on your answer sheet.</p>
                                            <br><b>Voice: </b><br>

                                            <input type="file" name="voice_part3"><br>

                                            @for($i=101; $i<141; $i++)
                                                <b>Question {{$i}}:</b>{{$question[$i-101][0]}}<br>
                                                <p>A.{{$question[$i-101][1]}}</p>
                                                <p>B.{{$question[$i-101][2]}}</p>
                                                <p>C.{{$question[$i-101][3]}}</p>
                                                <p>D.{{$question[$i-101][4]}}</p>

                                                <hr>
                                            @endfor
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                           
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

    <!--/.row-->
</div>
<!--end main-->
@endsection

