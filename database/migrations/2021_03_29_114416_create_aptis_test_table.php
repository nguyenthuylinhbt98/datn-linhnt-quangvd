<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAptisTestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aptis_test', function (Blueprint $table) {
            $table->id();
            $table->string('name_test')->nullable();
            $table->string('level')->nullable();
            $table->string('role')->nullable();
            $table->string('classify')->nullable();
            $table->string('read_id')->nullable();
            $table->string('write_id')->nullable();
            $table->string('listen_id')->nullable();
            $table->string('speak_id')->nullable();
            $table->string('gv_id')->nullable();
            $table->integer('view')->default(0);
            $table->integer('test')->default(0);
            $table->longText('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aptis_test');
    }
}
