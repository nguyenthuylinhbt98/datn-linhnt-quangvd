<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class User extends Model
{
    protected $fillable = [
        'email',
        'avatar',
        'password',
        'full',
        'sex',
        'date',
        'address',
        'phone',
        'level',
        'facebook'
    ];
    public function user_and_aptis() {
    	return $this -> hasMany('App\Models\Users\User_And_Aptis','user_id','id');
    }
}
