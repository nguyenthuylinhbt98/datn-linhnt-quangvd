<?php

namespace App\Repositories\Admin\Toeic;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Models\Toeic\Part3;
//use Your Model

/**
 * Class Part3Repository.
 */
class Part3Repository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Part3::class;
    }
    // 
    // 
    // ----------create--------
    // 
    // 

    public function createPart3(array $data)
    {
        $part_3['name'] = $data['name'];
        $part_3['begin'] = $data['begin'];
        $part_3['end'] = $data['end'];

        if(isset($data['void_part3'])){
            $part_3['void_part3'] = UploadFile($data['void_part3'], 'mp3/toeic_part3');
        }

        for ($i=0; $i < 40; $i++) {
            if(!empty($data['question_'.$i])){
                $part_3['question_'.$i]=implode('; ;',(array)$data['question_'.$i]);
            }
        }
        Part3::create($part_3);        
        return true;
    }
    // 
    // 
    // -----------------show edit----------
    // 
    // 

    public function editPart3(int $id)
    {
        $part_3 = self::getById($id);
       
        for ($i=0; $i < 40; $i++) {
            (array)$part_3['question_'.$i]=explode('; ;',$part_3->{'question_'.$i});
        }
        return $part_3;
    }
    // 
    // 
    // ------------update-------
    // 
    // 

    public function updatePart3(array $data, int $id)
    {
        $part3 = self::getById($id);
        
        $part_3['name'] = $data['name'];
        $part_3['begin'] = $data['begin'];
        $part_3['end'] = $data['end'];

        if(isset($data['void_part3'])){
            $part_3['void_part3'] = UploadFile($data['void_part3'], 'mp3/toeic_part3');
        }

        for ($i=0; $i < 40; $i++) {
            if(!empty($data['question_'.$i])){
                $part_3['question_'.$i]=implode('; ;',(array)$data['question_'.$i]);
            }
        }
        $part3->update($part_3);        
        return true;
    }
}
