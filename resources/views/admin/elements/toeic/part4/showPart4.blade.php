@extends('admin.master')
@section('content')
<!--main-->
<div class="container main exam">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Preview {{$part4->name}}</h1>
        </div>
    </div> 
    @if(session('thongbao'))
       <div class="alert alert-success">
            {{ session('thongbao') }}
        </div>

    @endif
    <!--/.row-->
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><i class="fas fa-user"></i>{{$part4->name}}</div>
                    <div class="panel-body">
                        <div class="row justify-content-center" style="margin-bottom:40px">
                            <div class="col-md-8 col-lg-8 col-lg-offset-2">
                               
                                <div class="container">
                                    
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#part1">Part IV</a></li>
                                        
                                    </ul>
                                    <div class="tab-content">
                                        <div id="part1" class="tab-pane fade in active">
                                             <h3><b>PART IV</b></h3>
                                             <p> <b>Directions:</b> You will hear some talks given by a single speaker. You will be asked to answer three questions about what the speaker says in each talk. Select the best response to each question and mark the letter (A), (B), (C), or (D) on your answer sheet. The talks will not be printed in your test book and will be spoken only one time.</p>
                                            <br><b>Voice: </b><br>

                                            <audio src="../{{$part4->void_part4}}" loop controls></audio>
                                            <br>
                                            @for($i=71; $i<101; $i++)
                                                <b>Question {{$i}}:</b>{{$part4->{'question_'.$i}[0]}}<br>
                                                <p>A.{{$part4->{'question_'.$i}[1]}}</p>
                                                <p>B.{{$part4->{'question_'.$i}[2]}}</p>
                                                <p>C.{{$part4->{'question_'.$i}[3]}}</p>
                                                <p>D.{{$part4->{'question_'.$i}[4]}}</p>

                                                <hr>
                                            @endfor
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>

    <!--/.row-->
</div>
<!--end main-->
@endsection

