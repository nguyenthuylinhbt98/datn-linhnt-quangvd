<?php

namespace App\Repositories\Admin\Aptis;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Models\Aptis\Listen;


/**
 * Class ListenRepository.
 */
class ListenRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Listen::class;
    }

    public function create($data){
        for ($i=0; $i < 25; $i++) {
            if(!empty($data['voice_question_'.$i])){
                $data['voice_question_'.$i] = UploadFile($data['voice_question_'.$i], 'mp3/aptis_listen');
            }else{
                $data['voice_question_'.$i] = "";
            }
            $data['question_listen_'.$i]=implode('; ;',(array)$data['question_listen_'.$i]);
        }
        Listen::create($data);        
        return true;
    }

    public function edit($id){
        $listen = self::getById($id);
        for ($i=0; $i < 25; $i++) {
            (array) $listen['question_listen_'.$i]=explode('; ;',$listen['question_listen_'.$i]);
        }
        return $listen;
    }

    public function update($data, $id){
        $listen = self::getById($id);
        for ($i=0; $i < 25; $i++) {
            if(!empty($data['voice_question_'.$i])){
                $data['voice_question_'.$i] = UploadFile($data['voice_question_'.$i], 'mp3/aptis_listen');
            }else{
                $data['voice_question_'.$i] = $listen->{'voice_question_'.$i};
            }
            $data['question_listen_'.$i]=implode('; ;',(array)$data['question_listen_'.$i]);
        }
        $listen->update($data);
        return true;
    }
}
