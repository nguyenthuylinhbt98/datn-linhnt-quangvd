@extends('admin.master')
@section('admin', 'class=active')
@section('content')
<!--main-->
<div class="container main user_edit">
    <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
            <div class="x_panel">
                <form action="/admin/user/{{$user->id}}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
                	@csrf
                    <input type="hidden" name="_method" value="put">
                    <input type="hidden" name="id" value="{{$user->id}}">
					 @if(session('thongbao'))
		                  <div class="alert alert-success">
		                      {{ session('thongbao') }}
		                  </div>
		                @endif
                	
                	<div class="x_content">
                    	<h3 class="price" style="font-size: 30px;">User: {{ $user->full }}</h3><hr>
	                    <div class="col-md-8 col-sm-6 col-xs-12" style="border:0px solid #e5e5e5;">
	                        
	          
	                        <div class="">
	                            <div class="product_price">
	                            	<br>
									<input type="hidden" name="id" value="{{$user->id}}">
	                            	<div class="row" >
	                            		<div class="col-md-6">
	                            			<label> <b>Họ và tên: </b><input type="" name="full" value="{{$user->full}}"><hr></label>
	                            			{{ ShowErrors($errors, 'full')}}

	                            			<label> <b>Email: </b><input type="email" name="email" value="{{$user->email}}"><hr></label>
	                            			{{ ShowErrors($errors, 'email')}}

	                            			<label> <b>Address: </b><input type="" name="address" value="{{$user->address}}"><hr></label>

	                            			<label> <b>Password: </b><input type="password" name="password" value="" placeholder=""><hr></label>
	                            			{{ ShowErrors($errors, 'password')}}
	                            				
	                            			
	                            		</div>
	                            		<div class="col-md-6">
	                            			<label> <b>Date of birth: </b><input type="date" name="date_of_birth" value="{{$user->date_of_birth}}"><hr></label>

	                            			<label> <b>Số điện thoại: </b><input type="" name="phone" value="{{$user->phone}}"><hr></label>
	                            			{{ ShowErrors($errors, 'phone')}}

	                            			<label> <b>Facebook: </b><input type="" name="facebook" value="{{$user->facebook}}" placeholder="Link Facebook"><hr></label>
	                            			<label> <b>Level: </b>

	                            				<select name="role" style=" width: 270px; padding: 5px; border-radius: 10px;">
	                            					<option value="0" @if($user->role == 0) selected @endif>User</option>
	                            					<option value="1" @if($user->role == 1) selected @endif>Admin</option>
		                            			</select>
	                            				
	                            			</label>
	                            			
	                            		</div>
	                            		
	                            	</div>
	                            	
	                            </div>
	                        </div>
	                         
	                    </div>

	                    <div class="col-md-4 col-sm-6 col-xs-12">
	                        <div class="product-image">
								<br><br><br>
	                            <input id="for_img" type="file" name="avatar" class="form-control hidden" onchange="changeImg(this)">
					            <div class="avatar_img" align="center">	
					            	@if(isset($user->avatar))				            	
				                   		<label for="for_img"> <img id="avatar" src="../{{ $user->avatar }}" style="width: 200px; height: 200px"></label> 
				                   	@else
				                   		<label for="for_img"> <img id="avatar" src="../upload/user_avatar/no_avatar.png" style="width: 200px; height: 200px"></label> 
				                   	
				                   	@endif	

					            </div>
	                        </div>
							<br><br><br>
	                        <div align="center">
		                		<button type="submit" class="btn btn-primary">Xác nhận chỉnh sửa thông tin</button>
		                		
		                	</div>
	                    </div>
                    
                	</div>
                	
                </form>
            </div>
        </div>
    </div>
</div>
<!--end main-->
@endsection
@section('script')

<script>
	  function changeImg(input){
            //Nếu như tồn thuộc tính file, đồng nghĩa người dùng đã chọn file mới
            if(input.files && input.files[0]){
                var reader = new FileReader();
                //Sự kiện file đã được load vào website
                reader.onload = function(e){
                    //Thay đổi đường dẫn ảnh
                    $('#avatar').attr('src',e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }
        $(document).ready(function() {
            $('#avatar').click(function(){
                $('#for_img').click();
            });
        });
</script>
@endsection