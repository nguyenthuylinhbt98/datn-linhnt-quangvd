@extends('user.master')

@section('css')
	<link rel="stylesheet" href="assets/css/test.css">
    <style>
        .question img {
            width: 250px;
            height: 150px;
            object-fit: cover
        }
    </style>
@endsection


@section('content')
    <main class="aptis" id="aptis-results">
        <!--? slider Area Start-->
        <section class="slider-area slider-area2">
            <div class="slider-active">
                <!-- Single Slider -->
                <div class="single-slider slider-height2">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-8 col-lg-11 col-md-12">
                                <div class="hero__caption hero__caption2">
                                    <h1 data-animation="bounceIn" data-delay="0.2s">{{ $results->test->name_test }}</h1>
                                    <nav aria-label="breadcrumb">
                                        <ol class="breadcrumb">
                                            <li class="breadcrumb-item"><a href="/">Home</a></li>
                                            <li class="breadcrumb-item"><a href="/aptis">Aptis</a></li>
                                            <li class="breadcrumb-item"><a href="/aptis/test/{{ $results->test->id }}">{{ $results->test->name_test }}</a></li>
                                            <li class="breadcrumb-item"><a>View Results</a></li>
                                        </ol>
                                    </nav>
                                </div>
                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </section>
        <section class=" test">
            <div class="container">
                <ul class="nav nav-pills nav-justified" id="pills-tab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#part1" role="tab" aria-controls="pills-home" aria-selected="true">Listening </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#part2" role="tab" aria-controls="pills-profile" aria-selected="false">Reading</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#part3" role="tab" aria-controls="pills-profile" aria-selected="false">Writing</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#part4" role="tab" aria-controls="pills-profile" aria-selected="false">Speaking </a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#part5" role="tab" aria-controls="pills-profile" aria-selected="false">Grammar & Vocabulary</a>
                    </li>
                </ul>
                
                <div class="tab-content" id="pills-tabContent">
                    <div class="tab-pane fade show active" id="part1" role="tabpanel" aria-labelledby="pills-home-tab">
                        <div class="row">
                            @for ($i = 0; $i < 25; $i++)
                                <div class="col-md-4 question">
                                    <audio src="../{{ $listen['voice_question_'.$i] }}"  loop controls></audio><br>
                                    <p> <b>{{ $i+1 }}.</b> {{ $listen['question_listen_'.$i][0] }}</p>
                                    <p class="{{ $results['listening_answer'][$i] == 'A' ? 'text-danger' : '' }} {{ $listen['question_listen_'.$i][1] == 'A' ? 'text-success' : '' }}">A.{{$listen['question_listen_'.$i][2]}}</p>
                                    <p class="{{ $results['listening_answer'][$i] == 'B' ? 'text-danger' : '' }} {{ $listen['question_listen_'.$i][1] == 'B' ? 'text-success' : '' }}">B.{{$listen['question_listen_'.$i][3]}}</p>
                                    <p class="{{ $results['listening_answer'][$i] == 'C' ? 'text-danger' : '' }} {{ $listen['question_listen_'.$i][1] == 'C' ? 'text-success' : '' }}">C.{{$listen['question_listen_'.$i][4]}}</p>
                                    <p class="{{ $results['listening_answer'][$i] == 'D' ? 'text-danger' : '' }} {{ $listen['question_listen_'.$i][1] == 'D' ? 'text-success' : '' }}">D.{{$listen['question_listen_'.$i][5]}}</p>
                                    @if($results['listening_answer'][$i] == $listen['question_listen_'.$i][1])
                                        <span class="text-primary">1/1 (correct)</span>
                                    @else
                                        <span class="text-warning">0/1 (fail)</span>
                                    @endif
                                </div>
                            @endfor
                        </div>
                        
                    </div>
                    <div class="tab-pane fade show" id="part2" role="tabpanel" aria-labelledby="pills-home-tab">
                        <div class="question">
                            <h2>Part I</h2>
                            <div class="row">
                                <div class="col-md-6">
                                    {!! $read->p1_para !!}
                                </div>
                                <div class="col-md-3">
                                    <p>Your answer</p>
                                    @foreach ($results['reading_answer_p1'] as $key=>$item)
                                        <p> <b>{{ $key+1 }}.</b>{{ $item }}</p>
                                    @endforeach
                                </div>
                                <div class="col-md-3">
                                    <p>Correct answer</p>
                                </div>
                            </div>
                        </div>
                        <div class="question">
                            <h2>Part II</h2>
                            <div class="row">
                                <div class="col-md-6">
                                    {!! $read->{'p2_ques1'}[1] !!}
                                    <p>Your answer: {{ $results['reading_answer_p2'][0] }}</p> 
                                    <p>Correct answer: {{$read->{'p2_ques1'}[2]}}</p>
                                </div>
                                <div class="col-md-6">
                                    {!! $read->{'p2_ques2'}[1] !!} 
                                    <p>Your answer: {{ $results['reading_answer_p2'][0] }}</p> 
                                    <p>Correct answer: {{$read->{'p2_ques2'}[2]}}</p>
                                </div>
                               
                            </div>
                        </div>
                        <div class="question">
                            <h2>Part III</h2>
                            <div class="row">
                                <div class="col-md-6">
                                    {!! $read->p3_para !!}
                                </div>
                                <div class="col-md-3">
                                    <p>Your answer</p> 
                                    @for($i=0; $i<16; $i+=2)
                                        <b>{{ $read->p3_ques[$i] }}</b>
                                        <p>{{ $results['reading_answer_p3'][$i/2] }}</p>
                                    @endfor
                                </div>
                                <div class="col-md-3">
                                    <p>Correct answer</p> 
                                    @for($i=0; $i<16; $i+=2)
                                        <b>{{ $read->p3_ques[$i] }}</b>
                                        <p>{{ $read->p3_ques[$i+1] }}</p>
                                    @endfor
                                </div>
                               
                            </div>
                        </div>
                        <div class="question">
                            <h2>Part IV</h2>
                            <div class="row">
                                <div class="col-md-6">
                                    {!! $read->p4_para !!}
                                </div>
                                <div class="col-md-3">
                                    <p>Your answer</p> 
                                    @foreach ( $results['reading_answer_p4'] as $key=>$item)
                                        <p> <b>{{ $key+1 }}.</b> {{ $item }}</p>
                                    @endforeach
                                </div>
                                <div class="col-md-3">
                                    <p>Correct answer</p> 
                                   
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade show" id="part3" role="tabpanel" aria-labelledby="pills-home-tab">
                        <div class="question">
                            <h2>Part I</h2>
                            <div class="row">
                                @for($i=1; $i<6; $i++)
                                    <div class="col-md-4 ">
                                        <b>{{$i}}.{{$write->{'p1_ques'.$i}[0]}}</b>
                                        <p><i> Your answer:</i> {{ $results['writing_answer'][$i-1] }} </p>
                                        <p><i>Suggestions:</i> {{ $write->{'p1_ques'.$i}[1] }}</p>
                                        <hr>
                                    </div>
                                @endfor
                            </div>
                        </div>
                        <div class="question">
                            <h2>Part II</h2>
                            <div class="row">
                                <div class="col-md-12">
                                    <b>{!! $write->p2[0] !!}</b>
                                    <p><i> Your answer:</i>  {{ $results['writing_answer'][5] }} </p>
                                    <p><i>Suggestions:</i> {!! $write->p2[1] !!}</p>
                                </div>
                            </div>
                        </div>
                        <div class="question">
                            <h2>Part III</h2>
                            <div class="row">
                                @for($i=1; $i<4; $i++)
                                    <div class="col-md-4 ">
                                        <b>{{$i}}.{!! $write->{'p3_ques'.$i}[0] !!}</b>
                                        <p><i> Your answer:</i> {{ $results['writing_answer'][$i+5] }} </p>
                                        <p><i>Suggestions:</i> {!! $write->{'p3_ques'.$i}[1] !!}</p>
                                    </div>
                                @endfor
                            </div>
                        </div>
                        <div class="question">
                            <h2>Part IV</h2>
                            <div class="row">
                                <div class="col-md-12">
                                    <p>{!! $write->p4_subject !!}</p>
                                    <b>Informal</b>
                                    <p><i> Your answer:</i> {{ $results['writing_answer'][9] }}  </p>
                                    <p><i>Suggestions:</i> {!! $write->p4_ques1 !!}</p>
                                    <hr>
                                    <b>Formal</b>
                                    <p><i> Your answer:</i>  {{ $results['writing_answer'][10] }} </p>
                                    <p><i>Suggestions:</i> {!! $write->p4_ques2 !!}</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade show" id="part4" role="tabpanel" aria-labelledby="pills-home-tab">
                        <div class="question">
                            <h2>Part I</h2>
                            <div class="row">
                                @for ($i = 1; $i < 4; $i++)
                                    <div class="col-md-4">
                                        <b>{{ $i }}. {{ $speak['p1_ques'.$i][0] }}</b>
                                        <p><i>Suggestions:</i> {!! $speak['p1_ques'.$i][1] !!}</p>
                                    </div>
                                @endfor
                            </div>
                        </div>
                        <div class="question">
                            <h2>Part II</h2>
                            <img src="../{{ $speak['img_p2'] }}" alt="">
                            <div class="row">
                                @for ($i = 1; $i < 4; $i++)
                                    <div class="col-md-4">
                                        <b>{{ $i }}. {{ $speak['p2_ques'.$i][0] }}</b>
                                        <p><i>Suggestions:</i> {!! $speak['p2_ques'.$i][1] !!}</p>
                                    </div>
                                @endfor
                            </div>
                        </div>
                        <div class="question">
                            <h2>Part III</h2>
                            <img src="../{{ $speak['img_p3'] }}" alt="">
                            <div class="row">
                                @for ($i = 1; $i < 4; $i++)
                                    <div class="col-md-4">
                                        <b>{{ $i }}. {{ $speak['p3_ques'.$i][0] }}</b>
                                        <p><i>Suggestions:</i> {!! $speak['p3_ques'.$i][1] !!}</p>
                                    </div>
                                @endfor
                            </div>
                        </div>
                        <div class="question">
                            <h2>Part IV</h2>
                            <div class="row">
                                <div class="col-md-4">
                                    <img src="../{{ $speak['img_p4'] }}" alt="">
                                </div>
                                <div class="col-md-8">
                                    <b>{{ $speak['p4_ques'][0] }}</b>
                                    <p><i>Suggestions:</i> </p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="tab-pane fade show" id="part5" role="tabpanel" aria-labelledby="pills-home-tab">
                        <div class="row">
                            {{-- Grammar --}}
                            @for ($i = 0; $i < 25; $i++)
                                <div class="col-md-4 question">
                                    <p> <b>{{ $i+1 }}.{{ $gv['question_grammar_'.$i][0] }}</b> </p>
                                    <p >A.{{$gv['question_grammar_'.$i][2]}}</p>
                                    <p >B.{{$gv['question_grammar_'.$i][3]}}</p>
                                    <p >C.{{$gv['question_grammar_'.$i][4]}}</p>
                                    <p >D.{{$gv['question_grammar_'.$i][5]}}</p>
                                </div>
                            @endfor
                        </div>
                        <div class="row">
                            {{-- Vocabulary --}}
                            @for ($j = 26; $j < 31; $j++)
                                <div class="col-md-6 question">
                                    <b>
                                        @php
                                            switch( $j ) {
                                                case 26: echo 'Smiller meaning'; break;
                                                case 27: echo 'Opposite meaning'; break;
                                                case 28: echo 'Complete sentence'; break;
                                                case 29: echo 'Words matching'; break;
                                                case 30: echo 'Word combinations'; break;
                                            }
                                        @endphp
                                    </b>
                                    <table>
                                        <tr>
                                            <td></td>
                                            <td><small><b>Your answer</b></small></td>
                                            <td><small><b>Correct Answer</b> </small></td>
                                        </tr>
                                        @foreach($gv['p'.$j.'_list_question'] as $key=>$item)
                                            <tr>
                                                <td>{{ $item }}</td>
                                                <td>
                                                    <span>............</span>
                                                </td>
                                                <td>
                                                    <span>{{ $gv['p26_list_correct_answer'][$key] }}</span>
                                                </td>
                                            </tr>
                                        @endforeach
                                    </table>
                                </div>
                            @endfor
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </main>

@endsection


@section('script')

@endsection
