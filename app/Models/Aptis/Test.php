<?php

namespace App\Models\Aptis;

use Illuminate\Database\Eloquent\Model;

class Test extends Model
{
    protected $table = 'aptis_test';

    protected $guarded = [
        'id'
    ];
    public function listening(){
        return $this->belongsTo('App\Models\Aptis\Listen', 'listen_id', 'id');
    }
    public function reading(){
        return $this->belongsTo('App\Models\Aptis\Read', 'read_id', 'id');
    }
    public function writing(){
        return $this->belongsTo('App\Models\Aptis\Write', 'write_id', 'id');
    }
    public function grammar(){
        return $this->belongsTo('App\Models\Aptis\GV', 'gv_id', 'id');
    }
    public function speaking(){
        return $this->belongsTo('App\Models\Aptis\Speak', 'speak_id', 'id');
    }
    public function aptis_and_user() {
    	return $this -> hasMany('App\Models\Users\User_And_Aptis','aptis_id','id');
    }
   
}
