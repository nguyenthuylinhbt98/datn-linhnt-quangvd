@extends('admin.master')
@section('content')
<!--main-->
<div class="container main exam">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Add new reading test</h1>
        </div>
    </div> 
   @if(session('thongbao'))
       <div class="alert alert-success">
            {{ session('thongbao') }}
        </div>

    @endif
    <!--/.row-->
    <form action="/admin/aptis/reading" method="post" accept-charset="utf-8" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><i class="fas fa-user"></i>Add reading test</div>
                    <div class="panel-body">
                        <div class="row justify-content-center" style="margin-bottom:40px">
                            <div class="col-md-8 col-lg-8 col-lg-offset-2">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input value="{{old('name')}}" type="text" name="name" placeholder="Name test" class="form-control" required>
                                    {{ ShowErrors($errors, 'name')}}
                                </div>
                                <div class="container">
                                    <h2>Exam </h2>
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#part1">Part I</a></li>
                                        <li><a data-toggle="tab" href="#part2">Part II</a></li>
                                        <li><a data-toggle="tab" href="#part3">Part III</a></li>
                                        <li><a data-toggle="tab" href="#part4">Part IV</a></li>
                                        
                                    </ul>
                                    <div class="tab-content">
                                        <div id="part1" class="tab-pane fade in active">
                                            <textarea name="p1_para" class="ckeditor" ></textarea>
                                            Example: <input class="form-control" type="text" name="example">
                                            @for($i=1; $i<8; $i++)
                                                <div class="col-md-3">
                                                    <br>
                                                    <label for="ques1">{{$i}}.</label>
                                                    <input class="form-control p1_ques{{$i}}" type="text" name="p1_ques{{$i}}[]"><br>
                                                    <input class="form-control p1_ques{{$i}}" type="text" name="p1_ques{{$i}}[]"><br>
                                                    <input class="form-control p1_ques{{$i}}" type="text" name="p1_ques{{$i}}[]"><br>
                                                    <input class="form-control p1_ques{{$i}}" type="text" name="p1_ques{{$i}}[]"><br>
                                                    <span>Correct Answer</span>
                                                    <select name="p1_ques{{$i}}[]" id="p1_ques{{$i}}" class="form-control">
                                                        <option value="">gvghh</option>
                                                    </select>
                                                </div>
                                            @endfor
                                        </div>                                       
                                        <div id="part2" class="tab-pane fade ">
                                            @for($i=1; $i<3; $i++)
                                                <p>{{$i}}.</p>
                                                <input class="form-control" type="text" name="p2_ques{{$i}}[]" placeholder="title"><br>
                                                <textarea name="p2_ques{{$i}}[]" class="ckeditor" ></textarea>
                                                <input class="form-control" type="text" name="p2_ques{{$i}}[]" placeholder="Answer"> <br>
                                            @endfor
                                            
                                        </div>                                       
                                        <div id="part3" class="tab-pane fade ">
                                            <textarea name="p3_para" class="ckeditor" ></textarea><br>
                                            <input class="form-control" type="text" name="p3_title">
                                            <p>List Answer:</p>
                                            @for($i=1; $i<5; $i++)
                                                <input class="form-control" type="text" name="p3_ans[]"><br>
                                            @endfor
                                            <p>Ques_Answer:</p>
                                            <table width="100%">
                                                @for($i=1; $i<10; $i++)
                                                    <tr>
                                                        <td>
                                                            <input class="form-control" type="text" name="p3_ques[]" value="{{$i}}.">
                                                        </td>
                                                        <td>
                                                            <input class="form-control" type="text" name="p3_ques[]">
                                                        </td>
                                                    </tr>
                                                @endfor
                                            </table>
                                            
                                        </div>                                       
                                        <div id="part4" class="tab-pane fade ">
                                            <textarea name="p4_para" class="ckeditor" ></textarea><br>
                                            <input class="form-control" type="text" name="p4_title">                                           
                                            <p>List Answer:</p>
                                            @for($i=1; $i<10; $i++)
                                                <div class="col-md-4">
                                                    {{$i}}.<input class="form-control list_ans_p4" type="text" name="p4_ans[]" placeholder="{{$i}}." value=""><br>
                                                </div>
                                            @endfor
                                            <br> Correct answer
                                            <table>
                                                @for ($i = 1; $i < 7; $i++)
                                                    <tr>
                                                        <td>{{$i}}.</td>
                                                        <td>
                                                            <select name="" class="form-control list_answer_p4">
                                                            </select>
                                                        </td>
                                                    </tr>
                                                @endfor
                                            </table>
                                        </div>                                       
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-lg-8 col-lg-offset-2 text-right"  style="margin-top: 20px;">
                                    <button class="btn btn-success"  type="submit">Cập nhật đề</button>
                                    <button class="btn btn-danger" type="reset">Huỷ bỏ</button>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!--/.row-->
</div>
<!--end main-->
@endsection

@section('script')
    <script src="./../ckeditor/ckeditor.js"></script>
    <script>
        $('#part1 input').on('change', function(){
            for(i=1; i<8; i++){
                let list_ans = $(`.p1_ques${i}`)
                $(`#p1_ques${i}`).html("");
                for(j=0 ; j<list_ans.length; j++) {
                    $(`#p1_ques${i}`).append(
                        `<option value="${list_ans.eq(j).val()}">${list_ans.eq(j).val()}</option> `
                    )
                }
            }
        })

        $('#part4 input').on('change', function(){
            let list_ans_p4 = $('.list_ans_p4')
            $(`list_answer_p4`).html("");
            for(j=0 ; j<list_ans_p4.length; j++) {
                $(`list_answer_p4`).append(
                    `<option value="${list_ans_p4.eq(j).val()}">${list_ans_p4.eq(j).val()}</option> `
                )
            }
        })


    </script>
@endsection