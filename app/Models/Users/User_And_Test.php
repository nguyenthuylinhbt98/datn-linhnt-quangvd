<?php

namespace App\Models\Users;

use Illuminate\Database\Eloquent\Model;

class User_And_Test extends Model
{
    protected $table = 'users_and_test';

    protected $fillable = [
        'user_id', 'test_id', 'score'
    ];
}
