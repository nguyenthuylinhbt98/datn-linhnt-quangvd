<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Http\Requests\Users\{UserRequest, EditUserRequest};
use App\Models\Users\User;
use App\Models\Note;

class ProfileController extends Controller
{
    public function getProfile($id){
        $data['note'] = Note::where('user_id', $id);
        return view('user.elements.profile.index', $data);
    }

    public function getEditProfile($id){
        $data = User::find($id);
        return view('user.elements.profile.edit', $data);
    }


    public function postEditProfile(EditUserRequest $r, $id){
        $user = User::find($id);

        $data = $r->all();

        if(isset($r->password)){
            $data['password'] = Hash::make($r->password);
        }else{
            $data['password'] = $user->password;
        }

        
        if($r->hasFile('avatar')){
            $data['avatar'] = UploadFile($r->avatar, 'user_avatar');
            // dd($data['avatar']);
        }else{
            $data['avatar'] = $user->avatar;
        }
        $user->update($data);

        return redirect()->back()->with('thongbao', 'Bạn vừa chỉnh sửa thông tin cá nhân của '.$user->name);
    }

}
