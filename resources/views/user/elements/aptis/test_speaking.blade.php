@extends('user.master')

@section('css')
	<link rel="stylesheet" href="assets/css/test.css">
    <style>
        label{
            float: left;
        }
        select{
            max-width: 90%;
        }
    </style>
@endsection


@section('content')
    
<main class="aptis" id="aptis-speaking">
    <!--? slider Area Start-->
    <section class="slider-area slider-area2">
        <div class="slider-active">
            <!-- Single Slider -->
            <div class="single-slider slider-height2">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-8 col-lg-11 col-md-12">
                            <div class="hero__caption hero__caption2">
                                <h1 data-animation="bounceIn" data-delay="0.2s">{{$name}}</h1>
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                                        <li class="breadcrumb-item"><a href="/aptis">Aptis</a></li>
                                        <li class="breadcrumb-item"><a href="/aptis/test/{{ $id }}">{{$name}}</a></li>
                                        <li class="breadcrumb-item"><a>Listening</a></li>
                                    </ol>
                                </nav>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class=" test">
       <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-12 col-lg-12">
                <div class="section-tittle  mb-10 mt-20">
                   <p>Welcome to the Aptis Listening Test. <br>
                    You will listen to <b>twenty five short recordings.</b><br>
                    Click on the play button to listen to each recording. 
                    You can listen to each recording two times only. <br>
                    You have <b> 56 minutes to complete the test</b>.<br>
                </div>
            </div>
            
            <div class="col-xl-12 col-lg-12">
                <form action="" method="post">
                    @csrf
                    <div class="row">
                        <div class="col-xl-9 col-lg-10">
                            <div class="part1 speaking-part block">
                                <span><b>Part 1.</b></span>
                                @for ($i = 1; $i < 4; $i++)
                                    <p class="part1-question {{ 'part1-question-'.$i }} {{ $i==1 ? 'block' : '' }}">{{ $i }}. {{ $speak['p1_ques'.$i][0] }}</p>
                                @endfor
                            </div>
                            <div class="part2 speaking-part">
                                <span><b>Part 2.</b></span> <br>
                                <img src="../{{ $speak['img_p2'] }}" alt="">
                                @for ($i = 1; $i < 4; $i++)
                                    <p class="part2-question {{ 'part2-question-'.$i }} {{ $i==1 ? 'block' : '' }}">{{ $i }}. {{ $speak['p2_ques'.$i][0] }}</p>
                                @endfor
                            </div>
                            <div class="part3 speaking-part">
                                <span><b>Part 3.</b></span> <br>
                                <img src="../{{ $speak['img_p3'] }}" alt="">
                                @for ($i = 1; $i < 4; $i++)
                                    <p  class="part3-question {{ 'part3-question-'.$i }} {{ $i==1 ? 'block' : '' }}">{{ $i }}. {{ $speak['p3_ques'.$i][0] }}</p>
                                @endfor
                            </div>
                            <div class="part4 speaking-part">
                                <span><b>Part 4.</b></span> <br>
                                <img src="../{{ $speak['img_p4'] }}" alt="">
                                <p>{{ $speak['p4_ques'][0] }}</p>
    
                            </div>
                        </div>
                        <div class="col-xl-3 col-lg-2">
                            <div class="time-ans">
                                <i class="far fa-clock"></i>
                                0<span id="h">0</span> :
                                0<span id="m">0</span> :
                                <span id="s">00</span>
                            </div>
                        </div>
                    </div>
                    <div class="sub d-flex justify-content-center mb-3">
                        <input type="submit" class="genric-btn primary-border circle" value="Submit"/>
                    </div>
                </form>
            </div>
        </div>
       </div>

    </section>
</main>

@endsection


@section('script')
<script>
    var h = 00; // Giờ
    var m = 00; // Phút
    var s = 30; // Giây
    var timeout = null;
    var count = 1;

    function start()
    {
        if (s === -1){
            m -= 1;
            s = 59;
        }

        if (m === -1){
            h -= 1;
            m = 59;
        }

        if (h == -1){
            count++;
            h = 00;
            m = 00;
            switch (count) {
                case 2: 
                    s = 30;
                    showQuestionPart1(2);
                    break;
                case 3:
                    s = 30;
                    showQuestionPart1(3);
                    break;
                case 4:
                    s = 45;
                    showPart(2);
                    break;
                case 5:
                    s=45;
                    showQuestionPart2(2);
                    break;
                case 6:
                    s=45;
                    showQuestionPart2(3);
                    break;
                case 7:
                    s = 45;
                    showPart(3);
                    break;
                case 8:
                    s=45;
                    showQuestionPart3(2);
                    break;
                case 9:
                    s=45;
                    showQuestionPart3(3);
                    break;
                case 10:
                    m= 01;
                    s = 00;
                    showPart(4);
                    break;
                case 11:
                    m= 02;
                    s = 00;
                    break;
                case 12:
                    clearTimeout(timeout);
                    $('form').submit();
                    break;

            }
        }

        document.getElementById('h').innerText = h.toString();
        document.getElementById('m').innerText = m.toString();
        document.getElementById('s').innerText = s.toString();

        timeout = setTimeout(function(){
            s--;
            start();
        }, 1000);
    }
    start();

    function activeAns(id){
        $(`.ques-${id}`).addClass('active')
    }
    
    function showPart(id){
        $(`.speaking-part`).removeClass('block');
        $(`.part${id}`).addClass('block');
    }

    function showQuestionPart1(id){
        $(`.part1-question`).removeClass('block');
        $(`.part1-question-${id}`).addClass('block');
    }

    function showQuestionPart2(id){
        $(`.part2-question`).removeClass('block');
        $(`.part2-question-${id}`).addClass('block');
    }

    function showQuestionPart3(id){
        $(`.part3-question`).removeClass('block');
        $(`.part3-question-${id}`).addClass('block');
    }
</script>
@endsection
