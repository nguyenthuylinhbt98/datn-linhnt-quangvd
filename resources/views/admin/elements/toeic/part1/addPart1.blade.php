@extends('admin.master')
@section('content')
<!--main-->
<div class="container main exam">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Cập nhật đề thi</h1>
        </div>
    </div> 
    @if(session('thongbao'))
    <div class="alert bg-success" role="alert">
        <svg class="glyph stroked checkmark">
            <use xlink:href="#stroked-checkmark"></use>
        </svg>
        {{session('thongbao')}} <a href="#" class="pull-right"><span class="glyphicon glyphicon-remove"></span></a>
    </div>
    @endif
    <!--/.row-->
    <form action="/admin/toeic/part_1" method="post" accept-charset="utf-8" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><i class="fas fa-user"></i>Thêm đề thi mới</div>
                    <div class="panel-body">
                        <div class="row justify-content-center" style="margin-bottom:40px">
                            <div class="col-md-8 col-lg-8 col-lg-offset-2">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input value="{{old('name')}}" type="text" name="name" placeholder="Name test" class="form-control" required>
                                    {{ ShowErrors($errors, 'name')}}
                                </div>
                                <div class="container">
                                    <h2>Exam Toeic</h2>
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#part1">Part I</a></li>
                                    </ul>
                                    <div class="tab-content">
                                        {{-- Part_1 --}}
                                        <div id="part1" class="tab-pane fade in active">
                                            <h3><b>PART I</b></h3>
                                            <p> <b>Directions:</b> For each question in this part, you will hear four statements about a picture in your test book. When you hear the statements, you must select the one statement that best describes what you see in the picture. Then find the number of the question on your answer sheet and mark your answer. The statements will not be printed in your test book and will be spoken only one time. Look at the example below.</p>

                                            <p><b>Example:</b></p>
                                            <input id="img_test1" type="file" name="example_img" class="form-control hidden" >
                                            <div class="example">
                                                <label for="img_test1"> <img style="width: 300px; height: 100%; " id="img_example" src="./images/touch.jpg"></label> 
                                            </div>

                                            <textarea id="summary-ckeditor" class="form-control" type="text" name="example_test1"  placeholder="Guide to choose the answer" style="height: 100px;"></textarea><br>
                                            <b>Voice: </b><br>
                                            <input type="file" name="void_part1"><br>

                                            <label for="number_ques">
                                                Number question:<input id="number_ques" type="text" name="number_ques" value="6">
                                            </label>
                                            <div class="question">
                                            </div>
                                           
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-lg-8 col-lg-offset-2 text-right"  style="margin-top: 20px;">
                                    <button class="btn btn-success"  type="submit">Add part 1</button>
                                    <button class="btn btn-danger" type="reset">Cancel</button>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!--/.row-->
</div>
<!--end main-->
@endsection

@section('script')
   
    <script>
        var number_ques = $('#number_ques').val();

        function ShowQues(num){
            for(let i= 1; i <= num; i++){
                $(".question").append(`
                    <p><b>Question ${i}:</b></p>
                    <div class="row">
                        <div class=" col-md-8 image">
                            <input id="img_question_${i}" type="file" name="img_question_${i}" class="form-control hidden" onchange="changeImg(this)">
                            <div class="example">
                                <label for="img_question_${i}"> <img id="img_${i}" src="./images/touch.jpg" width="30%"></label> 
                            </div>
                            
                        </div>
                        <div class="col-md-4 answer">
                            <div class="choices">
                                <ul class="list-question">
                                    <li>
                                        <span class="lq-number">A</span>
                                        <span class="my-radio">
                                            <input type="radio" id="" name="answer_question_${i}" value="A">
                                        </span>
                                    </li>
                                    <li>
                                        <span class="lq-number">B</span>
                                        <span class="my-radio">
                                            <input type="radio" id="" name="answer_question_${i}" value="B">
                                            
                                        </span>
                                    </li>
                                    <li>
                                        <span class="lq-number">C</span>
                                        <span class="my-radio">
                                            <input type="radio" id="" name="answer_question_${i}" value="C">
                                            
                                        </span>
                                    </li>
                                    <li>
                                        <span class="lq-number">D</span>
                                        <span class="my-radio">
                                            <input type="radio" id="" name="answer_question_${i}" value="D">
                                            
                                        </span>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>

                `)
            }
        }
        
        ShowQues(number_ques);

        $('#number_ques').on('change', function(){
            var num = $('#number_ques').val();
            $('.question').children().remove()
            ShowQues(num);
        })


        // ----------------------------------------
        function changeImg(input, idImg){
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                $(`#${idImg}`).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        // ------------------------------------------

        $("#img_test1").on('change', (function() {
            changeImg(this, "img_example");
            })
        );

        //   -------------------------------------
        for(let i=1; i<11; i++){
            $(`#img_question_${i}`).on('change', function(){
                let id = "img_" + i ;
                changeImg(this, id);
            })
        }
        
    </script>
    <script src="./../ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('summary-ckeditor');
    </script>
@endsection