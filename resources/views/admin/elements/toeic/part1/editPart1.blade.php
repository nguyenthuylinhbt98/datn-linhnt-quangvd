@extends('admin.master')
@section('content')
<!--main-->
<div class="container main exam">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Cập nhật đề thi</h1>
        </div>
    </div> 
    @if(session('thongbao'))
       <div class="alert alert-success">
            {{ session('thongbao') }}
        </div>

    @endif
    <!--/.row-->
    <form action="/admin/toeic/part_1/{{$part1->id}}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="_method" value="put">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><i class="fas fa-user"></i>Edit exam test part1: {{$part1->name}}</div>
                    <div class="panel-body">
                        <div class="row justify-content-center" style="margin-bottom:40px">
                            <div class="col-md-8 col-lg-8 col-lg-offset-2">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input value="{{$part1->name}}" type="text" name="name" placeholder="Name test" class="form-control" required>
                                    {{ ShowErrors($errors, 'name')}}
                                </div>
                                <div class="container">
                                    <h2>Exam Toeic</h2>
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#part1">Part I</a></li>
                                    </ul>
                                    <div class="tab-content">

                                        <div id="part1" class="tab-pane fade in active">
                                            <h3><b>PART I</b></h3>
                                            <p> <b>Directions:</b> For each question in this part, you will hear four statements about a picture in your test book. When you hear the statements, you must select the one statement that best describes what you see in the picture. Then find the number of the question on your answer sheet and mark your answer. The statements will not be printed in your test book and will be spoken only one time. Look at the example below.</p>

                                            <p><b>Example:</b></p>
                                            <input id="img_test1" type="file" name="example_img" class="form-control hidden" >
                                            <div class="example">
                                                <label for="img_test1"> <img style="width: 300px; height: 100%; " id="img_example" src="../{{$part1->example_img}}"></label> 
                                            </div>
                                            <textarea class="form-control" type="text" name="example_test1"  placeholder="Guide to choose the answer" style="height: 100px;">{{$part1->example_test1}}</textarea><br>
                                            <b>Voice: </b><br>
                                            <audio src="../{{$part1->void_part1}}"  controls></audio>
                                            <input type="file" name="void_part1"><br>
                                            <label for="number_ques">
                                                Number question:<input id="number_ques" type="text" name="number_ques" value="{{$part1->number_ques}}">
                                            </label>

                                            @for($i=1; $i<=$part1->number_ques; $i++)
                                                <p><b>Question {{$i}}:</b></p>
                                                <div class="row">
                                                    <div class=" col-md-8 image">
                                                        <input id="img_question_{{$i}}" type="file" name="img_question_{{$i}}" class="form-control hidden" onchange="changeImg(this)">
                                                        <div class="example">
                                                            <label for="img_question_{{$i}}"> <img style="width: 300px;height: 100%;" id="img_{{$i}}" src="../{{ $part1->{'img_question_'.$i} }}"></label> 
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="col-md-4 answer">
                                                        <div class="choices">
                                                            <ul class="list-question">
                                                                <li>
                                                                    <span class="lq-number">A</span>
                                                                    <span class="my-radio">
                                                                        <input @if( $part1->{'answer_question_'.$i}=='A' ) checked @endif type="radio" id="" name="answer_question_{{$i}}" value="A">
                                                                    </span>
                                                                </li>
                                                                <li>
                                                                    <span class="lq-number">B</span>
                                                                    <span class="my-radio">
                                                                        <input @if( $part1->{'answer_question_'.$i}=='B' ) checked @endif type="radio" id="" name="answer_question_{{$i}}" value="B">
                                                                        
                                                                    </span>
                                                                </li>
                                                                <li>
                                                                    <span class="lq-number">C</span>
                                                                    <span class="my-radio">
                                                                        <input @if( $part1->{'answer_question_'.$i}=='C' ) checked @endif type="radio" id="" name="answer_question_{{$i}}" value="C">
                                                                        
                                                                    </span>
                                                                </li>
                                                                <li>
                                                                    <span class="lq-number">D</span>
                                                                    <span class="my-radio">
                                                                        <input @if( $part1->{'answer_question_'.$i}=='D' ) checked @endif type="radio" id="" name="answer_question_{{$i}}" value="D">
                                                                        
                                                                    </span>
                                                                </li>
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </div>

                                            @endfor
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-lg-8 col-lg-offset-2 text-right"  style="margin-top: 20px;">
                                    <button class="btn btn-success"  type="submit">Cập nhật đề</button>
                                    <button class="btn btn-danger" type="reset">Huỷ bỏ</button>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!--/.row-->
</div>
<!--end main-->
@endsection

@section('script')
   
    <script>
        // ----------------------------------------
        function changeImg(input, idImg){
            if (input.files && input.files[0]) {
                var reader = new FileReader();
                reader.onload = function(e) {
                $(`#${idImg}`).attr('src', e.target.result);
                }
                reader.readAsDataURL(input.files[0]);
            }
        }

        // ------------------------------------------

        $("#img_test1").on('change', (function() {
            changeImg(this, "img_example");
            })
        );

        //   -------------------------------------
        for(let i=1; i<11; i++){
            $(`#img_question_${i}`).on('change', function(){
                let id = "img_" + i ;
                changeImg(this, id);
            })
        }
        
        
    </script>
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
    <script>
        $('textarea').ckeditor();
    </script>
@endsection