@extends('admin.master')
@section('content')
<!--main-->
<div class="container list-test">
    
    <!--/.row-->
    <div class="row">
        <div class="col-xs-12 col-md-12 col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <div class="bootstrap-table">
                        <div class="table-responsive">
                            @if(session('thongbao'))
                                <div class="alert alert-success">
                                    {{ session('thongbao') }}
                                </div>
                            @endif
                            <a href="/admin/toeic/test/create" class="btn btn-primary">Add new test toeic</a>
                            <div class="col-md-12 col-sm-12 col-xs-12">
                                <div class="x_panel">
                                    <div class="x_title">
                                        <h2>List test Toeic </h2>
                                        <ul class="nav navbar-right panel_toolbox">
                                            <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                                            </li>
                                        </ul>
                                        <div class="clearfix"></div>
                                    </div>
                                    <div class="x_content">
                                        
                                        <div class="table-responsive">
                                            <table class="table table-striped jambo_table bulk_action">
                                                <thead>
                                                    <tr class="headings">
                                                        <th class="column-title">Choice</th>
                                                        <th class="column-title">Name </th>
                                                        <th class="column-title">Time update </th>
                                                        <th class="column-title">Number view </th>
                                                        <th class="column-title">Number test </th>
                                                        <th class="column-title">Role </th>
                                                        <th class="column-title">Level</th>
                                                        <th class="column-title">#</th>
                                                    </tr>
                                                </thead>
                                                <tbody>

                                                    @foreach($test as $item)
                                                        <tr class="even pointer">
                                                            <td class="a-center "><input type="checkbox" value="{{ $item->id }}"></td>
                                                            <td class=" ">{{ $item->name_test }}</td>
                                                            <td class=" ">{{$item->updated_at}} </td>
                                                            <td class=" "> {{ $item->view }} <i class="success fa fa-long-arrow-up"></i></td>
                                                            <td class=" ">{{ $item->test }}</td>
                                                            <td class=" ">{{ $item->role }}</td>
                                                            <td class="a-right a-right ">{{ $item->level }}</td>
                                                            <td class=" last" style="text-align: center">
                                                                <a href="/admin/toeic/test/{{$item->id}}" class="btn btn-primary"><i class="fa fa-book" aria-hidden="true"></i> Xem trước</a>
                                                                <a href="/admin/toeic/test/{{$item->id}}/edit" class="btn btn-warning"><i class="fa fa-pencil" aria-hidden="true"></i> Sửa</a>
                                                                <a href="#" class="btn btn-danger" data-toggle="modal" data-target="#exampleModal{{$item->id}}"><i class="fa fa-trash" aria-hidden="true"></i> Xóa</a>
                                                                <div class="modal fade" id="exampleModal{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                                                                    <div class="modal-dialog" role="document">
                                                                        <div class="modal-content">
                                                                            <div class="modal-header">
                                                                                <h5 class="modal-title" id="exampleModalLabel">Xóa thành viên</h5>
                                                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                                                <span aria-hidden="true">&times;</span>
                                                                                </button>
                                                                            </div>
                                                                            <form action="/admin/toeic/test/{{$item->id}}" method="POST" accept-charset="utf-8">
                                                                                <div class="modal-body">
                                                                                    @csrf
                                                                                    <input type="hidden" name="_method" value="delete">
                                                                                    <h4>Bạn có chắc chắn muốn xóa {{$item->name_test}}!!!</h4>
                                                                                </div>
                                                                                <div class="modal-footer">
                                                                                    <button type="button" class="btn btn-default" data-dismiss="modal">Quay lại</button>
                                                                                    <button type="submit" class="btn btn-danger">Xóa</button>
                                                                                </div>
                                                                            </form>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        
                                                    @endforeach

                                                </tbody>
                                            </table>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div align='right'>
                                <ul class="pagination">
                                    {{$test->links()}}
                                </ul>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
            <!--/.row-->
        </div>
    </div>
</div>
<!--end main-->
@endsection