<?php

namespace App\Models\Toeic;

use Illuminate\Database\Eloquent\Model;

class Part3 extends Model
{
    protected $table = 'toeic_part3';

    protected $guarded = [
        'id'
    ];
}
