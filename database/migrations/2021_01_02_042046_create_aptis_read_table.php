<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAptisReadTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aptis_read', function (Blueprint $table) {
            $table->id();
            $table->string('name', 50)->nullable();
            $table->longText('p1_para')->nullable();
            $table->string('example', 50)->nullable();
            $table->text('p1_ques1', 191)->nullable();
            $table->text('p1_ques2', 191)->nullable();
            $table->text('p1_ques3', 191)->nullable();
            $table->text('p1_ques4', 191)->nullable();
            $table->text('p1_ques5', 191)->nullable();
            $table->text('p1_ques6', 191)->nullable();
            $table->text('p1_ques7', 191)->nullable();
            $table->longText('p2_ques1')->nullable();
            $table->longText('p2_ques2')->nullable();
            $table->longText('p3_para')->nullable();
            $table->longText('p3_title')->nullable();
            $table->longText('p3_ques')->nullable();
            $table->string('p3_ans', 191)->nullable();
            $table->longText('p4_para')->nullable();
            $table->longText('p4_title')->nullable();
            $table->longText('p4_ans', 191)->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aptis_read');
    }
}
