<?php

namespace App\Models\Toeic;

use Illuminate\Database\Eloquent\Model;

class Part4 extends Model
{
    protected $table = 'toeic_part4';

    protected $guarded = [
        'id'
    ];
}
