<?php

namespace App\Http\Requests\Toeic\Test;

use Illuminate\Foundation\Http\FormRequest;

class AddTestRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'name'=>'required|unique:toeic_test,name',
        ];
    }
    public function messages()
    {
        return [
            'name.required'=>'Tên đề thi không được để trống!',
            'name.unique'=>'Tên đề thi đã được sử dụng trước đó!',
        ];
    }
}
