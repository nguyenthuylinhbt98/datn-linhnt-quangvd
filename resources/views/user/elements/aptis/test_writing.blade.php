@extends('user.master')

@section('css')
	<link rel="stylesheet" href="assets/css/test.css">
    <style>
        label{
            float: left;
        }
        select{
            max-width: 90%;
        }
    </style>
@endsection


@section('content')
    
<main class="aptis" id="aptis-writing">
    <!--? slider Area Start-->
    <section class="slider-area slider-area2">
        <div class="slider-active">
            <!-- Single Slider -->
            <div class="single-slider slider-height2">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-8 col-lg-11 col-md-12">
                            <div class="hero__caption hero__caption2">
                                <h1 data-animation="bounceIn" data-delay="0.2s">{{$name}}</h1>
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                                        <li class="breadcrumb-item"><a href="/aptis">Aptis</a></li>
                                        <li class="breadcrumb-item"><a href="/aptis/test/{{ $id }}">{{$name}}</a></li>
                                        <li class="breadcrumb-item"><a>Writing</a></li>
                                    </ol>
                                </nav>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class=" test">
       <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-9 col-lg-10">
                <div class="section-tittle  mb-10 mt-20">
                    <p>
                        Welcome to the Aptis Writing Test. <br>
                        The test has four parts and takes up to <b>50 minutes.</b>
                    </p>
                </div>
            </div>
            <div class="col-xl-3 col-lg-2">
                <div class="time-ans">
                    <i class="far fa-clock"></i>
                    0<span id="h">0</span> :
                    <span id="m">00</span> :
                    <span id="s">00</span>
                </div>
            </div>
            <div class="col-xl-12 col-lg-12">
                <form action="" method="POST">
                    @csrf
                    <input type="hidden" value="{{$write->id}}" name="listen_id">
                    <div class="row">
                        <div class="col-md-12 col-lg-12">
                            <ul class="nav nav-pills nav-justified" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                  <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#part1" role="tab" aria-controls="pills-home" aria-selected="true">Part I</a>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#part2" role="tab" aria-controls="pills-profile" aria-selected="false">Part II</a>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#part3" role="tab" aria-controls="pills-contact" aria-selected="false">Part III</a>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#part4" role="tab" aria-controls="pills-contact" aria-selected="false">Part IV</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="part1" role="tabpanel" aria-labelledby="pills-home-tab">
                                    @for ($i = 1; $i < 6; $i++)
                                        <b>{{$i}}.</b>{{ $write['p1_ques'.$i][0] }}
                                        <input class="form-control" type="text" name="answer_writing[]">
                                    @endfor
                                </div>
                                <div class="tab-pane fade show " id="part2" role="tabpanel" aria-labelledby="pills-home-tab">
                                    <b>{!! $write['p2'][0] !!}</b>
                                    <textarea class="form-control" name="answer_writing[]" id="" cols="30" rows="10"></textarea>
                                </div>
                                <div class="tab-pane fade show " id="part3" role="tabpanel" aria-labelledby="pills-home-tab">
                                    {!! $write['p3_subject'] !!} <br>
                                    @for ($i = 1; $i < 4; $i++)
                                        <b>{{$i}}.{!! $write['p3_ques'.$i][0] !!}</b>
                                        <textarea class="form-control" name="answer_writing[]" id="" cols="30" rows="5"></textarea>
                                    @endfor
                                </div>
                                <div class="tab-pane fade show " id="part4" role="tabpanel" aria-labelledby="pills-home-tab">
                                    {!! $write['p4_subject'] !!} 
                                    <span>{!! $write['p4_ques1'] !!}</span>
                                    <textarea class="form-control" name="answer_writing[]" id="" cols="30" rows="5"></textarea>
                                    <span>{!! $write['p4_ques2'] !!}</span>
                                    <textarea class="form-control" name="answer_writing[]" id="" cols="30" rows="10"></textarea>
                                </div>
                            </div>
                            <div class="sub d-flex justify-content-center mb-3">
                                <input type="submit" class="genric-btn primary-border circle" value="Submit"/>
                            </div>
                        </div>
                        
                    </div>
                </form>
              
            </div>
        </div>
       </div>
    </section>
</main>


@endsection


@section('script')
<script>
    var h = 00; // Giờ
    var m = 50; // Phút
    var s = 01; // Giây
    var timeout = null;

    function start()
    {
        if (s === -1){
            m -= 1;
            s = 59;
        }

        if (m === -1){
            h -= 1;
            m = 59;
        }

        if (h == -1){
            clearTimeout(timeout);
            document.getElementById("testToeic").submit();
            return false;
        }

        document.getElementById('h').innerText = h.toString();
        document.getElementById('m').innerText = m.toString();
        document.getElementById('s').innerText = s.toString();

        timeout = setTimeout(function(){
            s--;
            start();
        }, 1000);
    }
    start();

    function activeAns(id){
        $(`.ques-${id}`).addClass('active')
    }
    
    function showQuestion(id){
        $(`.question-listening`).removeClass('block');
        $(`.question-${id}`).addClass('block');

    }
</script>
@endsection
