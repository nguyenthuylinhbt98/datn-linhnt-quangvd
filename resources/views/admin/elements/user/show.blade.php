@extends('admin.master')
@section('admin', 'class=active')


@section('content')
<!--main-->
	<div class="container main">
		<div class="">
      
      
      <div class="clearfix"></div>

      <div class="row">
        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            
            <div class="x_content">

              <div class="col-md-5 col-sm-6 col-xs-12">
                <div class="product-image">
                  
                  @if(isset($user->avatar))
                    <img src="../{{ $user->avatar }}" alt="" >
                  @else
                    <img src="../images/avatar/no-avatar.jpg" alt="" >
                  @endif
                </div>
              
              </div>

              <div class="col-md-7 col-sm-6 col-xs-12" style="border:0px solid #e5e5e5;">

                <h3 class="price">{{ $user->name }} ({{ $user->k }})</h3><hr>

                <p>
                    @if($user->role == 0) <h4 class="brief"><i>Thành viên Lab</i></h4> @endif
                    @if($user->role == 1) <h4 class="brief"><i>Phó trưởng Lab</i></h4> @endif
                    @if($user->role == 2) <h4 class="brief"><i>Trưởng Lab</i></h4> @endif
                    @if($user->role == 3) <h4 class="brief"><i>Giảng viên phụ trách Lab</i></h4>@endif
                </p>

                <div class="">
                  <div class="product_price"><br>
                    <p><b>Email:</b>   {{ $user->email }}</p>
                    <p><b>Phone:</b>   {{ $user->phone }}</p>
                    <p><b>Viện:</b>   {{ $user->education }}</p>
                    <p><b>Địa chỉ:</b>   {{ $user->address }}</p>
                    <p><b>Facebook:</b>   {{ $user->facebook }}</p>
                    <br>
                  </div>
                </div>

                <div class="product_social">
                  <ul class="list-inline">
                    <li><a href="{{ $user->facebook }}"><i class="fa fa-facebook-square"></i></a>
                    </li>
                    <li><a href="#"><i class="fa fa-twitter-square"></i></a>
                    </li>
                    <li><a href="{{ $user->email }}"><i class="fa fa-envelope-square"></i></a>
                    </li>
                    <li><a href="#"><i class="fa fa-rss-square"></i></a>
                    </li>
                  </ul>
                </div>

              </div>


              <div class="col-md-12">

                <div class="" role="tabpanel" data-example-id="togglable-tabs">
                  <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                    <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">Học thuật</a>
                    </li>
                    <li role="presentation" class=""><a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">Mô tả</a>
                    </li>
                    
                  </ul>

                  <div id="myTabContent" class="tab-content">
                    <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">
                      <p>{{ $user->course }}</p>
                    </div>
                    <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">
                      <p>{{ $user->description }}</p>
                    </div>
                    
                  </div>
                </div>

              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
		

	</div>
	<!--end main-->

@endsection

