<?php

namespace App\Http\Controllers\Admin\Toeic;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Toeic\Part1;
use App\Http\Requests\Toeic\Part1\{AddPart1Request, EditPart1Request};
use App\Repositories\Admin\Toeic\Part1Repository;

class Part1Controller extends Controller
{
    private $part1Repository;

    public function __construct()
    {
        $this->part1Repository = new Part1Repository();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['part1'] = Part1::paginate(10);
        return view('admin.elements.toeic.part1.listPart1', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.elements.toeic.part1.addPart1');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddPart1Request $r)
    {
        $data = $r->all();
        $part_1 = $this->part1Repository->createPart1($data); 
        if($part_1){   
             return redirect('/admin/toeic/part_1')->with('thongbao', 'Success create new part1 test'); 
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['part1'] = Part1::find($id);
        return view('admin.elements.toeic.part1.editPart1', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditPart1Request $r, $id)
    {
        $data = $r->all();
        $part_1 = $this->part1Repository->updatePart1($data, $id); 
        if($part_1){   
             return redirect('/admin/toeic/part_1')->with('thongbao', 'Success edit part II test'); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Part1::destroy($id);
        return redirect()->back()->with('thongbao', 'Đã xóa thành công!!!');
    }
}
