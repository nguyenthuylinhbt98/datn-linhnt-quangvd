<footer>
	<div class="footer-wrappper footer-bg">
	  
	   <div class="footer-bottom-area">
		   <div class="container">
			   <div class="footer-border">
				   <div class="row d-flex align-items-center">
					   <div class="col-xl-12 ">
						   <div class="footer-copy-right text-center">
							   <p>
								  --- Nguyễn Thùy Linh _ 20167261 --- Vũ Duy Quang _20163331 ---
								</p>
							 </div>
						 </div>
					 </div>
				 </div>
			 </div>
		 </div>
		 <!-- Footer End-->
	 </div>
 </footer> 
<!-- Scroll Up -->
<div id="back-top" style="margin-bootom: 60px" >
	<a title="Go to Top" href="#"> <i class="fas fa-level-up-alt"></i></a>
 </div>
@if(Auth::user() && Auth::user()->level != 2)
	<div id="assign-teacher" class="rounded-circle d-flex align-content-center" style="height: 60px; width: 60px; background: #8080ff;">
		<a title="Assign to be Teacher"  href="/for-teacher" style="color: #ffffff; margin:auto">Setting</a>
	</div>
@endif
<div id="take-note" class="wow bounceIn rounded-circle d-flex align-content-center" data-wow-iteration="3" data-delay="2s"  style="height: 60px; width: 60px; background: #8080ff;">
	<a title="Take note now" type="button" data-toggle="modal" data-target="#exampleModalNote" style="color: #ffffff; margin:auto"> Note</a>
</div>
<div class="modal fade" id="exampleModalNote" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="margin-top: 30px; font-size: 50px">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel" style="font-size: 30px">Record your knowledge</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
		<form action="/note" method="post">
			@csrf
			<div class="modal-body">
				<textarea name="note" class="form-control" style="height: 400px; font-size: 20px">
				</textarea>
			</div>
			<div class="modal-footer">
				<button type="submit" class="btn-note btn btn-info btn-sm">Save</button>
			</div>
		</form>
  </div>
</div>

 
  <!--Start of Tawk.to Script-->
  <script type="text/javascript">
	var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
	(function(){
	var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
	s1.async=true;
	s1.src='https://embed.tawk.to/5d7a38e8c22bdd393bb580e2/default';
	s1.charset='UTF-8';
	s1.setAttribute('crossorigin','*');
	s0.parentNode.insertBefore(s1,s0);
	})();
</script>
<!--End of Tawk.to Script-->




