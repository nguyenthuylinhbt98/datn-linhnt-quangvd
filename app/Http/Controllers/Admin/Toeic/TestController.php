<?php

namespace App\Http\Controllers\Admin\Toeic;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Toeic\{Part1, Part2, Part3, Part4, Part5, Part6, Part7, Test};
use App\Http\Requests\Toeic\Test\{AddTestRequest, EditTestRequest};
use App\Repositories\Admin\Toeic\TestRepository;

class TestController extends Controller
{
    private $testRepository;

    public function __construct()
    {
        $this->testRepository = new TestRepository();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['test'] = Test::paginate(10);
        return view('admin.elements.toeic.test.listTest', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $data['part_1'] = Part1::all();
        $data['part_2'] = Part2::all();
        $data['part_3'] = Part3::all();
        $data['part_4'] = Part4::all();
        $data['part_5'] = Part5::all();
        $data['part_6'] = Part6::all();
        $data['part_7'] = Part7::all();
        return view('admin.elements.toeic.test.addTest', $data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        $test = $this->testRepository->createTest($r->all()); 
        if($test){   
             return redirect('/admin/toeic/test')->with('thongbao', 'Success create new test'); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data = $this->testRepository->showTest($id); 
        return view("admin.elements.toeic.test.showTest", $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['part_1'] = Part1::all();
        $data['part_2'] = Part2::all();
        $data['part_3'] = Part3::all();
        $data['part_4'] = Part4::all();
        $data['part_5'] = Part5::all();
        $data['part_6'] = Part6::all();
        $data['part_7'] = Part7::all();
        $data['test'] = $this->testRepository->editTest($id); 
        return view('admin.elements.toeic.test.editTest',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
        $test = $this->testRepository->updateTest($r->all(), $id); 
        if($test){   
             return redirect('/admin/toeic/test')->with('thongbao', 'Success edit'); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Test::destroy($id);
        return redirect()->back()->with('thongbao', 'Đã xóa thành công!!!');
    }
}
