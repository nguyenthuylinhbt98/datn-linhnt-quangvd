<?php

namespace App\Http\Controllers\User\Toeic;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Toeic\{Part1, Part2, Part3, Part4, Part5, Part6, Part7, Test, Transcript};
use App\Models\Users\User_And_Test;
use App\Models\Teacher;
use App\Repositories\Admin\Toeic\{TestRepository, TranscriptRepository};
use Auth;


class ToeicController extends Controller
{
    private $testRepository;
    private $tranRepository;

    public function __construct()
    {
        $this->testRepository = new TestRepository();
        $this->tranRepository = new TranscriptRepository();
    }

    public function getList(){
        $data['test'] = Test::orderBy('id', 'ASC')->paginate(6);
        $data['class'] = Teacher::where('course', 'Toeic')->get();
        return view('user.elements.toeic.list', $data);
    }

    public function getTest($id){
        $test = Test::find($id);
        $test->view = $test['view']+1;
        $test->save();
        $data = $this->testRepository->showTest($id); 
        return view('user.elements.toeic.test', $data);
    }

    public function postTest(Request $r, $id){
        $test = Test::find($id);
        $test->test = $test['test']+1;
        $test->save();

        // correct answer
        $correct = $this->testRepository->getCorrectAns($id);

        // user answer
        $ans = $this->testRepository->getUserAns($r->all());

        // check point
        $point = $this->testRepository->checkPoint($correct, $ans);
        $a = Transcript::all()->first();
        $tran = $this->tranRepository->getShowTran($a);
       
        $data['ans_ls'] = $point['ls'];
        $data['ans_re'] = $point['re'];
        $data['point_ls'] = $tran['diem_nghe'][$point['ls']];
        $data['point_re'] = $tran['diem_doc'][$point['re']];

        $a = $data['ans_ls'].'; ;'.$data['ans_re'].'; ;'.$data['point_ls'].'; ;'.$data['point_re'];
        User_And_Test::create([
            'user_id'=>Auth::user()->id,
            'test_id'=>$id,
            'score'=>$a, 
        ]);

        return view('user.elements.toeic.result', $data);
    }
}
