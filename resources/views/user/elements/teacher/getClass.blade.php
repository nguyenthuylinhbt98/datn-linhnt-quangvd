@extends('user.master')

@section('css')
     <style>
        .blog_left_sidebar img{
             height: 310px;
             object-fit: cover;
             
         }
        .form-group label {
            font-size: 20px;
            font-weight: 700;
            width: 200px;
             padding-top: 8px;
        }
        .form {
            background: url('../../default/create.png')
        }
     </style>
    
@endsection

@section('content')
<main>
    <!--? slider Area Start-->
    <section class="slider-area slider-area2">
        <div class="slider-active">
            <!-- Single Slider -->
            <div class="single-slider slider-height2">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-8 col-lg-11 col-md-12">
                            <div class="hero__caption hero__caption2">
                                <h1 data-animation="bounceIn" data-delay="0.2s">Category for teachers</h1>
                                <!-- breadcrumb Start-->
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                                        <li class="breadcrumb-item"><a>For Teacher</a></li> 
                                    </ol>
                                </nav>
                                <!-- breadcrumb End -->
                            </div>
                        </div>
                    </div>
                </div>          
            </div>
        </div>
    </section>
    <div class="courses-area section-padding40 fix">
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-xl-4 col-lg-4">
                    <div class="section-tittle text-center mb-55">
                        <h2>List Members <span>({{ count($members) }})</span></h2>
                    </div>
                    <div class="blog_right_sidebar" style="height: 310px; overflow: auto;">
                        @foreach($members as $key=>$member)
                            <p>
                                <div class="row">
                                    <div class="col-xl-8 col-lg-8">{{ $key+1 }}. {{ $member->user }}</div>
                                    <div class="col-xl-4 col-lg-4">
                                        <a style=" color: red; padding: 5px;" href="/for-teacher/class/{{ $class_id }}/ban/{{ $member->user_id }}">Delete</a>
                                    </div>
                                </div>
                            </p>
                        @endforeach
                    </div> <br><br>
                    {{-- chờ phê duyệt --}}
                    <div class="section-tittle text-center mb-55">
                        <h2>Waiting for approval <span>({{ count($waitting) }})</span></h2>
                    </div>
                    <div class="blog_right_sidebar" style="height: 410px; overflow: auto;">
                        @foreach($waitting as $key=>$item)
                            <p>
                                <div class="row">
                                    <div class="col-xl-8 col-lg-8">{{ $key+1 }}. {{ $item->user }}</div>
                                    <div class="col-xl-4 col-lg-4">
                                        <a style=" color: green; padding: 5px;" href="/for-teacher/class/{{ $class_id }}/approval/{{ $item->user_id }}">Approval</a>
                                    </div>
                                </div>
                            </p>
                        @endforeach
                    </div>
                </div>
                <div class="col-xl-8 col-lg-8">
                    <div class="section-tittle text-center mb-55">
                        <h2>Group content</h2>
                        @if(session('thongbao'))
                            <div class="alert bg-success" role="alert" style="font-size: 20px; color: #ffffff">
                                {!!  session('thongbao') !!} 
                            </div>
                        @endif
                    </div>
                    <div class="section-tittle text-center mb-55">
                    </div>
                </div>
            </div>
            
        </div>
    </div>
</main>
@endsection

@section('script')
    <script>
        $('.button-contactForm').on('click', function(){
            $('form').submit();
        })
    </script>
@endsection