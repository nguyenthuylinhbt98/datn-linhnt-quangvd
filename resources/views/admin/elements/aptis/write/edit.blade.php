@extends('admin.master')
@section('content')
<!--main-->
<div class="container main exam">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Add new writing test</h1>
        </div>
    </div> 
   @if(session('thongbao'))
       <div class="alert alert-success">
            {{ session('thongbao') }}
        </div>

    @endif
    <!--/.row-->
    <form action="/admin/aptis/writing/{{$write->id}}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="_method" value="put">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><i class="fas fa-user"></i>Add writing test</div>
                    <div class="panel-body">
                        <div class="row justify-content-center" style="margin-bottom:40px">
                            <div class="col-md-8 col-lg-8 col-lg-offset-2">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input value="{{$write->name}}" type="text" name="name" placeholder="Name test" class="form-control" required>
                                    {{ ShowErrors($errors, 'name')}}
                                </div>
                                <div class="container">
                                    <h2>Exam </h2>
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#part1">Part I</a></li>
                                        <li><a data-toggle="tab" href="#part2">Part II</a></li>
                                        <li><a data-toggle="tab" href="#part3">Part III</a></li>
                                        <li><a data-toggle="tab" href="#part4">Part IV</a></li>
                                        
                                    </ul>
                                    <div class="tab-content">
                                        <div id="part1" class="tab-pane fade in active">
                                            @for($i=1; $i<6; $i++)
                                                <div>
                                                    <label for="ques1">{{$i}}.</label>
                                                    <input class="form-control" type="text" name="p1_ques{{$i}}[]" placeholder="Ques{{$i}}" value="{{$write->{'p1_ques'.$i}[0]}}"><br>
                                                    <input class="form-control" type="text" name="p1_ques{{$i}}[]" placeholder="Suggestions" value="{{$write->{'p1_ques'.$i}[1]}}">
                                                </div>
                                            @endfor
                                        </div>                                       
                                        <div id="part2" class="tab-pane fade ">
                                            <div>
                                                <label for="ques2">Part 2</label><br>
                                                Subject: <textarea class="form-control ckeditor" name="p2[]" id="">{{$write->p2[0]}}</textarea><br>
                                                Suggestions: <textarea class="form-control ckeditor" name="p2[]" id="">{{$write->p2[1]}}</textarea>
                                            </div>
                                        </div>                                       
                                        <div id="part3" class="tab-pane fade ">
                                            <div>
                                                <label for="ques3">Part 3</label><br>
                                                Subject: <textarea class="form-control ckeditor" name="p3_subject" id="">{{$write->p3_subject}}</textarea><br>
                                            </div>
                                            @for($i=1; $i<4; $i++)
                                                <div>
                                                    <label for="ques3">{{$i}}.</label><br>
                                                    Ques: <textarea class="form-control ckeditor" name="p3_ques{{$i}}[]" id="">{{$write->{'p3_ques'.$i}[0]}}</textarea><br>
                                                    Suggestions: <textarea class="form-control ckeditor" name="p3_ques{{$i}}[]" id="">{{$write->{'p3_ques'.$i}[1]}}</textarea>
                                                </div>
                                            @endfor
                                        </div>                                       
                                        <div id="part4" class="tab-pane fade ">
                                            <div>
                                                <label for="ques4">Part 4</label><br>
                                                Subject: <textarea class="form-control ckeditor" name="p4_subject" id="">{{$write->p4_subject}}</textarea><br>
                                            </div>
                                            <div>
                                                <label for="ques4">Informal</label><br>
                                                <textarea class="form-control ckeditor" name="p4_ques1" id="">{{$write->p4_ques1}}</textarea><br>
                                            </div>
                                            <div>
                                                <label for="ques4">Formal</label><br>
                                                <textarea class="form-control ckeditor" name="p4_ques2" id="">{{$write->p4_ques2}}</textarea><br>
                                            </div>
                                            
                                        </div>                                       
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-lg-8 col-lg-offset-2 text-right"  style="margin-top: 20px;">
                                    <button class="btn btn-success"  type="submit">Cập nhật đề</button>
                                    <button class="btn btn-danger" type="reset">Huỷ bỏ</button>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!--/.row-->
</div>
<!--end main-->
@endsection

@section('script')
    <script src="./../ckeditor/ckeditor.js"></script>
@endsection