<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateToeicPart3Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('toeic_part3', function (Blueprint $table) {
            $table->id();
            $table->string('name', 191);
            $table->string('void_part3', 191)->nullable();
            $table->integer('begin');
            $table->integer('end');
            for($i=0; $i < 40; $i++){
                $table->longText('question_'.$i)->nullable();
            };
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('toeic_part3');
    }
}
