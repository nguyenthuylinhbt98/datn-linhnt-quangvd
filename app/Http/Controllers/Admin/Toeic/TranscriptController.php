<?php

namespace App\Http\Controllers\Admin\Toeic;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Toeic\Transcript;
use App\Http\Requests\Toeic\TranscriptRequest;
use App\Repositories\Admin\Toeic\TranscriptRepository;

class TranscriptController extends Controller
{
    private $transcriptRepository;

    public function __construct()
    {
        $this->transcriptRepository = new TranscriptRepository();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $transcript= Transcript::all()->toarray();
        if ($transcript==null) {
            return view('admin.elements.toeic.transcript.addTranscript');
        }else{
            $id=$transcript[0]['id'];
            $transcript= Transcript::find($id);
            (array)$data['diem_nghe']=explode('; ;',$transcript->{'diem_nghe'});
            (array)$data['diem_doc']=explode('; ;',$transcript->{'diem_doc'});

            return view('admin.elements.toeic.transcript.editTranscript',compact('transcript'), $data);

        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return redirect('/admin/toeic/transcript');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        $transcript = $this->transcriptRepository->createTranscript($r->all()); 
        if ($transcript) {
            return redirect('/admin/toeic/transcript')->with('thongbao', 'Thành công!!!');
        } 
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        return redirect('/admin/toeic/transcript');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        return redirect('/admin/toeic/transcript');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
        $transcript = $this->transcriptRepository->updateTranscript($r->all(), $id);         
        
        if ($transcript) {
            return redirect('/admin/toeic/transcript')->with('thongbao', 'Chỉnh sửa bẳng điểm thành công!!!');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
