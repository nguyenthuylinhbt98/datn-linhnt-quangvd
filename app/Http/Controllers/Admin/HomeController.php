<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Users\User;
use App\Models\Users\User_And_Test as User_Toeic;
use App\Models\Users\User_And_Aptis as User_Aptis;
use App\Models\Teacher;
use App\Models\Toeic\Test as ToeicTest;
use App\Models\Aptis\Test as AptisTest;
use Carbon\Carbon; 

class HomeController extends Controller
{
    public function getIndex(){
        $data['users'] = User::where('level', 0)->get();
        $data['toeic'] = ToeicTest::all()->count();
        $data['aptis'] = AptisTest::all()->count();
        $data['class'] = Teacher::all()->count();
        $data['user_toeic'] = User_Toeic::orderBy('id', 'desc')->paginate(10);
        $data['user_aptis'] = User_Aptis::orderBy('id', 'desc')->paginate(10);
        $data['listen_point'] = $this->getlistPointListen();
        $data['read_point'] = $this->getlistPointRead();
        
        foreach ($data['user_toeic'] as $key=>$item) {
            $score = explode('; ;',$item->score);
            $item['user'] = User::findOrFail($item->user_id)->full;
            $item['listen'] = $score[1];
            array_push($data['listen_point'], $score[1]);
            $item['read'] = $score[3];
            array_push($data['read_point'], $score[3]);

            $item['total'] = (int)$score[1] + (int)$score[3];
        }  
        $data['chart_toeic'] = $this->getDataChartToeic();

        $month_now = Carbon::now()->format('m');
    	$year_now = Carbon::now()->format('Y');
    	for($i=1; $i<=$month_now; $i++){
    		$data['chart_user'][$i]= User::whereMonth('created_at', $i)
                            ->whereYear('created_at', $year_now)
                            ->count();
        	}
    	return view('admin.elements.index', $data);
    }

    public function getDataChartToeic() {
        $user_toeic = User_Toeic::all();
        $data['Less than 350'] = 0;
        $data['Between 350-450'] = 0;
        $data['Between 450-550'] = 0;
        $data['Between 550-650'] = 0;
        $data['More than 750'] = 0;
        foreach ($user_toeic as $item) {
            $score = explode('; ;',$item->score);
            $total = (int)$score[1] + (int)$score[3];
            if($total < 350) {
                $data['Less than 350'] += 1;
            } else {
                if($total < 450) {
                    $data['Between 350-450'] += 1;
                } else {
                    if($total < 550) {
                        $data['Between 450-550'] += 1;
                    } else {
                        if($total < 650) {
                            $data['Between 550-650'] += 1;
                        } else {
                            $data['More than 750'] += 1;
                        }
                    }
                }
            }
        }  
        return $data;
    }

    public function getlistPointListen() {
        $listen_point = [];
        $user_toeic = User_Toeic::all();
        foreach ($user_toeic as $item) { 
            $score = explode('; ;',$item->score);
            array_push($listen_point, $score[1]);
        }
         return $listen_point;
    }

    public function getlistPointRead() {
        $read_point = [];
        $user_toeic = User_Toeic::all();
        foreach ($user_toeic as $item) { 
            $score = explode('; ;',$item->score);
            array_push($read_point, $score[3]);
        }
         return $read_point;
    }
}
