<?php

use Illuminate\Support\Facades\Route;


Route::get('/clear-cache', function() {
    $exitCode = Artisan::call('config:clear');
    $exitCode = Artisan::call('cache:clear');
    $exitCode = Artisan::call('config:cache');
    return 'DONE'; 
});
Route::get('ckeditor', 'CkeditorController@index');
Route::post('ckeditor/upload', 'CkeditorController@upload')->name('ckeditor.upload');

// -----------Login Admin---------------
Route::get('/login', 'Admin\LoginAdminController@getLogin');
Route::post('/login', 'Admin\LoginAdminController@postLogin');
Route::get('/logout', 'Admin\LoginAdminController@getLogout');




