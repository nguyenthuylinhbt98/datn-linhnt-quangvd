<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUserAndAptisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('user_and_aptis', function (Blueprint $table) {
            $table->id();
            $table->integer('user_id')->nullable();
            $table->integer('aptis_id')->nullable();
            $table->longText('listening_answer')->nullable();
            $table->longText('reading_answer')->nullable();
            $table->longText('writing_answer')->nullable();
            $table->longText('grammar_answer')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('user_and_aptis');
    }
}
