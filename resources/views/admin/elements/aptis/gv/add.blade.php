@extends('admin.master')
@section('css')
    <style>
        #part2 input.form-control{
            margin-bottom: 10px;
            width: 90%;
        }
    </style>   
@endsection
@section('content')
<!--main-->
<div class="container main exam">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Add new Grammar & Vocabulary test</h1>
        </div>
    </div> 
   @if(session('thongbao'))
       <div class="alert alert-success">
            {{ session('thongbao') }}
        </div>

    @endif
    <!--/.row-->
    <form action="/admin/aptis/gv" method="post" accept-charset="utf-8" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><i class="fas fa-user"></i>Add Grammar & Vocabulary test</div>
                    <div class="panel-body">
                        <div class="row justify-content-center" style="margin-bottom:40px">
                            <div class="col-md-8 col-lg-8 col-lg-offset-2">
                                <div class="form-group">
                                    <label>Name Grammar & Vocabulary test</label>
                                    <input value="{{old('name')}}" type="text" name="name" placeholder="Name Grammar & Vocabulary test" class="form-control" required>
                                    {{ ShowErrors($errors, 'name')}}
                                </div>
                                <div class="container">
                                    <h2>Exam </h2>
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#part1">Grammar</a></li>
                                        <li><a data-toggle="tab" href="#part2">Vocabulary </a></li>
                                    </ul>
                                    <div class="tab-content">
                                        <div id="part1" class="tab-pane fade in active">
                                            <div class="question">
                                                
                                            </div>
                                        </div>                                       
                                        <div id="part2" class="tab-pane fade ">
                                            <div class="smillar-meaning row">
                                                <h3>Smiller meaning</h3>
                                                <div class="col-md-4">
                                                    <div class="div_p26_list_answer">
                                                        <b>List answer</b>
                                                        @for($i=0; $i<5; $i++)
                                                            <input class="p26_list_answer form-control" type="text" name="p26_list_answer[]" onchange="addListP26()">
                                                        @endfor
                                                    </div>
                                                    <button type="button" class="btn btn-success" onclick="addAnswerP26()">Add</button>
                                                </div>
                                                <div class="col-md-8">
                                                    <table class="table table-hover">
                                                        <tr>
                                                            <td>Question</td>
                                                            <td>Correct Answer</td>
                                                        </tr>
                                                        @for($i=0; $i<5; $i++)
                                                            <tr>
                                                                <td>
                                                                    <input type="text" class="form-control" name="p26_list_question[]">
                                                                </td>
                                                                <td>
                                                                    <select class="form-control smiller-meaning-correct" name="p26_list_correct_answer[]"></select>
                                                                </td>
                                                            </tr>
                                                        @endfor
                                                    </table>
                                                </div>
                                                <hr>
                                            </div>
                                            <div class="opposite-meaning row">
                                                <h3>Opposite meaning</h3>
                                                <div class="col-md-4">
                                                    <div class="div_p27_list_answer">
                                                        <b>List answer</b>
                                                        @for($i=0; $i<5; $i++)
                                                            <input class="p27_list_answer form-control" type="text" name="p27_list_answer[]" onchange="addListP27()">
                                                        @endfor
                                                    </div>
                                                    <button type="button" class="btn btn-success" onclick="addAnswerP27()">Add</button>
                                                </div>
                                                <div class="col-md-8">
                                                    <table class="table table-hover">
                                                        <tr>
                                                            <td>Question</td>
                                                            <td>Correct Answer</td>
                                                        </tr>
                                                        @for($i=0; $i<5; $i++)
                                                            <tr>
                                                                <td>
                                                                    <input type="text" class="form-control" name="p27_list_question[]">
                                                                </td>
                                                                <td>
                                                                    <select class="form-control opposite-meaning-correct" name="p27_list_correct_answer[]"></select>
                                                                </td>
                                                            </tr>
                                                        @endfor
                                                    </table>
                                                </div>
                                                <hr>
                                            </div>
                                            <div class="complete-sentence row">
                                                <h3>Complete sentence</h3>
                                                <div class="col-md-4">
                                                    <div class="div_p28_list_answer">
                                                        <b>List answer</b>
                                                        @for($i=0; $i<5; $i++)
                                                            <input class="p28_list_answer form-control" type="text" name="p28_list_answer[]" onchange="addListP28()">
                                                        @endfor
                                                    </div>
                                                    <button type="button" class="btn btn-success" onclick="addAnswerP28()">Add</button>
                                                </div>
                                                <div class="col-md-8">
                                                    <table class="table table-hover">
                                                        <tr>
                                                            <td>Question</td>
                                                            <td>Correct Answer</td>
                                                        </tr>
                                                        @for($i=0; $i<5; $i++)
                                                            <tr>
                                                                <td>
                                                                    <input type="text" class="form-control" name="p28_list_question[]">
                                                                </td>
                                                                <td>
                                                                    <select class="form-control complete-sentence-correct" name="p28_list_correct_answer[]"></select>
                                                                </td>
                                                            </tr>
                                                        @endfor
                                                    </table>
                                                </div>
                                                <hr>
                                            </div>
                                            <div class="words-matching row">
                                                <h3>Words matching</h3>
                                                <div class="col-md-4">
                                                    <div class="div_p29_list_answer">
                                                        <b>List answer</b>
                                                        @for($i=0; $i<5; $i++)
                                                            <input class="p29_list_answer form-control" type="text" name="p29_list_answer[]" onchange="addListP29()">
                                                        @endfor
                                                    </div>
                                                    <button type="button" class="btn btn-success" onclick="addAnswerP29()">Add</button>
                                                </div>
                                                <div class="col-md-8">
                                                    <table class="table table-hover">
                                                        <tr>
                                                            <td>Question</td>
                                                            <td>Correct Answer</td>
                                                        </tr>
                                                        @for($i=0; $i<5; $i++)
                                                            <tr>
                                                                <td>
                                                                    <input type="text" class="form-control" name="p29_list_question[]">
                                                                </td>
                                                                <td>
                                                                    <select class="form-control words-matching-correct" name="p29_list_correct_answer[]"></select>
                                                                </td>
                                                            </tr>
                                                        @endfor
                                                    </table>
                                                </div>
                                                <hr>
                                            </div>
                                            <div class="word-combinations row">
                                                <h3>Word combinations</h3>
                                                <div class="col-md-4">
                                                    <div class="div_p30_list_answer">
                                                        <b>List answer</b>
                                                        @for($i=0; $i<5; $i++)
                                                            <input class="p30_list_answer form-control" type="text" name="p30_list_answer[]" onchange="addListP30()">
                                                        @endfor
                                                    </div>
                                                    <button type="button" class="btn btn-success" onclick="addAnswerP30()">Add</button>
                                                </div>
                                                <div class="col-md-8">
                                                    <table class="table table-hover">
                                                        <tr>
                                                            <td>Question</td>
                                                            <td>Correct Answer</td>
                                                        </tr>
                                                        @for($i=0; $i<5; $i++)
                                                            <tr>
                                                                <td>
                                                                    <input type="text" class="form-control" name="p30_list_question[]">
                                                                </td>
                                                                <td>
                                                                    <select class="form-control word-combinations-correct" name="p30_list_correct_answer[]"></select>
                                                                </td>
                                                            </tr>
                                                        @endfor
                                                    </table>
                                                </div>
                                                <hr>
                                            </div>
                                        </div>                                       
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-lg-8 col-lg-offset-2 text-right"  style="margin-top: 20px;">
                                    <button class="btn btn-success"  type="submit">Cập nhật đề</button>
                                    <button class="btn btn-danger" type="reset">Huỷ bỏ</button>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!--/.row-->
</div>
<!--end main-->
@endsection

@section('script')
    <script>
        // Grammar
        const Show = ()=>{
            console.log("ghvhvd");
            for(let i = 0; i <= 25; i++ ){
                $(".question").append(`
                    <div class="row">
                        <div class="col-md-6">
                            <b><u>Question ${i+1}:</u></b>
                            <textarea name="question_grammar_${i}[]" placeholder="Question ${i+1}" rows="5" class="form-control"></textarea>
                            <b>Answer choice:</b>
                            <div class="row">
                                <input style="display: none" type="radio" name="question_grammar_${i}[]" checked value="null">
                                <div class="col-md-3">
                                    <label for="ques_${i}_A">A. <input id="ques_${i}_A" type="radio" name="question_grammar_${i}[]" value="A"></label>
                                </div>
                                <div class="col-md-3">
                                    <label for="ques_${i}_B">B. <input id="ques_${i}_B" type="radio" name="question_grammar_${i}[]" value="B"></label>
                                </div>
                                <div class="col-md-3">
                                    <label for="ques_${i}_C">C. <input id="ques_${i}_C" type="radio" name="question_grammar_${i}[]" value="C"></label>
                                </div>
                                <div class="col-md-3">
                                    <label for="ques_${i}_D">D. <input id="ques_${i}_D" type="radio" name="question_grammar_${i}[]" value="D"></label>
                                </div>
                               
                            </div>
                        </div>
                        <div class="col-md-6">
                            <b>List answer</b>
                            <input class="form-control" type="text" name="question_grammar_${i}[]" placeholder="Answer A"><br>
                            <input class="form-control" type="text" name="question_grammar_${i}[]" placeholder="Answer B"><br>
                            <input class="form-control" type="text" name="question_grammar_${i}[]" placeholder="Answer C"><br>
                            <input class="form-control" type="text" name="question_grammar_${i}[]" placeholder="Answer D">
                        </div>
                    </div>
                    <hr>

                `)
            }    
        };
        $( document ).ready(function() {
            Show();
        });

        //  Vocabulary
        const addAnswerP26 = () => {
            $('.div_p26_list_answer').append(`
                <input class="p26_list_answer form-control" type="text" name="p26_list_answer[]" onchange="addListP26()">
            `)
            addListP26();
        }
        const addAnswerP27 = () => {
            $('.div_p27_list_answer').append(`
                <input class="p27_list_answer form-control" type="text" name="p27_list_answer[]" onchange="addListP27()">
            `)
            addListP27();
        }
        const addAnswerP28 = () => {
            $('.div_p28_list_answer').append(`
                <input class="p28_list_answer form-control" type="text" name="p28_list_answer[]" onchange="addListP28()">
            `)
            addListP28();
        }
        const addAnswerP29 = () => {
            $('.div_p29_list_answer').append(`
                <input class="p29_list_answer form-control" type="text" name="p29_list_answer[]" onchange="addListP29()">
            `)
            addListP29();
        }
        const addAnswerP30 = () => {
            $('.div_p30_list_answer').append(`
                <input class="p30_list_answer form-control" type="text" name="p30_list_answer[]" onchange="addListP30()">
            `)
            addListP30();
        }

        // ------------
        const addListP26 = () => {
            $(".smiller-meaning-correct").html("");
            let list = $(".p26_list_answer");
            for(i=0; i < list.length; i++ ){
                $('.smiller-meaning-correct').append(`
                    <option value="${ list.eq(i).val() }">${ list.eq(i).val() }</option>
                `)
            }
        }
        const addListP27 = () => {
            $(".opposite-meaning-correct").html("");
            let list = $(".p27_list_answer");
            for(i=0; i < list.length; i++ ){
                $('.opposite-meaning-correct').append(`
                    <option value="${ list.eq(i).val() }">${ list.eq(i).val() }</option>
                `)
            }
        }
        const addListP28 = () => {
            $(".complete-sentence-correct").html("");
            let list = $(".p28_list_answer");
            for(i=0; i < list.length; i++ ){
                $('.complete-sentence-correct').append(`
                    <option value="${ list.eq(i).val() }">${ list.eq(i).val() }</option>
                `)
            }
        }
        const addListP29 = () => {
            $(".words-matching-correct").html("");
            let list = $(".p29_list_answer");
            for(i=0; i < list.length; i++ ){
                $('.words-matching-correct').append(`
                    <option value="${ list.eq(i).val() }">${ list.eq(i).val() }</option>
                `)
            }
        }
        const addListP30 = () => {
            $(".word-combinations-correct").html("");
            let list = $(".p30_list_answer");
            for(i=0; i < list.length; i++ ){
                $('.word-combinations-correct').append(`
                    <option value="${ list.eq(i).val() }">${ list.eq(i).val() }</option>
                `)
            }
        }
        
    </script>
    <script src="./../ckeditor/ckeditor.js"></script>

@endsection