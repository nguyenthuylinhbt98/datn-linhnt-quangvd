<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Teacher;
use App\User;
use App\Models\ClassMember;
use Auth;

class ClassroomController extends Controller
{
    public function getClass(Request $request, int $id) {
        $user_id = Auth::user()->id;
        if($user_id) {
            $class = Teacher::findOrFail($id);
            if ($user_id == $class->teacher_id) {
                return $this->teacherJoinClass($id);
            }
            return $this->memberJoinClass($user_id, $id);
        }
        return redirect('/register');
    }

    public function teacherJoinClass(int $class_id) {
        $members = ClassMember::where('class_id', $class_id)
                                        ->where('status', 1)
                                        ->get();
        $waitting = ClassMember::where('class_id', $class_id)
                                ->where('status', 0)
                                ->get();
        foreach($members as $member) {
            $member['user'] = User::findOrFail($member->user_id)->full;
        }

        foreach($waitting as $item) {
            $item['user'] = User::findOrFail($item->user_id)->full;
        }

        return view('user.elements.teacher.getClass', compact('members', 'waitting', 'class_id'));
    }

    public function memberJoinClass(int $user_id, int $class_id) {
        $user = ClassMember::where('user_id', $user_id)
                                ->where('class_id', $class_id)
                                ->where('status', 1)
                                ->get();
        if($user) {
            $members = ClassMember::where('class_id', $class_id)
                                            ->where('status', 1)
                                            ->get();
            foreach($members as $member) {
                $member['user'] = User::findOrFail($member->user_id)->full;
            }
            return view('user.elements.teacher.memberJoinClass', compact('members'));
        }
        return view('user.elements.teacher.joinClass', compact('class_id'));
    }

    public function joinClass( int $id) {
        $data['user_id'] = Auth::user()->id;
        $data['class_id'] = $id;
        $member = ClassMember::where('user_id', $data['user_id'])
                                ->where('class_id', $id)
                                ->get();
        if($member) {
            ClassMember::create($data);
            return redirect()->back()->with('thongbao', 'You have just requested to join the class.</br>Please wait for administrator approval to continue');
        } else {
            return redirect()->back()->with('thongbao', "You have requested group access before.<br> Please wait for the administrator's approval to continue");
        }
    }

    public function approvalMember(int $class_id, int $user_id) {
        $class = Teacher::findOrFail($class_id);
        if (Auth::user()->id == $class->teacher_id) {
            $member = ClassMember::where('user_id', $user_id)
                                ->where('class_id', $class_id)
                                ->first();
            $member['status'] = 1;
            $member->update();
            return redirect()->back()->with('thongbao', "You just approved a user to join the group");
        }
    }

    public function banMember(int $class_id, int $user_id) {
        $class = Teacher::findOrFail($class_id);
        if (Auth::user()->id == $class->teacher_id) {
            $member = ClassMember::where('user_id', $user_id)
                                ->where('class_id', $class_id)
                                ->first();
            $member->delete();
            return redirect()->back()->with('thongbao', "You just removed 1 member from this group");
        }
    }
}
