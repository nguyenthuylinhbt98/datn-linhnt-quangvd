@extends('admin.master')

@section('content')
     <!--main-->
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Tổng hợp đề</h1>
            </div>
        </div>

        @if(session('thongbao'))
           <div class="alert alert-success">
                {{ session('thongbao') }}
            </div>

        @endif
        {{-- @dd($test) --}}

        <!--/.row-->
    <form action="/admin/toeic/test/{{$test->id}}" method="post" accept-charset="utf-8">
        @csrf
        <input type="hidden" name="_method" value="put">
        <input type="hidden" name="id" value="{{$test->id}}">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12">
                    <div class="panel panel-primary">
                        <div class="panel-heading"><i class="fas fa-user"></i> Tạo đề thi mới</div>
                        <div class="panel-body">
                            <div class="row justify-content-center" style="margin-bottom:40px">

                                <div class="col-md-8 col-lg-8 col-lg-offset-2">
                                 
                                    <div class="form-group">
                                        <label><b>Name</b></label>
                                        <input value="{{$test->name_test}}" type="text" name="name_test" class="form-control">
                                         {{ ShowErrors($errors, 'name')}}
                                    </div>
                                    <div class="form-group">
                                        <label><b>Phân hóa mức điểm</b></label>
                                        <select class="form-control" name="level">
                                            <option @if($test->level==350) selected  @endif value="350">350+</option>
                                            <option @if($test->level==450) selected  @endif value="450">450+</option>
                                            <option @if($test->level==550) selected  @endif value="550">550+</option>
                                            <option @if($test->level==750) selected  @endif value="750">750+</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label><b>Phân loại đề</b></label>
                                        <select class="form-control" name="classify">
                                            <option @if($test->classify==="practice") selected  @endif value="practice">Luyện tập</option>
                                            <option @if($test->classify==="test") selected  @endif value="test">Thi thử</option>
                                            
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <label><b>Trả phí</b></label>
                                        <select class="form-control" name="role">
                                            <option @if($test->role==="free") selected  @endif value="free">Miễn phí</option>
                                            <option @if($test->role==="fee") selected  @endif value="fee">Trả phí</option>
                                            
                                        </select>
                                    </div>

                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label><b>Part I</b></label>
                                            <select class="form-control" name="id_part[]">
                                                <option value="0"> Chọn Part I</option>
                                                option
                                                @foreach($part_1 as $item)
                                                    <option @if($test->id_part[0] == $item->id) selected  @endif value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach    
                                            </select>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label><b>Part II</b></label>
                                            <select class="form-control" name="id_part[]">
                                                <option value="0"> Chọn Part II</option>

                                                @foreach($part_2 as $item)
                                                    <option @if($test->id_part[1]== $item->id) selected  @endif value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach    
                                            </select>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label><b>Part III</b></label>
                                            <select class="form-control" name="id_part[]">
                                                <option value="0"> Chọn Part III</option>

                                                @foreach($part_3 as $item)
                                                    <option @if($test->id_part[2]== $item->id) selected  @endif value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach    
                                            </select>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label><b>Part IV</b></label>
                                            <select class="form-control" name="id_part[]">
                                                <option value="0"> Chọn Part IV</option>

                                                @foreach($part_4 as $item)
                                                    <option @if($test->id_part[3]== $item->id) selected  @endif value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach    
                                            </select>
                                        </div>
                                    </div>

                                    <div class="col-md-2">
                                        
                                    </div>

                                    <div class="col-md-5">
                                        <div class="form-group">
                                            <label><b>Part V</b></label>
                                            <select class="form-control" name="id_part[]">
                                                <option value="0"> Chọn Part V</option>

                                                @foreach($part_5 as $item)
                                                    <option @if($test->id_part[4]== $item->id) selected  @endif value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach    
                                            </select>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label><b>Part VI</b></label>
                                            <select class="form-control" name="id_part[]">
                                                <option value="0"> Chọn Part VI</option>

                                                @foreach($part_6 as $item)
                                                    <option @if($test->id_part[5]== $item->id) selected  @endif value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach    
                                            </select>
                                        </div>
                                        
                                        <div class="form-group">
                                            <label><b>Part VII</b></label>
                                            <select class="form-control" name="id_part[]">
                                                <option value="0"> Chọn Part VII</option>

                                                @foreach($part_7 as $item)
                                                    <option @if($test->id_part[6]== $item->id) selected  @endif value="{{$item->id}}">{{$item->name}}</option>
                                                @endforeach    
                                            </select>
                                        </div>
                                    </div>
                                     
                                    
                                    
                                       
                                    
                                </div>
                                <div class="row">
                                    <div class="col-md-8 col-lg-8 col-lg-offset-2 text-right">
                                      
                                        <button class="btn btn-success"  type="submit">Tạo đề thi mới</button>
                                        <button class="btn btn-danger" type="reset">Huỷ bỏ</button>
                                    </div>
                                </div>
                               

                            </div>
                        
                            <div class="clearfix"></div>
                        </div>
                    </div>

            </div>
        </div>
    </form>

        <!--/.row-->
    </div>

    <!--end main-->

@endsection
