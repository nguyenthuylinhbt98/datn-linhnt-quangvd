<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAptisSpeakTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aptis_speak', function (Blueprint $table) {
            $table->id();
            $table->string('name', 50)->nullable();
            $table->longText('p1_ques1')->nullable();
            $table->longText('p1_ques2')->nullable();
            $table->longText('p1_ques3')->nullable();
            $table->string('img_p2', 191)->nullable();
            $table->longText('p2_ques1')->nullable();
            $table->longText('p2_ques2')->nullable();
            $table->longText('p2_ques3')->nullable();
            $table->string('img_p3', 191)->nullable();
            $table->longText('p3_ques1')->nullable();
            $table->longText('p3_ques2')->nullable();
            $table->longText('p3_ques3')->nullable();
            $table->string('img_p4', 191)->nullable();
            $table->longText('p4_ques')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aptis_speak_');
    }
}
