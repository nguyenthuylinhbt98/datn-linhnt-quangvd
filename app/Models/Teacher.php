<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Teacher extends Model
{
    protected $table = 'class';

    protected $fillable = [
        'course', 'time', 'start_date', 'fee', 'teacher_id'
    ];
}
