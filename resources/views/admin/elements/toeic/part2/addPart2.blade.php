@extends('admin.master')
@section('content')
<!--main-->
<div class="container main exam">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Cập nhật đề thi</h1>
        </div>
    </div> 
   @if(session('thongbao'))
       <div class="alert alert-success">
            {{ session('thongbao') }}
        </div>

    @endif
    <!--/.row-->
    <form action="/admin/toeic/part_2" method="post" accept-charset="utf-8" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><i class="fas fa-user"></i>Add new Part II</div>
                    <div class="panel-body">
                        <div class="row justify-content-center" style="margin-bottom:40px">
                            <div class="col-md-8 col-lg-8 col-lg-offset-2">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input value="{{old('name')}}" type="text" name="name" placeholder="Name test" class="form-control" required>
                                    {{ ShowErrors($errors, 'name')}}
                                </div>
                                <div class="container">
                                    <h2>Exam Toeic</h2>
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#part1">Part II</a></li>
                                        
                                    </ul>
                                    <div class="tab-content">
                                        
                                        {{-- Part_2 --}}
                                        <div id="part1" class="tab-pane fade in active">
                                            <h3><b>PART II</b></h3>
                                            <p> <b>Directions:</b> Listen to these questions and statements. After each question or statement, you will hear three responses. Select the most appropriate response. Mark your answer by clicking (A), (B), or (C). You will hear each question or statement, and the responses, only once.</p>
                                            <p><b>Example:</b></p>
                                            <textarea class="form-control" id="summary-ckeditor" type="text" name="example_test2"  placeholder="Guide to choose the answer" style="height: 200px;"></textarea>
                                            <br><b>Voice: </b><br>
                                            <input type="file" name="void_part2"><br>
                                            <label for="begin">
                                                Question begin: <input type="text" id="begin" name="begin" value="7">
                                            </label><br>
                                            <label for="end">
                                                Question end: <input type="text" id="end" name="end" value="31">
                                            </label><br><hr>

                                            <div class="question">

                                            </div>
                                        </div>                                       
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-lg-8 col-lg-offset-2 text-right"  style="margin-top: 20px;">
                                    <button class="btn btn-success"  type="submit">Cập nhật đề</button>
                                    <button class="btn btn-danger" type="reset">Huỷ bỏ</button>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!--/.row-->
</div>
<!--end main-->
@endsection

@section('script')
    <script>
        const Show = ()=>{
            var begin = Number($("#begin").val());
            var end = Number($("#end").val());
            for(let i = 0; i <= end-begin; i++ ){
                $(".question").append(`
                    <b>Question ${i+begin}:</b>
                    <div class="row">
                        <div class="col-md-1">
                            <p style="float: left; margin-right: 5px;">A</p> <input type="radio" name="answer_question_${i}" value="A">
                        </div>
                        <div class="col-md-1">
                            <p style="float: left; margin-right: 5px;">B</p> <input type="radio" name="answer_question_${i}" value="B">
                        </div>
                        <div class="col-md-1">
                            <p style="float: left; margin-right: 5px;">C</p> <input type="radio" name="answer_question_${i}" value="C">
                        </div>
                    </div>
                    <hr>
                `)
            }
        };
        Show();
        $("#begin").on('change', ()=>{
            $(".question").children().remove();
            Show();
        });
        $("#end").on('change', ()=>{
            $(".question").children().remove();
            Show();
        });
    </script>

    <script src="./../ckeditor/ckeditor.js"></script>
    <script>
        CKEDITOR.replace( 'summary-ckeditor');
    </script>
@endsection