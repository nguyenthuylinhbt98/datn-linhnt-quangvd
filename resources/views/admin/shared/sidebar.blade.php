
<div class="col-md-3 left_col">
   <div class="left_col scroll-view" style="width: 100%">
      <div class="clearfix"></div>
      <!-- menu profile quick info -->
      <div class="profile clearfix">
         <div class="profile_pic">
              @if(Auth::user()->avatar)
                <img src="../{{Auth::user()->avatar}}" alt="" class="img-circle profile_img" style="width: 45px; height: 50px; border-radius: 50%">
              @else
                <img src="../upload/user_avatar/no_avatar.png" alt="..." class="img-circle profile_img">
              @endif  
               
         </div>
         <div class="profile_info" >
            <span>Xin chào,</span>
            <h2>{{Auth::user()->full}}</h2>
         </div>
      </div>
      <!-- /menu profile quick info -->
      <br />
      <!-- sidebar menu -->
      <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
         <div class="menu_section">
            
            <ul class="nav side-menu">
               
              @if(Auth::user()->level == 1)
                  <li>
                      <a href="/admin/"><i class="fa fa-home"></i> Home</a>
                  </li>

                  <li>
                      <a href="/admin/user"><i class="fa fa-users"></i> Users</a>
                  </li>
              @endif

              {{-- Toeic --}}
               <li>
                  <a><i class="fa fa-edit"></i> Toeic <span class="fa fa-chevron-down"></span></a>
                      <ul class="module nav child_menu">
                        @for($i=1; $i<8; $i++)
                          <li>           
                            <a style="padding-left: 25px;" href="/admin/toeic/part_{{$i}}" title="Cập nhật Part {{$i}}"> Part {{$i}}</a>  
                          </li>

                        @endfor
                        <li>
                          <a style="padding-left: 25px;" href="/admin/toeic/test" title="Toeic Test">Toeic Test</a>
                        </li>

                        <li>
                          <a style="padding-left: 25px;" href="/admin/toeic/transcript" title="Toeic Test">Transcript</a>
                        </li>


                      </ul>
               </li>
               {{-- Aptis --}}
               <li>
                  <a><i class="fa fa-edit"></i> Aptis <span class="fa fa-chevron-down"></span></a>
                      <ul class="module nav child_menu">
                        <li>
                          <a style="padding-left: 25px;" href="/admin/aptis/listening" title="Listening"> Listening</a>  
                        </li>
                        <li>
                          <a style="padding-left: 25px;" href="/admin/aptis/reading" title="Reading"> Reading</a>  
                        </li>
                        <li>
                          <a style="padding-left: 25px;" href="/admin/aptis/writing" title="Writing"> Writing</a>  
                        </li>
                        <li>
                          <a style="padding-left: 25px;" href="/admin/aptis/speaking" title="Speaking"> Speaking</a>  
                        </li>
                        <li>
                          <a style="padding-left: 25px;" href="/admin/aptis/gv" title="Grammar and vocabulary"> Grammar and vocabulary</a>  
                        </li>
                        <li>
                          <a style="padding-left: 25px;" href="/admin/aptis/test" title="Toeic Test">Aptis Test</a>
                        </li>
                      </ul>
               </li>
              @if(Auth::user()->level == 1)
                  <li>
                      <a href="/admin/post"><i class="fa fa-newspaper-o"></i>Post</a>
                  </li>
              @endif
               <li>
                <a href="/logout"><i class="fa fa-sign-out"></i> Logout</a>
            </li>
            </ul>
         </div>
      </div>
   </div>
</div>