<?php

namespace App\Repositories\Admin\Toeic;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Models\Toeic\Part7;

//use Your Model

/**
 * Class Part7Repository.
 */
class Part7Repository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Part7::class;
    }

    public function createPart7(array $data)
    {
        $part_7['name'] = $data['name'];
        $part_7['num_para'] = $data['num_para'];
        $part_7['begin'] = $data['begin'];

        for ($i=0; $i<15; $i++) {
            $part_7['paragraph_'.$i]=implode('; ;',(array)$data['paragraph_'.$i]);

        };

        for ($i=0; $i<55; $i++){
            $part_7['question_'.$i]=implode('; ;',(array)$data['question_'.$i]);
        }

        Part7::create($part_7);        
        return true;
    }

    // ---------------
    public function editPart7(int $id)
    {
        $part_7 = self::getById($id);    
        for ($i=0; $i<15; $i++) {
            (array)$part_7['paragraph_'.$i]=explode('; ;',$part_7->{'paragraph_'.$i});
        };
        
        for ($i=0; $i<55; $i++){
            (array)$part_7['question_'.$i]=explode('; ;',$part_7->{'question_'.$i});
        }
        return $part_7;
    }

    // ----------------
    public function updatePart7(array $data, int $id){
        $part7 = self::getById($id);

        $part_7['name'] = $data['name'];
        $part_7['num_para'] = $data['num_para'];
        $part_7['begin'] = $data['begin'];
        // dd($data);
        for ($i=0; $i<15; $i++) {
            if(!empty($data['paragraph_'.$i])){
                $part_7['paragraph_'.$i]=implode('; ;',(array)$data['paragraph_'.$i]);
            }
        };

        for ($i=0; $i<55; $i++){
            if(!empty($data['question_'.$i])){
                $part_7['question_'.$i]=implode('; ;',(array)$data['question_'.$i]);
            }
        }
        $part7->update($part_7);        
        return true;
    } 
}
