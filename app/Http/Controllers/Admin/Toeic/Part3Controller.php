<?php

namespace App\Http\Controllers\Admin\Toeic;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Toeic\Part3;
use App\Http\Requests\Toeic\Part3\{AddPart3Request, EditPart3Request};
use App\Repositories\Admin\Toeic\Part3Repository;

class Part3Controller extends Controller
{
    private $part3Repository;

    public function __construct()
    {
        $this->part3Repository = new Part3Repository();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['part3'] = Part3::paginate(10);
        return view('admin.elements.toeic.part3.listPart3', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.elements.toeic.part3.addPart3');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddPart3Request $r)
    {
        $data = $r->all();
        $part_3 = $this->part3Repository->createPart3($data); 
        if($part_3){   
             return redirect('/admin/toeic/part_3')->with('thongbao', 'Success create new part III test'); 
        }  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['part3'] = $this->part3Repository->editPart3($id); 
        return view('admin.elements.toeic.part3.showPart3', $data);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['part3'] = $this->part3Repository->editPart3($id); 
        return view('admin.elements.toeic.part3.editPart3', $data);
        
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditPart3Request $r, $id)
    {
        $data = $r->all();
        $part_3 = $this->part3Repository->updatePart3($data, $id); 
        if($part_3){   
             return redirect('/admin/toeic/part_3')->with('thongbao', 'Success edit part III'); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Part3::destroy($id);
        return redirect()->back()->with('thongbao', 'Đã xóa thành công!!!');
    }
}
