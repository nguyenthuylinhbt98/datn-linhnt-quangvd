@extends('admin.master')
@section('admin', 'class=active')
@section('content')
<!--main-->
<div class="container main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <a href="/admin/post/create" class="btn btn-primary" style="margin-bottom: 20px;">Add new post</a>
                @if(session('thongbao'))
                  <div class="alert alert-success">
                      {{ session('thongbao') }}
                  </div>
                @endif
                @if(session('canhbao'))
                  <div class="alert alert-danger">
                      {{ session('canhbao') }}
                  </div>
                @endif
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>List Posts  <small></small></h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
               
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              
              <table id="datatable" class="table table-striped table-bordered" style="text-align: center;">
                <thead>
                  <tr style="background: #daeaf2" >
                    <th style="text-align: center;">ID</th>
                    <th style="text-align: center;">Image</th>
                    <th style="text-align: center;">Title</th>
                    <th style="text-align: center;">Short description</th>
                    <th style="text-align: center;">Action</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($posts as $item)

                    <tr>
                      <td>{{$item->id}}</td>
                      <td>
                        @if(isset($item->image))
                          <img src="../{{$item->image}}" alt="" style="width: 150px; height: 80px; object-fit: cover">
                        @else
                          <img src="../upload/item_avatar/no_avatar.png" alt="" style="width: 150px; height: 80px; object-fit: cover">
                        @endif
                        
                      </td>
                      <td>{{ $item->title }}</td>
                      <td>{{ $item->short_description }}</td>
                      <td>
                        <a href="/admin/post/{{ $item->id }}/edit" class="btn btn-round btn-primary btn-xs"><i class="fa fa-pencil" aria-hidden="true"></i> Sửa</a>
                        <a href="#" class="btn btn-round btn-danger btn-xs" data-toggle="modal" data-target="#exampleModal{{$item->id}}"><i class="fa fa-trash" aria-hidden="true"></i> Xóa</a>

                            <div class="modal fade" id="exampleModal{{$item->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                      <div class="modal-header">
                                          <h5 class="modal-title" id="exampleModalLabel">Delete item</h5>
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                          </button>
                                      </div>
                                      
                                      <form action="/admin/post/{{ $item->id }}" method="POST" accept-charset="utf-8">
                                          <div class="modal-body">
                                              @csrf
                                              <input type="hidden" name="_method" value="delete">
                                              <h4>Xóa <b style="color: red">{{ $item->title }}</b> </h4>
                                          </div>
                                          <div class="modal-footer">
                                              <button type="button" class="btn btn-default" data-dismiss="modal">Quay lại</button>
                                              <button type="submit" class="btn btn-danger">Xóa</button>
                                          </div>
                                      </form>
                                  </div>
                              </div>
                            </div>
                      </td>
                    </tr>
                  @endforeach
                  
                </tbody>

              </table>
              <div align="center">
                {{$posts->links()}}
              </div>
              
            </div>
          </div>
        </div>
        
    </div>
</div>
<!--end main-->
@endsection