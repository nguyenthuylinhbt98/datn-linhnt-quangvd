<?php 

// trong app tạo folder helper
// tạo file function trong folder helper, viết các hàm xử lí tại đây


// vào file composer.json thêm đường dẫn vào phần autoload
// "autoload": { 

// 	.....
// 	"files": [ 
// 	"app/helper/function.php" 
// 	]  

// 	}, 


// chạy lệnh  composer dump-autoload, trong file (vendor/composer/autoload_file.php tự sinh ra thêm 1 đường dẫn (ktra): '4ef25952a3d3f7c399fe269e8e36b783' => $baseDir . '/app/helper/function.php',  )

// => thường dùng để viết API

function ShowErrors($errors, $nameinput){

	if($errors->has($nameinput))
	{
        echo '<br/><div class="alert alert-danger" ><strong>';

        echo $errors->first($nameinput);
            
        echo "</strong></div>";
                                        
    }
}


function UploadFile($file, $folder)
{         
    $fileName = $file->getClientOriginalName();
    $folderPath = "upload/" . $folder;        
    $random = Str::random(5);
    $fileName = $random ."_".$fileName;
    $file->move($folderPath, $fileName);
    return $folderPath . '/' . $fileName;      
}



function showEdit($caubatdau, $socau, $question, $paragraph){
    $a = $caubatdau;
    for ($i=0; $i<15; $i++) { 
        $b = $i+1;
        if (isset($socau[$i])) {
            echo '<div>
                    <b>Paragraph '.$b.'</b>
                    <textarea name="paragraph_'.$b.'">'.$paragraph[0].'</textarea><br>
                    <label>Số câu hỏi</label>                                
                    <input type="number" onchange="Para'.$b.'(this)" name="ques_'.$b.'" class="form-control" value="'.$socau[$i].'">
                </div>
                <div style="padding-top: 10px" class="cau_hoi_para'.$b.'">';

            for ($j=0; $j < $socau[$i]; $j++) { 
                $cau=$caubatdau + $i +$j;
                $ques = $cau - $a;
                echo '<b>Question '.$cau.':</b><br>
                      <div>';
                    if(isset($question[$ques][0])){
                        echo'<input class="form-control" type="text" value="'.$question[$ques][0].'" name="question_'.$cau.'[]" placeholder="Question "><br>';
                    }
                    else{
                        echo'<input class="form-control" type="text" value="" name="question_'.$cau.'[]" placeholder="Question "><br>';
                    }

                    if(isset($question[$ques][1])){
                        echo'<input class="form-control" type="text" value="'.$question[$ques][1].'" name="question_'.$cau.'[]" placeholder="Question "><br>';
                    }
                    else{
                        echo'<input class="form-control" type="text" value="" name="question_'.$cau.'[]" placeholder="Question "><br>';
                    }

                    if(isset($question[$ques][2])){
                        echo'<input class="form-control" type="text" value="'.$question[$ques][2].'" name="question_'.$cau.'[]" placeholder="Question "><br>';
                    }
                    else{
                        echo'<input class="form-control" type="text" value="" name="question_'.$cau.'[]" placeholder="Question "><br>';
                    }

                    if(isset($question[$ques][3])){
                        echo'<input class="form-control" type="text" value="'.$question[$ques][3].'" name="question_'.$cau.'[]" placeholder="Question "><br>';
                    }
                    else{
                        echo'<input class="form-control" type="text" value="" name="question_'.$cau.'[]" placeholder="Question "><br>';
                    }

                    if(isset($question[$ques][4])){
                        echo'<input class="form-control" type="text" value="'.$question[$ques][4].'" name="question_'.$cau.'[]" placeholder="Question "><br>';
                    }
                    else{
                        echo'<input class="form-control" type="text" value="" name="question_'.$cau.'[]" placeholder="Question "><br>';
                    }

                    echo'<b>Answer choice:</b>';

                    if(isset($question[$ques][5])){
                        echo'<div class="row">
                            <div class="col-md-1">
                                <p style="float: left; margin-right: 5px;">A</p> <input if($question['.$ques.'][5]=="A") checked endif type="radio" name="question_'.$cau.'[]" value="A">
                            </div>
                            <div class="col-md-1">
                                <p style="float: left; margin-right: 5px;">B</p> <input if($question['.$ques.'][5]=="B") checked endif type="radio" name="question_'.$cau.'[]" value="B">
                            </div>
                            <div class="col-md-1">
                                <p style="float: left; margin-right: 5px;">C</p> <input if($question['.$ques.'][5]=="C") checked endif type="radio" name="question_'.$cau.'[]" value="C">
                            </div>
                             <div class="col-md-1">
                                <p style="float: left; margin-right: 5px;">D</p> <input if($question['.$ques.'][5]=="D") checked endif type="radio" name="question_'.$cau.'[]" value="D">
                            </div>
                        </div>';
                    }else{
                        echo'<div class="row">
                            <div class="col-md-1">
                                <p style="float: left; margin-right: 5px;">A</p> <input type="radio" name="question_'.$cau.'[]" value="A">
                            </div>
                            <div class="col-md-1">
                                <p style="float: left; margin-right: 5px;">B</p> <input type="radio" name="question_'.$cau.'[]" value="B">
                            </div>
                            <div class="col-md-1">
                                <p style="float: left; margin-right: 5px;">C</p> <input type="radio" name="question_'.$cau.'[]" value="C">
                            </div>
                             <div class="col-md-1">
                                <p style="float: left; margin-right: 5px;">D</p> <input type="radio" name="question_'.$cau.'[]" value="D">
                            </div>
                        </div>';
                    }
                    
                echo'</div>';      
            }    
            echo '</div><div style="clear: both;"></div><hr>';
        }else{
            echo '<div>
                    <b>Paragraph '.$b.'</b>
                    <textarea name="paragraph_'.$b.'"></textarea><br>
                    <label>Số câu hỏi</label>                                
                    <input type="number" onchange="Para'.$b.'(this)" name="ques_'.$b.'" class="form-control" value="'.$socau[$i].'">
                </div>
                <div style="padding-top: 10px" class="cau_hoi_para'.$b.'">
                </div><div style="clear: both;"></div><hr>';
        }
        $caubatdau = $caubatdau + $socau[$i] - 1;
    }
}

 ?>