@extends('admin.master')
@section('content')
<!--main-->
<div class="container main exam">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Add new speaking test</h1>
        </div>
    </div> 
   @if(session('thongbao'))
       <div class="alert alert-success">
            {{ session('thongbao') }}
        </div>

    @endif
    <!--/.row-->
    <form action="/admin/aptis/speaking" method="post" accept-charset="utf-8" enctype="multipart/form-data">
        @csrf
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><i class="fas fa-user"></i>Add speaking test</div>
                    <div class="panel-body">
                        <div class="row justify-content-center" style="margin-bottom:40px">
                            <div class="col-md-8 col-lg-8 col-lg-offset-2">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input value="{{old('name')}}" type="text" name="name" placeholder="Name test" class="form-control" required>
                                    {{ ShowErrors($errors, 'name')}}
                                </div>
                                <div class="container">
                                    <h2>Exam </h2>
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#part1">Part I</a></li>
                                        <li><a data-toggle="tab" href="#part2">Part II</a></li>
                                        <li><a data-toggle="tab" href="#part3">Part III</a></li>
                                        <li><a data-toggle="tab" href="#part4">Part IV</a></li>
                                        
                                    </ul>
                                    <div class="tab-content">
                                        <div id="part1" class="tab-pane fade in active">
                                            @for($i=1; $i<4; $i++)
                                                <div>
                                                    <label for="ques1">{{$i}}.</label>
                                                    <input class="form-control" type="text" name="p1_ques{{$i}}[]" placeholder="Ques{{$i}}"><br>
                                                    <textarea name="p1_ques{{$i}}[]" class="ckeditor" ></textarea>
                                                </div>
                                            @endfor
                                        </div>                                       
                                        <div id="part2" class="tab-pane fade ">
                                            Img part II: 
                                            <input type="file" name="img_p2"> <br>
                                            @for($i=1; $i<4; $i++)
                                                <div>
                                                    <label for="ques2">{{$i}}.</label>
                                                    <input class="form-control" type="text" name="p2_ques{{$i}}[]" placeholder="Ques{{$i}}"><br>
                                                    <textarea name="p2_ques{{$i}}[]" class="ckeditor" ></textarea>
                                                </div>
                                            @endfor
                                        </div>                                       
                                        <div id="part3" class="tab-pane fade ">
                                            Img part III: 
                                            <input type="file" name="img_p3"> <br>
                                            @for($i=1; $i<4; $i++)
                                                <div>
                                                    <label for="ques3">{{$i}}.</label>
                                                    <input class="form-control" type="text" name="p3_ques{{$i}}[]" placeholder="Ques{{$i}}"><br>
                                                    <textarea name="p3_ques{{$i}}[]" class="ckeditor" ></textarea>
                                                </div>
                                            @endfor
                                        </div>                                       
                                        <div id="part4" class="tab-pane fade ">
                                            Img part IV: 
                                            <input type="file" name="img_p4"> <br>
                                           
                                            <div>
                                                <input class="form-control" type="text" name="p4_ques[]" placeholder="Ques{{$i}}"><br>
                                                <textarea name="p4_ques[]" class="ckeditor" ></textarea>
                                            </div>
                                            
                                        </div>                                       
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-lg-8 col-lg-offset-2 text-right"  style="margin-top: 20px;">
                                    <button class="btn btn-success"  type="submit">Cập nhật đề</button>
                                    <button class="btn btn-danger" type="reset">Huỷ bỏ</button>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!--/.row-->
</div>
<!--end main-->
@endsection

@section('script')
    <script src="./../ckeditor/ckeditor.js"></script>
@endsection