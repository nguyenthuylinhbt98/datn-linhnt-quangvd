<?php

namespace App\Http\Requests\Toeic\Part3;

use Illuminate\Foundation\Http\FormRequest;

class EditPart3Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'name'=>'required|unique:toeic_part3,name,'.$this->id,
        ];
    }
    public function messages()
    {
        return [
            'name.required'=>'Tên Part3 không được để trống!',
            'name.unique'=>'Tên Part3 đã tồn tại!',
        ];
    }
}
