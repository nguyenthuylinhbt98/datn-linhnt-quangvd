<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ClassMember extends Model
{
    protected $table = 'class_member';

    protected $fillable = [
        'user_id', 'class_id', 'status'
    ];
}
