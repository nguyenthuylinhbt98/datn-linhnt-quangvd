
I. Chuẩn bị môi trường:
- Cài đặt php 7.3: https://computingforgeeks.com/how-to-install-php-on-centos-fedora/
- Cài đặt xampp 7.4:https://orcacore.com/install-use-xampp-on-centos-7/ 
                    wget https://downloadsapachefriends.global.ssl.fastly.net/7.4.29/xampp-linux-x64-7.4.29-1-installer.run
- Cài đặt compose: https://phoenixnap.com/kb/how-to-install-and-use-php-composer-on-centos-7
II. Setup code
1. git clone https://gitlab.com/nguyenthuylinhbt98/datn-linhnt-quangvd.git
2. mv datn-linhnt-quangvd project
3. cd project
4. php artisan key:generate
5. composer install
6. cp .env.example .env
Sửa biến database name trong file env. Ví dụ "database1"
7. Vào database tạo mới database tên là "database1"
(Vào đường link server xampp (localhost port 80) vừa tạo ở trên để truy cập database)
8. php artisan migrate

9. php artisan db:seed

10. nohup php artisan serve --host=<IP> --port=<Port>

Tạo user admin bằng cách thêm mới user vào bảng user của database  (Password mã hóa MD5)
III. Sử  dụng
Đăng nhập vào trang: IP:port/admin để vào page admin nhập đề và thêm sửa xóa
Đăng nhập vào trang: IP:port để vào giao diện người dùng

