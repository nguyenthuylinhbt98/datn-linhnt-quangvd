@extends('user.master')

@section('css')
	<link rel="stylesheet" href="assets/css/test.css">
@endsection


@section('content')
    
<main class="aptis" id="aptis-listening">
    <!--? slider Area Start-->
    <section class="slider-area slider-area2">
        <div class="slider-active">
            <!-- Single Slider -->
            <div class="single-slider slider-height2">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-8 col-lg-11 col-md-12">
                            <div class="hero__caption hero__caption2">
                                <h1 data-animation="bounceIn" data-delay="0.2s">{{$name}}</h1>
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                                        <li class="breadcrumb-item"><a href="/aptis">Aptis</a></li>
                                        <li class="breadcrumb-item"><a href="/aptis/test/{{ $id }}">{{$name}}</a></li>
                                        <li class="breadcrumb-item"><a>Listening</a></li>
                                    </ol>
                                </nav>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class=" test">
       <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-9 col-lg-10">
                <div class="section-tittle  mb-10 mt-20">
                   <p>Welcome to the Aptis Listening Test. <br>
                    You will listen to <b>twenty five short recordings.</b><br>
                    Click on the play button to listen to each recording. 
                    You can listen to each recording two times only. <br>
                    You have <b> 56 minutes to complete the test</b>.<br>
                </div>
            </div>
            <div class="col-xl-12 col-lg-12">
                <form action="" method="POST">
                    @csrf
                    <input type="hidden" value="{{$listen->id}}" name="listen_id">
                    <div class="row">
                        <div class="col-md-8 col-lg-8">
                            @for ($i = 0; $i < 25; $i++)
                                <div class="question-listening question-{{$i}} {{ $i!=0 ? 'hidden' : 'block'}}">
                                    <b><u>Question {{ $i+1 }}:</u></b><br>
                                    <audio src="../{{ $listen['voice_question_'.$i] }}"  loop controls></audio><br>
                                    <p>{{ $listen['question_listen_'.$i][0] }}</p>
                                    <div class="choices">
                                        <input type="hidden" name="answer_listening_question_{{ $i }}" value="null">
                                        <p>
                                            <span class="my-radio" onclick="activeAns({{ $i+1 }})">
                                                <input name="answer_listening_question_{{ $i }}" type="radio" id="choice-{{ $i+1 }}-A" value="A">
                                                <label for="choice-{{ $i+1 }}-A">A.{{$listen['question_listen_'.$i][2]}}</label>
                                            </span>
                                        </p>
                                        <p>
                                            <span class="my-radio"  onclick="activeAns({{ $i+1 }})">
                                                <input name="answer_listening_question_{{ $i }}" type="radio" id="choice-{{ $i+1 }}-B" value="B">
                                                <label for="choice-{{ $i+1 }}-B">B.{{$listen['question_listen_'.$i][3]}}</label>
                                            </span>
                                        </p>
                                        <p>
                                            <span class="my-radio"  onclick="activeAns({{ $i+1 }})">
                                                <input name="answer_listening_question_{{ $i }}" type="radio" id="choice-{{ $i+1 }}-C" value="C">
                                                <label for="choice-{{ $i+1 }}-C">C.{{$listen['question_listen_'.$i][4]}}</label>
                                            </span>
                                        </p>
                                        <p>
                                            <span class="my-radio"  onclick="activeAns({{ $i+1 }})">
                                                <input name="answer_listening_question_{{ $i }}" type="radio" id="choice-{{ $i+1 }}-D" value="D">
                                                <label for="choice-{{ $i+1 }}-D">D.{{$listen['question_listen_'.$i][5]}}</label>
                                            </span>
                                        </p>
                                    </div>
                                    <div  class="d-flex justify-content-center mb-3 btn-paginate">
                                        @if($i==0) <button type="button" class="btn btn-primary" onclick="showQuestion({{$i+1}})">Next</button>  @endif
                                        @if ($i==24) <button type="button" class="btn btn-primary" onclick="showQuestion({{$i-1}})">Pre</button> @endif
                                        @if ($i!=0 && $i!=24)
                                        <button type="button" class="btn btn-primary" onclick="showQuestion({{$i-1}})">Pre</button>
                                        <button type="button" class="btn btn-primary" onclick="showQuestion({{$i+1}})">Next</button>
                                        @endif
                                        
                                    </div>
                                </div>
                            @endfor
                            <div class="sub d-flex justify-content-center mb-3">
                                <input type="submit" class="genric-btn primary-border circle" value="Submit"/>
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4 ans-map">
                            <div class="time-ans">
                                <i class="far fa-clock"></i>
                                0<span id="h">0</span> :
                                <span id="m">00</span> :
                                <span id="s">00</span>
                            </div>
                            <div class="row">
                                @for($i=1; $i<=25; $i++)
                                    <div class="col-lg-2 col-md-2 choice-ques " >
                                        <div class="ques-{{$i}} ans-ques">{{$i}}</div>
                                    </div>
                                @endfor
                                <hr>
                            </div>
                            
                        </div>
                    </div>
                </form>
              
            </div>
        </div>
       </div>
    </section>
</main>

@endsection


@section('script')
<script>
    var h = 00; // Giờ
    var m = 56; // Phút
    var s = 00; // Giây
    var timeout = null;

    function start()
    {
        if (s === -1){
            m -= 1;
            s = 59;
        }

        if (m === -1){
            h -= 1;
            m = 59;
        }

        if (h == -1){
            clearTimeout(timeout);
            document.getElementById("testToeic").submit();
            return false;
        }

        document.getElementById('h').innerText = h.toString();
        document.getElementById('m').innerText = m.toString();
        document.getElementById('s').innerText = s.toString();

        timeout = setTimeout(function(){
            s--;
            start();
        }, 1000);
    }
    start();

    function activeAns(id){
        $(`.ques-${id}`).addClass('active')
    }
    
    function showQuestion(id){
        $(`.question-listening`).removeClass('block');
        $(`.question-${id}`).addClass('block');

    }
</script>
@endsection
