<?php

namespace App\Http\Controllers\Admin\Aptis;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Aptis\Read;
use App\Repositories\Admin\Aptis\ReadRepository;

class ReadingController extends Controller
{
    private $readRepository;

    
    public function __construct()
    {
        $this->readRepository = new ReadRepository();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['read'] = Read::paginate(10);
        return view('admin.elements.aptis.read.index', $data);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.elements.aptis.read.add');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $r)
    {
        $data = $r->all();
        $read = $this->readRepository->createRead($data); 
        if($read){   
             return redirect('/admin/aptis/reading')->with('thongbao', 'Success create new reading test'); 
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['read'] = $this->readRepository->editRead($id); 
        return view('admin.elements.aptis.read.edit', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $r, $id)
    {
        $data = $r->all();
        $read = $this->readRepository->updateRead($data, $id); 
        if($read){   
             return redirect('/admin/aptis/reading')->with('thongbao', 'Success edit reading test'); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Read::destroy($id);
        return redirect()->back()->with('thongbao', 'Success delete');
    }
}
