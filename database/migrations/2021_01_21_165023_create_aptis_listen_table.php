<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAptisListenTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aptis_listen', function (Blueprint $table) {
            $table->id();
            $table->string('name', 191)->nullable();
            for($i=0; $i<25; $i++){
                $table->string('voice_question_'.$i, 191)->nullable();
                $table->longText('question_listen_'.$i)->nullable();
            };
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aptis_listen');
    }
}
