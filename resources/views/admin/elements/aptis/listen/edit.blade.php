@extends('admin.master')
@section('css')
    <style>
        #part2 input.form-control{
            margin-bottom: 10px;
            width: 90%;
        }
        .tab-content{
            padding: 20px 0px;
        }
    </style>   
@endsection
@section('content')
<!--main-->
<div class="container main exam">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Add new Listening test</h1>
        </div>
    </div> 
   @if(session('thongbao'))
       <div class="alert alert-success">
            {{ session('thongbao') }}
        </div>

    @endif
    <!--/.row-->
    <form action="/admin/aptis/listening/{{ $listen->id }}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="_method" value="put">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><i class="fas fa-user"></i>Add Listening test</div>
                    <div class="panel-body">
                        <div class="row justify-content-center" style="margin-bottom:40px">
                            <div class="col-md-8 col-lg-8 col-lg-offset-2">
                                <div class="form-group">
                                    <label>Name Listening test</label>
                                    <input value="{{old('name', $listen->name)}}" type="text" name="name" placeholder="Name Listening test" class="form-control" required>
                                    {{ ShowErrors($errors, 'name')}}
                                </div>
                                <div class="container">
                                    <h2>Exam </h2>
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#part1">Grammar</a></li>
                                        {{-- <li><a data-toggle="tab" href="#part2">Vocabulary </a></li> --}}
                                    </ul>
                                    <div class="tab-content">
                                        <div id="part1" class="tab-pane fade in active">
                                            @for( $i=0 ; $i<25; $i++)
                                                <div class="question-{{$i}}">
                                                    <div class="row">
                                                        <div class="col-md-6">
                                                            <b><u>Question {{$i+1}}:</u></b>
                                                            <input type="file" name="voice_question_{{$i}}"><br>
                                                            <audio src="../{{ $listen['voice_question_'.$i] }}"  loop controls></audio>
                                                            <textarea name="question_listen_{{$i}}[]" placeholder="Question {{$i+1}}" rows="5" class="form-control">{{ $listen['question_listen_'.$i][0] }}</textarea>
                                                            <b>Answer choice:</b>
                                                            <div class="row">
                                                                <input style="display: none" type="radio" name="question_listen_{{$i}}[]" checked value="null" {{ $listen['question_listen_'.$i][1] == "null" ? "checked" : "" }}>
                                                                <div class="col-md-3">
                                                                    <label for="ques_{{$i}}_A">A. <input  id="ques_{{$i}}_A" type="radio" name="question_listen_{{$i}}[]" value="A" {{ $listen['question_listen_'.$i][1] == "A" ? "checked" : "" }}></label>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <label for="ques_{{$i}}_B">B. <input id="ques_{{$i}}_B" type="radio" name="question_listen_{{$i}}[]" value="B" {{ $listen['question_listen_'.$i][1] == "B" ? "checked" : "" }}></label>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <label for="ques_{{$i}}_C">C. <input id="ques_{{$i}}_C" type="radio" name="question_listen_{{$i}}[]" value="C" {{ $listen['question_listen_'.$i][1] == "C" ? "checked" : "" }}></label>
                                                                </div>
                                                                <div class="col-md-3">
                                                                    <label for="ques_{{$i}}_D">D. <input id="ques_{{$i}}_D" type="radio" name="question_listen_{{$i}}[]" value="D" {{ $listen['question_listen_'.$i][1] == "D" ? "checked" : "" }}></label>
                                                                </div>
                                                            
                                                            </div>
                                                        </div>
                                                        <div class="col-md-6">
                                                            <b>List answer</b>
                                                            <input class="form-control" type="text" name="question_listen_{{$i}}[]" placeholder="Answer A" value="{{ $listen['question_listen_'.$i][2] }}"><br>
                                                            <input class="form-control" type="text" name="question_listen_{{$i}}[]" placeholder="Answer B" value="{{ $listen['question_listen_'.$i][3] }}"><br>
                                                            <input class="form-control" type="text" name="question_listen_{{$i}}[]" placeholder="Answer C" value="{{ $listen['question_listen_'.$i][4] }}"><br>
                                                            <input class="form-control" type="text" name="question_listen_{{$i}}[]" placeholder="Answer D" value="{{ $listen['question_listen_'.$i][5] }}">
                                                        </div>
                                                    </div>
                                                    <hr>
                                                </div>
                                            @endfor
                                        </div>                                       
                                        {{-- <div id="part2" class="tab-pane fade "></div> --}}
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-lg-8 col-lg-offset-2 text-right"  style="margin-top: 20px;">
                                    <button class="btn btn-success"  type="submit">Cập nhật đề</button>
                                    <button class="btn btn-danger" type="reset">Huỷ bỏ</button>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!--/.row-->
</div>
<!--end main-->
@endsection

@section('script')
    
@endsection