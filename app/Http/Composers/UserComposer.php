<?php

namespace App\Http\Composers;

use Illuminate\View\View;
use App\Models\Users\User;


class UserComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    private $user;

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(User $user)
    {
        // Dependencies automatically resolved by service container...
        $this->user = $user;
       
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $adminComposer = User::where('level', '=', 1)->get();
        $view->with('adminComposer', $adminComposer);
        
    }
}