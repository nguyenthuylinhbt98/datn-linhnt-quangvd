@extends('user.master')

@section('css')
	<link rel="stylesheet" href="assets/css/test.css">
    <style>
        #part1 li{
            margin-right: 20px
        }
    </style>
@endsection


@section('content')
    
<main class="">
    <!--? slider Area Start-->
    <section class="slider-area slider-area2">
        <div class="slider-active">
            <!-- Single Slider -->
            <div class="single-slider slider-height2">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-8 col-lg-11 col-md-12">
                            <div class="hero__caption hero__caption2">
                                <h1 data-animation="bounceIn" data-delay="0.2s">{{$test->name_test}}</h1>
                                <!-- breadcrumb Start-->
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                                        <li class="breadcrumb-item"><a href="/toeic">Toeic</a></li>
                                        <li class="breadcrumb-item"><a href="/#">Test</a></li>
                                    </ol>
                                </nav>
                                <!-- breadcrumb End -->
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <!--? Blog Area Start -->
    <section class=" section-padding test">
        <form action="/toeic/test/{{$test->id}}" method="POST" id="testToeic">
            @csrf
            <div class="container">
                <div class="row">
                    <div class="col-lg-8 posts-list">
                        <ul class="nav nav-pills nav-justified" id="pills-tab" role="tablist">
                            <li class="nav-item">
                              <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#part1" role="tab" aria-controls="pills-home" aria-selected="true">Part I</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#part2" role="tab" aria-controls="pills-profile" aria-selected="false">Part II</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#part3" role="tab" aria-controls="pills-contact" aria-selected="false">Part III</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#part4" role="tab" aria-controls="pills-contact" aria-selected="false">Part IV</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#part5" role="tab" aria-controls="pills-contact" aria-selected="false">Part V</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#part6" role="tab" aria-controls="pills-contact" aria-selected="false">Part VI</a>
                            </li>
                            <li class="nav-item">
                              <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#part7" role="tab" aria-controls="pills-contact" aria-selected="false">Part VII</a>
                            </li>
                        </ul>
                          <hr>
                        <div class="tab-content" id="pills-tabContent">
    
                            {{-- Part I --}}
                            <div class="tab-pane fade show active" id="part1" role="tabpanel" aria-labelledby="pills-home-tab">
                                <div class="panel-heading">
                                    <h3 class="panel-title">
                                        <i class="fa fa-question"></i>
                                        TOEIC® Listening part 1: Photographs
                                    </h3> 
                                    @if(isset($part1->void_part1))
                                        <audio controls  src="../{{$part1->void_part1}}"   style="margin-top: 10px; margin-left: 10px">
                                            Sorry, your browser doesn't support html5!
                                        </audio>
                                    @endif
                                </div>
                                <div class="panel-body">
                                    <div id="">
                                        <span id="">
                                            <p> <b>Directions:</b> For each question in this part, you will hear four statements about a picture in your test book.
                                                    When you hear the statements, you must select the one statement that best describes what you see in the picture. 
                                                    Then find the number of the question on your answer sheet and mark your answer.
                                                    The statements will not be printed in your test book and will be spoken only one time. Look at the example below.</p>
                                            <div class="row">
                                                <div class="col-lg-4">
                                                    <img src="../{{$part1->example_img}}" alt="">
                                                </div>
                                                <div class="col-lg-8">
                                                    <span>{!!$part1->example_test1!!}</span>
                                                </div>
                                            </div>
                                        </span>
                                        <hr>
                                    </div>
                                    <div>
                                        @for($i=1; $i<=$part1->number_ques; $i++)
                                            <div class="row">
                                                <span>{{$i}}. <br></span>
                                                <div class="col-lg-5 col-md-5">
                                                    <img src="../{{ $part1->{'img_question_'.$i} }}" >
                                                </div>
                                                <div class="col-lg-7 col-md-7">
                                                    <div class="choices" style="float: left;">
                                                        <ul class="list-question d-flex">
                                                            <li>
                                                                <span class="my-radio">
                                                                    <input name="ans-ls-{{$i}}" type="radio" id="choice-{{$i}}-A" value="A">
                                                                    <label for="choice-{{$i}}-A" onclick="activeAns({{$i}})">A</label>
                                                                </span>
                                                            </li>
                                                            <li>
                                                                <span class="my-radio">
                                                                    <input name="ans-ls-{{$i}}" type="radio" id="choice-{{$i}}-B" value="B">
                                                                    <label for="choice-{{$i}}-B" onclick="activeAns({{$i}})">B</label>
                                                                </span>
                                                            </li>
                                                            <li>
                                                                <span class="my-radio">
                                                                    <input name="ans-ls-{{$i}}" type="radio" id="choice-{{$i}}-C" value="C">
                                                                    <label for="choice-{{$i}}-C" onclick="activeAns({{$i}})">C</label>
                                                                </span>
                                                            </li>
                                                            <li>
                                                                <span class="my-radio">
                                                                    <input name="ans-ls-{{$i}}" type="radio" id="choice-{{$i}}-D" value="D">
                                                                    <label for="choice-{{$i}}-D" onclick="activeAns({{$i}})">D</label>
                                                                </span>
                                                            </li>
                                                        </ul>
                                                    </div>
                                                </div>
                                            </div>
                                        @endfor    
                                    </div>
                                </div>
                            </div>
    
                            {{-- Part II --}}
                            <div class="tab-pane fade" id="part2" role="tabpanel" aria-labelledby="pills-profile-tab">
                                <div class="panel-heading">
                                    <h3 class="panel-title">
                                        <i class="fa fa-question"></i>
                                        TOEIC® Listening part 2: Question & Response
                                    </h3> 
                                    @if(isset($part2->void_part2))
                                        <audio controls  src="../{{$part2->void_part2}}"   style="margin-top: 10px; margin-left: 10px">
                                            Sorry, your browser doesn't support html5!
                                        </audio>
                                    @endif
                                </div>
                                <div class="panel-body">
                                    <div id="">
                                        <span id="">
                                            <p> <b>Directions:</b> Listen to these questions and statements. After each question or statement,
                                                you will hear three responses. Select the most appropriate response. 
                                                Mark your answer by clicking (A), (B), or (C). You will hear each question or statement, and the responses, only once.</p>
                                            <p><b>Example:</b></p>
                                            <p>{!!$part2->example_test2!!}</p>
                                        </span>
                                        <hr>
                                    </div>
                                    <div>
                                        @for($i=0; $i <= $part2->end - $part2->begin; $i++)
                                            <br><b>{{$i + $part2->begin}}.</b>
                                            <div>
                                                <div class="choices">
                                                    <span>
                                                        <span class="my-radio" onclick="activeAns({{$i + $part2->begin}})">
                                                            <input name="ans-ls-{{$i + $part2->begin}}" type="radio" id="choice-{{$i + $part2->begin}}-A" value="A">
                                                            <label for="choice-{{$i + $part2->begin}}-A">A</label>
                                                        </span>
                                                    </span>
                                                    <span>
                                                        <span class="my-radio"  onclick="activeAns({{$i + $part2->begin}})">
                                                            <input name="ans-ls-{{$i + $part2->begin}}" type="radio" id="choice-{{$i + $part2->begin}}-B" value="B">
                                                            <label for="choice-{{$i + $part2->begin}}-B">B</label>
                                                        </span>
                                                    </span>
                                                    <span>
                                                        <span class="my-radio"  onclick="activeAns({{$i + $part2->begin}})">
                                                            <input name="ans-ls-{{$i + $part2->begin}}" type="radio" id="choice-{{$i + $part2->begin}}-C" value="C">
                                                            <label for="choice-{{$i + $part2->begin}}-C">C</label>
                                                        </span>
                                                    </span>
                                                    <span>
                                                        <span class="my-radio"  onclick="activeAns({{$i + $part2->begin}})">
                                                            <input name="ans-ls-{{$i + $part2->begin}}" type="radio" id="choice-{{$i + $part2->begin}}-D" value="D">
                                                            <label for="choice-{{$i + $part2->begin}}-D">D</label>
                                                        </span>
                                                    </span>
                                                </div>
                                            </div>
                                        @endfor    
                                    </div>
                                </div>
                            </div>

                            {{-- Part III --}}
                            <div class="tab-pane fade" id="part3" role="tabpanel" aria-labelledby="pills-contact-tab">
                                <div class="panel-heading">
                                    <h3 class="panel-title">
                                        <i class="fa fa-question"></i>
                                        TOEIC® Listening part 3: Conversations
                                    </h3> 
                                    @if(isset($part3->void_part3))
                                        <audio controls  src="../{{$part3->void_part3}}"   style="margin-top: 10px; margin-left: 10px">
                                            Sorry, your browser doesn't support html5!
                                        </audio>
                                    @endif
                                </div>
                                <div class="panel-body">
                                    <div id="">
                                        <span id="">
                                            <p>In this part, you will listen to ten short conversations and 30 multiple choice questions.</p>
                                            <p> <b>Directions:</b> You will hear some conversations between two people. You will be asked to answer three 
                                                questions about what the speakers say in each conversation. Select the best response to each question and 
                                                mark the letter (A), (B), (C), or (D) on your answer sheet. The conversations will not be printed in your 
                                                test book and will be spoken only one time.</p>
                                        </span>
                                        <hr>
                                    </div>
                                    <div>
                                        @for($i=0; $i<= $part3->end-$part3->begin; $i++)
                                            <br><b>{{$i+$part3->begin}}.</b>{{$part3->{'question_'.$i}[0]}}
                                            <div>
                                                <div class="choices">
                                                    <p>
                                                        <span class="my-radio" onclick="activeAns({{$i+$part3->begin}})">
                                                            <input name="ans-ls-{{$i+$part3->begin}}" type="radio" id="choice-{{$i+$part3->begin}}-A" value="A">
                                                            <label for="choice-{{$i+$part3->begin}}-A">A.{{$part3->{'question_'.$i}[1]}}</label>
                                                        </span>
                                                    </p>
                                                    <p>
                                                        <span class="my-radio"  onclick="activeAns({{$i+$part3->begin}})">
                                                            <input name="ans-ls-{{$i+$part3->begin}}" type="radio" id="choice-{{$i+$part3->begin}}-B" value="B">
                                                            <label for="choice-{{$i+$part3->begin}}-B">B.{{$part3->{'question_'.$i}[2]}}</label>
                                                        </span>
                                                    </p>
                                                    <p>
                                                        <span class="my-radio"  onclick="activeAns({{$i+$part3->begin}})">
                                                            <input name="ans-ls-{{$i+$part3->begin}}" type="radio" id="choice-{{$i+$part3->begin}}-C" value="C">
                                                            <label for="choice-{{$i+$part3->begin}}-C">C.{{$part3->{'question_'.$i}[3]}}</label>
                                                        </span>
                                                    </p>
                                                    <p>
                                                        <span class="my-radio"  onclick="activeAns({{$i+$part3->begin}})">
                                                            <input name="ans-ls-{{$i+$part3->begin}}" type="radio" id="choice-{{$i+$part3->begin}}-D" value="D">
                                                            <label for="choice-{{$i+$part3->begin}}-D">D.{{$part3->{'question_'.$i}[4]}}</label>
                                                        </span>
                                                    </p>
                                                </div>
                                            </div>
                                        @endfor    
                                    </div>
                                </div>
                            </div>

                            {{-- Part Iv --}}
                            <div class="tab-pane fade" id="part4" role="tabpanel" aria-labelledby="pills-contact-tab">
                                <div class="panel-heading">
                                    <h3 class="panel-title">
                                        <i class="fa fa-question"></i>
                                        TOEIC® Listening part 4: Talks
                                    </h3> 
                                    @if(isset($part4->void_part4))
                                        <audio controls  src="../{{$part4->void_part4}}"   style="margin-top: 10px; margin-left: 10px">
                                            Sorry, your browser doesn't support html5!
                                        </audio>
                                    @endif
                                </div>
                                <div class="panel-body">
                                    <div id="">
                                        <span id="">
                                            <p>In this part, you will listen to ten short conversations and 30 multiple choice questions.</p>
                                            <p> <b>Directions:</b> You will hear some talks given by a single speaker. You will be asked to answer three questions
                                                 about what the speaker says in each talk. Select the best response to each question and mark the letter (A), (B), (C), or (D) 
                                                 on your answer sheet. The talks will not be printed in your test book and will be spoken only one time.</p>
                                        </span>
                                        <hr>
                                    </div>
                                    <div>
                                        @for($i=0; $i <= 100-$part4->begin; $i++)
                                            <br><b>{{$i+$part4->begin}}.</b>{{$part4->{'question_'.$i}[0]}}
                                            <div>
                                                <div class="choices">
                                                    <p>
                                                        <span class="my-radio" onclick="activeAns({{$i+$part4->begin}})">
                                                            <input name="ans-ls-{{$i+$part4->begin}}" type="radio" id="choice-{{$i+$part4->begin}}-A" value="A">
                                                            <label for="choice-{{$i+$part4->begin}}-A">A.{{$part4->{'question_'.$i}[1]}}</label>
                                                        </span>
                                                    </p>
                                                    <p>
                                                        <span class="my-radio"  onclick="activeAns({{$i+$part4->begin}})">
                                                            <input name="ans-ls-{{$i+$part4->begin}}" type="radio" id="choice-{{$i+$part4->begin}}-B" value="B">
                                                            <label for="choice-{{$i+$part4->begin}}-B">B.{{$part4->{'question_'.$i}[2]}}</label>
                                                        </span>
                                                    </p>
                                                    <p>
                                                        <span class="my-radio"  onclick="activeAns({{$i+$part4->begin}})">
                                                            <input name="ans-ls-{{$i+$part4->begin}}" type="radio" id="choice-{{$i+$part4->begin}}-C" value="C">
                                                            <label for="choice-{{$i+$part4->begin}}-C">C.{{$part4->{'question_'.$i}[3]}}</label>
                                                        </span>
                                                    </p>
                                                    <p>
                                                        <span class="my-radio"  onclick="activeAns({{$i+$part4->begin}})">
                                                            <input name="ans-ls-{{$i+$part4->begin}}" type="radio" id="choice-{{$i+$part4->begin}}-D" value="D">
                                                            <label for="choice-{{$i+$part4->begin}}-D">D.{{$part4->{'question_'.$i}[4]}}</label>
                                                        </span>
                                                    </p>
                                                </div>
                                            </div>
                                        @endfor    
                                    </div>
                                </div>
                            </div>
                            {{-- Part V --}}
                            <div class="tab-pane fade" id="part5" role="tabpanel" aria-labelledby="pills-contact-tab">
                                <div class="panel-heading">
                                    <h3 class="panel-title">
                                        <i class="fa fa-question"></i>
                                        TOEIC® Reading part 5 : Incomplete sentences
                                    </h3> 
                                </div>
                                <div class="panel-body">
                                    <div id="">
                                        <span id="">
                                            <p> <b>Directions:</b> A word or phrase is missing in each of the sentences below. Four answer choices are given below each sentence. 
                                                Select the best answer to complete the sentence. Then mark the letter (A), (B), (C), or (D) on your answer sheet.</p>
                                        </span>
                                        <hr>
                                    </div>
                                    <div>
                                        @for($i=0; $i<=$part5->end-101; $i++)
                                            <br><b>{{$i+101}}.</b>{{$part5->{'question_'.$i}[0]}}
                                            <div>
                                                <div class="choices row">
                                                    <div class="col-lg-6">
                                                        <p>
                                                            <span class="my-radio" onclick="activeAns({{$i+101}})">
                                                                <input name="ans-re-{{$i+101}}" type="radio" id="choice-{{$i+101}}-A" value="A">
                                                                <label for="choice-{{$i+101}}-A">A.{{$part5->{'question_'.$i}[1]}}</label>
                                                            </span>
                                                        </p>
                                                        <p>
                                                            <span class="my-radio"  onclick="activeAns({{$i+101}})">
                                                                <input name="ans-re-{{$i+101}}" type="radio" id="choice-{{$i+101}}-B" value="B">
                                                                <label for="choice-{{$i+101}}-B">B.{{$part5->{'question_'.$i}[2]}}</label>
                                                            </span>
                                                        </p>
                                                    </div>
                                                    <div class="col-lg-6">
                                                        <p>
                                                            <span class="my-radio"  onclick="activeAns({{$i+101}})">
                                                                <input name="ans-re-{{$i+101}}" type="radio" id="choice-{{$i+101}}-C" value="C">
                                                                <label for="choice-{{$i+101}}-C">C.{{$part5->{'question_'.$i}[3]}}</label>
                                                            </span>
                                                        </p>
                                                        <p>
                                                            <span class="my-radio"  onclick="activeAns({{$i+101}})">
                                                                <input name="ans-re-{{$i+101}}" type="radio" id="choice-{{$i+101}}-D" value="D">
                                                                <label for="choice-{{$i+101}}-D">D.{{$part5->{'question_'.$i}[4]}}</label>
                                                            </span>
                                                        </p>
                                                    </div>                                                    
                                                </div>
                                            </div>
                                        @endfor    
                                    </div>
                                </div>
                            </div>

                            {{-- Part VI --}}
                            <div class="tab-pane fade" id="part6" role="tabpanel" aria-labelledby="pills-contact-tab">
                                <div class="panel-heading">
                                    <h3 class="panel-title">
                                        <i class="fa fa-question"></i>
                                        TOEIC® Reading part 6 : Incomplete sentences
                                    </h3> 
                                </div>
                                <div class="panel-body">
                                    <div id="">
                                        <span id="">
                                            <p> <b>Directions:</b> Read the texts that follow. A word or phrase is missing in some of the sentences. Four answer choices are given 
                                                below each of the sentences. Select the best answer to complete the text. Then mark the letter (A), (B), (C) or (D) on the Answer Sheet. </p>
                                        </span>
                                        <hr>
                                    </div>
                                    <div>
                                        @php $ques = $part6->begin @endphp
                                            @php $a=0 @endphp
                                            @for($i=0; $i<$part6->num_para; $i++)
                                                <b>Paragraph {{$i+1}}</b>
                                                <p>{!! $part6->{'paragraph_'.$i}[0] !!}</p>
                                                <div class="question_paragraph_{{$i}}">
                                                    @for($j=0; $j<$part6->{'paragraph_'.$i}[1]; $j++)
                                                        <b> {{$ques + $j}}.</b><br>
                                                        <div>
                                                            <div class="choices">
                                                                <p>
                                                                    <span class="my-radio" onclick="activeAns({{$ques + $j}})">
                                                                        <input name="ans-re-{{$ques + $j}}" type="radio" id="choice-{{$ques + $j}}-A" value="A">
                                                                        <label for="choice-{{$ques + $j}}-A">A.{{$part6->{"question_".$a}[0]}}</label>
                                                                    </span>
                                                                </p>
                                                                <p>
                                                                    <span class="my-radio"  onclick="activeAns({{$ques + $j}})">
                                                                        <input name="ans-re-{{$ques + $j}}" type="radio" id="choice-{{$ques + $j}}-B" value="B">
                                                                        <label for="choice-{{$ques + $j}}-B">B.{{$part6->{"question_".$a}[1]}}</label>
                                                                    </span>
                                                                </p>
                                                                <p>
                                                                    <span class="my-radio"  onclick="activeAns({{$ques + $j}})">
                                                                        <input name="ans-re-{{$ques + $j}}" type="radio" id="choice-{{$ques + $j}}-C" value="C">
                                                                        <label for="choice-{{$ques + $j}}-C">C.{{$part6->{"question_".$a}[2]}}</label>
                                                                    </span>
                                                                </p>
                                                                <p>
                                                                    <span class="my-radio"  onclick="activeAns({{$ques + $j}})">
                                                                        <input name="ans-re-{{$ques + $j}}" type="radio" id="choice-{{$ques + $j}}-D" value="D">
                                                                        <label for="choice-{{$ques + $j}}-D">D.{{$part6->{"question_".$a}[3]}}</label>
                                                                    </span>
                                                                </p>
                                                            </div>
                                                        </div> 

                                                        @php $a++ @endphp
                                                    @endfor
                                                </div>
                                                @php $ques+= $part6->{'paragraph_'.$i}[1]  @endphp
                                                <hr>
                                            @endfor
                                    </div>
                                </div>
                            </div>

                            {{-- Part VII --}}
                            <div class="tab-pane fade" id="part7" role="tabpanel" aria-labelledby="pills-contact-tab">
                                <div class="panel-heading">
                                    <h3 class="panel-title">
                                        <i class="fa fa-question"></i>
                                        TOEIC® Reading part 7 : Comprehension
                                    </h3> 
                                </div>
                                <div class="panel-body">
                                    <div id="">
                                        <span id="">
                                            <p> <b>Directions:</b> Read the texts. You will notice that each text is followed by several questions. For each question, 
                                                decide which of the four answer choices: (A), (B), (C), or (D), best answers the question. Then mark your answer on the Answer Sheet. </p>
                                        </span>
                                        <hr>
                                    </div>
                                    <div>
                                        @php $ques = $part7->begin @endphp
                                        @php $a=0 @endphp
                                        @for($i=0; $i<$part7->num_para; $i++)
                                            <b>Paragraph {{$i+1}}</b>
                                            <p>{!! $part7->{'paragraph_'.$i}[0] !!}</p>
                                            <div class="question_paragraph_{{$i}}">
                                                @for($j=0; $j<$part7->{'paragraph_'.$i}[1]; $j++)
                                                    <b>{{$ques + $j}}.</b> {{$part7->{"question_".$a}[0]}}<br>
                                                    <div>

                                                        <div>
                                                            <div class="choices">
                                                                <p>
                                                                    <span class="my-radio" onclick="activeAns({{$ques + $j}})">
                                                                        <input name="ans-re-{{$ques + $j}}" type="radio" id="choice-{{$ques + $j}}-A" value="A">
                                                                        <label for="choice-{{$ques + $j}}-A">A. {{$part7->{"question_".$a}[1]}}</label>
                                                                    </span>
                                                                </p>
                                                                <p>
                                                                    <span class="my-radio"  onclick="activeAns({{$ques + $j}})">
                                                                        <input name="ans-re-{{$ques + $j}}" type="radio" id="choice-{{$ques + $j}}-B" value="B">
                                                                        <label for="choice-{{$ques + $j}}-B">B. {{$part7->{"question_".$a}[2]}}</label>
                                                                    </span>
                                                                </p>
                                                                <p>
                                                                    <span class="my-radio"  onclick="activeAns({{$ques + $j}})">
                                                                        <input name="ans-re-{{$ques + $j}}" type="radio" id="choice-{{$ques + $j}}-C" value="C">
                                                                        <label for="choice-{{$ques + $j}}-C">C. {{$part7->{"question_".$a}[3]}}</label>
                                                                    </span>
                                                                </p>
                                                                <p>
                                                                    <span class="my-radio"  onclick="activeAns({{$ques + $j}})">
                                                                        <input name="ans-re-{{$ques + $j}}" type="radio" id="choice-{{$ques + $j}}-D" value="D">
                                                                        <label for="choice-{{$ques + $j}}-D">D. {{$part7->{"question_".$a}[4]}}</label>
                                                                    </span>
                                                                </p>
                                                            </div>
                                                        </div> 
                                                        
                                                        
                                                    </div> <hr>

                                                   @php $a++ @endphp
                                                @endfor
                                            </div>
                                            @php $ques+= (int)$part7->{'paragraph_'.$i}[1]  @endphp
                                        @endfor
                                    </div>
                                </div>

                            </div>
                        </div>

                       
                    </div>
                    <div class="col-lg-4 ans-map ">
                        <div class="row">
                            <div class="time-ans">
                                <i class="far fa-clock"></i>
                                0<span id="h">0</span> :
                                <span id="m">00</span> :
                                <span id="s">00</span>
                            </div>
                            @for($i=1; $i<201; $i++)
                                <div class="col-lg-2 choice-ques " >
                                    {{-- <div class="ques-{{$i}} ans-ques"><a href="/#">{{$i}}</a></div> --}}
                                    <div class="ques-{{$i}} ans-ques">{{$i}}</div>
                                </div>
                            @endfor
                            
                        </div>
                        <div class="sub">
                            <input type="submit" class="genric-btn primary-border circle" value="Submit"/>
                        </div>
                    </div>
                </div>
               
            </div>
        </form>
        
    </section>
    <!-- Blog Area End -->
</main>

@endsection


@section('script')
<script>
    var h = 02; // Giờ
    var m = 00; // Phút
    var s = 00; // Giây
    var timeout = null;

    function start()
    {
        if (s === -1){
            m -= 1;
            s = 59;
        }

        if (m === -1){
            h -= 1;
            m = 59;
        }

        if (h == -1){
            clearTimeout(timeout);
            document.getElementById("testToeic").submit();
            return false;
        }

        document.getElementById('h').innerText = h.toString();
        document.getElementById('m').innerText = m.toString();
        document.getElementById('s').innerText = s.toString();

        timeout = setTimeout(function(){
            s--;
            start();
        }, 1000);
    }
    start();

    function activeAns(id){
        $(`.ques-${id}`).addClass('active')
    }
    
</script>
@endsection
