@extends('user.master')


@section('content')
<main>
	<!--? slider Area Start-->
	<section class="slider-area ">
		<div class="slider-active">
			<!-- Single Slider -->
			<div class="single-slider slider-height d-flex align-items-center">
				<div class="container">
					<div class="row">
						<div class="col-xl-6 col-lg-7 col-md-12">
							<div class="hero__caption">
								<h1 data-animation="fadeInLeft" data-delay="0.2s">Online test<br> English </h1>
								<p data-animation="fadeInLeft" data-delay="0.4s">Build skills with courses, certificates, and degrees online from world-class universities and companies</p>
								<a href="/singup" class="btn hero-btn" data-animation="fadeInLeft" data-delay="0.7s">Join for Free</a>
							</div>
						</div>
					</div>
				</div>          
			</div>
		</div>
	</section>
	<!-- ? services-area -->
	<div class="services-area">
		<div class="container">
			<div class="row justify-content-sm-center">
				<div class="col-lg-4 col-md-6 col-sm-8">
					<div class="single-services mb-30">
						<div class="features-icon">
							<img src="assets/img/icon/icon1.svg" alt="">
						</div>
						<div class="features-caption">
							<h3>60+ UX courses</h3>
							<p>The automated process all your website tasks.</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-8">
					<div class="single-services mb-30">
						<div class="features-icon">
							<img src="assets/img/icon/icon2.svg" alt="">
						</div>
						<div class="features-caption">
							<h3>Expert instructors</h3>
							<p>The automated process all your website tasks.</p>
						</div>
					</div>
				</div>
				<div class="col-lg-4 col-md-6 col-sm-8">
					<div class="single-services mb-30">
						<div class="features-icon">
							<img src="assets/img/icon/icon3.svg" alt="">
						</div>
						<div class="features-caption">
							<h3>Life time access</h3>
							<p>The automated process all your website tasks.</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<!-- Courses area start -->
	<div class="courses-area section-padding40 fix">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-7 col-lg-8">
					<div class="section-tittle text-center mb-55">
						<h2>Our featured courses</h2>
					</div>
				</div>
			</div>
			<div class="courses-actives">
				{{-- Toeic --}}
				<div class="properties pb-20">
					<div class="properties__card">
						<div class="properties__img overlay1">
							<a href="/toeic/"><img src="../default/toeic/toeic0{{mt_rand(1, 3)}}.png" alt=""></a>
						</div>
						<div class="properties__caption">
							<h3><a href="#">Examp Toeic test</a></h3>
							<p>Toeic 2 ky nang</p>
							<div class="properties__footer d-flex justify-content-between align-items-center">
								<div class="restaurant-name">
									<div class="rating">
										<p>123456 de da duoc cap nhat</p>
									</div>
								</div>
								
							</div>
							<a href="/toeic/" class="border-btn border-btn2">Click to start</a>
						</div>
					</div>
				</div>

				{{-- Aptis --}}
				<div class="properties pb-20">
					<div class="properties__card">
						<div class="properties__img overlay1">
							<a href="/aptis/"><img src="../default/aptis/aptis0{{mt_rand(1, 3)}}.png" alt=""></a>
						</div>
						<div class="properties__caption">
							<h3><a href="#">Examp Aptis test</a></h3>
							<p>aptis 4 ky nang</p>
							<div class="properties__footer d-flex justify-content-between align-items-center">
								<div class="restaurant-name">
									<div class="rating">
										<p>123456 de da duoc cap nhat</p>
									</div>
								</div>
								
							</div>
							<a href="/aptis/" class="border-btn border-btn2">Click to start</a>
						</div>
					</div>
				</div>

				{{-- Ielts --}}
				<div class="properties pb-20">
					<div class="properties__card">
						<div class="properties__img overlay1">
							<a href="/"><img src="../default/ielts/ielts0{{mt_rand(1, 3)}}.png" alt=""></a>
						</div>
						<div class="properties__caption">
							<h3><a href="#">Examp Ielts test</a></h3>
							<p>gvghv 4 ky nang</p>
							<div class="properties__footer d-flex justify-content-between align-items-center">
								<div class="restaurant-name">
									<div class="rating">
										<p>123456 de da duoc cap nhat</p>
									</div>
								</div>
								
							</div>
							<a href="/" class="border-btn border-btn2" data-toggle="modal" data-target="#exampleModal">Click to start</a>
						</div>
					</div>
				</div>

				{{-- toefl --}}
				<div class="properties pb-20">
					<div class="properties__card">
						<div class="properties__img overlay1">
							<a href="/"><img src="../default/toefl/toefl0{{mt_rand(1, 3)}}.png" alt=""></a>
						</div>
						<div class="properties__caption">
							<h3><a href="#">Examp toefl test</a></h3>
							<p>gvghv 4 ky nang</p>
							<div class="properties__footer d-flex justify-content-between align-items-center">
								<div class="restaurant-name">
									<div class="rating">
										<p>123456 de da duoc cap nhat</p>
									</div>
								</div>
								
							</div>
							<a href="/" class="border-btn border-btn2" data-toggle="modal" data-target="#exampleModal">Click to start</a>
						</div>
					</div>
				</div>

				{{-- math --}}
				<div class="properties pb-20">
					<div class="properties__card">
						<div class="properties__img overlay1">
							<a href="/"><img src="../default/math/math0{{mt_rand(1, 3)}}.png" alt=""></a>
						</div>
						<div class="properties__caption">
							<h3><a href="#">Examp math test</a></h3>
							<p>gvghv test</p>
							<div class="properties__footer d-flex justify-content-between align-items-center">
								<div class="restaurant-name">
									<div class="rating">
										<p>123456 de da duoc cap nhat</p>
									</div>
								</div>
								
							</div>
							<a href="/" class="border-btn border-btn2" data-toggle="modal" data-target="#exampleModal">Click to start</a>
						</div>
					</div>
				</div>

				{{-- toefl --}}
				<div class="properties pb-20">
					<div class="properties__card">
						<div class="properties__img overlay1">
							<a href="/"><img src="../default/informatics/informatics0{{mt_rand(1, 3)}}.png" alt=""></a>
						</div>
						<div class="properties__caption">
							<h3><a href="#">Examp informatics test</a></h3>
							<p>gvghv 4 ky nang</p>
							<div class="properties__footer d-flex justify-content-between align-items-center">
								<div class="restaurant-name">
									<div class="rating">
										<p>123456 de da duoc cap nhat</p>
									</div>
								</div>
								
							</div>
							<a href="/" class="border-btn border-btn2" data-toggle="modal" data-target="#exampleModal">Click to start</a>
						</div>
					</div>
				</div>

			</div>
		</div>
	</div>
	
	<section class="team-area section-padding40 fix">
		<div class="container">
			<div class="row justify-content-center">
				<div class="col-xl-7 col-lg-8">
					<div class="section-tittle text-center mb-55">
						<h2>Our team</h2>
					</div>
				</div>
			</div>
			<div class="team-active">
				@foreach ($adminComposer as $admin)
					<div class="single-cat text-center">
						<div class="cat-icon">
							@if($admin->avatar)
								<img class="rounded-circle border border-primary"  src="../{{$admin->avatar}}" style="width: 200px; height: 200px" alt={{$admin->full }} >
							@else
								<img class="rounded-circle border border-primary"  src="../upload/user_avatar/no_avatar.png" style="width: 200px; height: 200px" alt={{$admin->full }} >
							@endif
						</div>
						<div class="cat-cap">
							<h5><a href="services.html">{{$admin->full}}</a></h5>
							<p>Admin / Developer</p>
						</div>
					</div>
				@endforeach
				
			</div>
		</div>
	</section>
	<!-- Services End -->
	<!--? About Area-2 Start -->
	<section class="about-area2 fix pb-padding">
		<div class="support-wrapper align-items-center">
			<div class="right-content2">
				<!-- img -->
				<div class="right-img">
					<img src="/assets/img/contact.jpg" alt="">
				</div>
			</div>
			<div class="left-content2">
				<!-- section tittle -->
				<div class="section-tittle section-tittle2 mb-20">
					<div class="front-text">
						<h2 class="">Contact us if you have any questions or want 
							to contribute to the development of the website.</h2>
						<p>
							<a style="color: black" href="https://www.facebook.com/linhnt1998/">Nguyễn Thùy Linh</a> - nguyenthuylinhbt98@gmail.com <br>
							<a style="color: black" href="https://www.facebook.com/quangvd98">Vũ Duy Quang</a> - quang.vd98@gmail.com
						</p>
						<hr/>
						<a href="/singup" class="btn">Join now for Free</a>
					</div>
				</div>
			</div>
		</div>
	</section>
	<!-- About Area End -->


	<!-- Modal -->
	<div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
		<div class="modal-header">
			<h5 class="modal-title" id="exampleModalLabel">Notification</h5>
			<button type="button" class="close" data-dismiss="modal" aria-label="Close">
			<span aria-hidden="true">&times;</span>
			</button>
		</div>
		<div class="modal-body">
			We are preparing and will bring this course to you as soon as possible!! <br>
			Love, see you soon...
		</div>
		<div class="modal-footer">
			<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
		</div>
		</div>
	</div>
	</div>
</main>

@endsection

