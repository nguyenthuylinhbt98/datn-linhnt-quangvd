<?php

namespace App\Repositories\Admin\Toeic;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Models\Toeic\Part5;

//use Your Model

/**
 * Class Part5Repository.
 */
class Part5Repository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Part5::class;
    }

    public function createPart5(array $data)
    {
        $part_5['name'] = $data['name'];
        $part_5['end'] = $data['end'];

        for ($i=0; $i < 40; $i++) {
            if(!empty($data['question_'.$i])){
                $part_5['question_'.$i]=implode('; ;',(array)$data['question_'.$i]);
            };
        }
        Part5::create($part_5);        
        return true;
    }

    // 
    // 
    // -----------------show edit----------
    // 
    // 

    public function editPart5(int $id)
    {
        $part_5 = self::getById($id);
        
        for ($i=0; $i < 40; $i++) {
            (array)$part_5['question_'.$i]=explode('; ;',$part_5->{'question_'.$i});
        }
        return $part_5;
    }
    // 
    // 
    // ------------update-------
    // 
    // 

    public function updatePart5(array $data, int $id)
    {
        $part5 = self::getById($id);
        $part_5['name'] = $data['name'];
        for ($i=0; $i <= 40; $i++) {
            if(!empty($data['question_'.$i])){
                $part_5['question_'.$i]=implode('; ;',(array)$data['question_'.$i]);
            };
        }
        $part5->update($part_5);        
        return true;
    }
}
