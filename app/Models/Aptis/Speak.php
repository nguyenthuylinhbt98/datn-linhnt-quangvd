<?php

namespace App\Models\Aptis;

use Illuminate\Database\Eloquent\Model;

class Speak extends Model
{
    protected $table = 'aptis_speak';

    protected $guarded = [
        'id'
    ];

    public function test() {
    	return $this -> hasMany('App\Models\Aptis\Test','speak_id','id');
    }
}