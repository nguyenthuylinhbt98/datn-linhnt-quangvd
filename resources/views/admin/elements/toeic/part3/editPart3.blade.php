@extends('admin.master')
@section('content')
<!--main-->
<div class="container main exam">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Edit part III: {{$part3->name}}</h1>
        </div>
    </div> 
    @if(session('thongbao'))
       <div class="alert alert-success">
            {{ session('thongbao') }}
        </div>

    @endif
    <!--/.row-->
    <form action="/admin/toeic/part_3/{{$part3->id}}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="_method" value="put">
        <input type="hidden" name="id" value="{{$part3->id}}">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><i class="fas fa-user"></i>Edit part III: {{$part3->name}}</div>
                    <div class="panel-body">
                        <div class="row justify-content-center" style="margin-bottom:40px">
                            <div class="col-md-8 col-lg-8 col-lg-offset-2">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input value="{{$part3->name}}" type="text" name="name" placeholder="Name test" class="form-control" required>
                                    {{ ShowErrors($errors, 'name')}}
                                </div>
                                <div class="container">
                                    <h2>Exam Toeic</h2>
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#part1">Part III</a></li>
                                        
                                    </ul>
                                    <div class="tab-content">

                                        {{-- Part3 --}}
                                        <div id="part1" class="tab-pane fade in active">
                                            <h3><b>PART III</b></h3>
                                            <p> <b>Directions:</b> You will hear some conversations between two people. You will be asked to answer three questions about what the speakers say in each conversation. Select the best response to each question and mark the letter (A), (B), (C), or (D) on your answer sheet. The conversations will not be printed in your test book and will be spoken only one time.</p>
                                            <br><b>Voice: </b><br>
                                            <audio src="../{{$part3->void_part3}}"  loop controls></audio>
                                            <input type="file" name="void_part3"><br>
                                            <label for="begin">
                                                Question begin: <input type="text" id="begin" name="begin" value={{$part3->begin}}>
                                            </label><br>
                                            <label for="end">
                                                Question end: <input type="text" id="end" name="end" value={{$part3->end}}>
                                            </label><br><hr>

                                            @for($i=0; $i<=$part3->end-$part3->begin; $i++)
                                                <b>Question {{$i+$part3->begin}}:</b>
                                                <br>
                                                <input class="form-control" type="text" value="{{$part3->{'question_'.$i}[0]}}" name="question_{{$i}}[]" placeholder="Question {{$i+$part3->begin}}"><br>
                                                <input class="form-control" type="text" value="{{$part3->{'question_'.$i}[1]}}" name="question_{{$i}}[]" placeholder="Answer A"><br>
                                                <input class="form-control" type="text" value="{{$part3->{'question_'.$i}[2]}}" name="question_{{$i}}[]" placeholder="Answer B"><br>
                                                <input class="form-control" type="text" value="{{$part3->{'question_'.$i}[3]}}" name="question_{{$i}}[]" placeholder="Answer C"><br>
                                                <input class="form-control" type="text" value="{{$part3->{'question_'.$i}[4]}}" name="question_{{$i}}[]" placeholder="Answer D">
                                                <b>Answer choice:</b>
                                                @if(isset($part3->{'question_'.$i}[5]))
                                                    <div class="row">
                                                        <div class="col-md-1">
                                                            <p style="float: left; margin-right: 5px;">A</p> <input @if($part3->{'question_'.$i}[5]=='A') checked @endif type="radio" name="question_{{$i}}[]" value="A">
                                                        </div>
                                                        <div class="col-md-1">
                                                            <p style="float: left; margin-right: 5px;">B</p> <input @if($part3->{'question_'.$i}[5]=='B') checked @endif type="radio" name="question_{{$i}}[]" value="B">
                                                        </div>
                                                        <div class="col-md-1">
                                                            <p style="float: left; margin-right: 5px;">C</p> <input @if($part3->{'question_'.$i}[5]=='C') checked @endif type="radio" name="question_{{$i}}[]" value="C">
                                                        </div>
                                                         <div class="col-md-1">
                                                            <p style="float: left; margin-right: 5px;">D</p> <input @if($part3->{'question_'.$i}[5]=='D') checked @endif type="radio" name="question_{{$i}}[]" value="D">
                                                        </div>
                                                    </div>
                                                @else
                                                    <div class="row">
                                                        <div class="col-md-1">
                                                            <p style="float: left; margin-right: 5px;">A</p> <input type="radio" name="question_{{$i}}[]" value="A">
                                                        </div>
                                                        <div class="col-md-1">
                                                            <p style="float: left; margin-right: 5px;">B</p> <input type="radio" name="question_{{$i}}[]" value="B">
                                                        </div>
                                                        <div class="col-md-1">
                                                            <p style="float: left; margin-right: 5px;">C</p> <input type="radio" name="question_{{$i}}[]" value="C">
                                                        </div>
                                                         <div class="col-md-1">
                                                            <p style="float: left; margin-right: 5px;">D</p> <input type="radio" name="question_{{$i}}[]" value="D">
                                                        </div>
                                                    </div>
                                                @endif 

                                                <hr>
                                            @endfor
                                        </div>
                                        
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-lg-8 col-lg-offset-2 text-right"  style="margin-top: 20px;">
                                    <button class="btn btn-success"  type="submit">Edit</button>
                                    <button class="btn btn-danger" type="reset">Reset</button>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!--/.row-->
</div>
<!--end main-->
@endsection

@section('script')
        
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
    <script>
        $('textarea').ckeditor();
    </script>
@endsection