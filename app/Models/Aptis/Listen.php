<?php

namespace App\Models\Aptis;

use Illuminate\Database\Eloquent\Model;

class Listen extends Model
{
    protected $table = 'aptis_listen';

    protected $guarded = [
        'id'
    ];

    public function test() {
    	return $this -> hasMany('App\Models\Aptis\Test','listen_id','id');
    }
}
