<?php

use Illuminate\Database\Seeder;

class ToeicUserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users_and_test')->delete();
        DB::table('users_and_test')->insert([             
            [
                "id"=> 186,
                "user_id"=> 77,
                "test_id"=> 6,
                "score"=> "25; ;100; ;45; ;205"
              ],
              [
                "id"=> 187,
                "user_id"=> 32,
                "test_id"=> 6,
                "score"=> "34; ;150; ;43; ;195"
              ],
              [
                "id"=> 188,
                "user_id"=> 248,
                "test_id"=> 9,
                "score"=> "50; ;245; ;60; ;295"
              ],
              [
                "id"=> 189,
                "user_id"=> 322,
                "test_id"=> 2,
                "score"=> "73; ;375; ;50; ;235"
              ],
              [
                "id"=> 190,
                "user_id"=> 334,
                "test_id"=> 5,
                "score"=> "30; ;125; ;55; ;270"
              ],
              [
                "id"=> 191,
                "user_id"=> 397,
                "test_id"=> 5,
                "score"=> "48; ;235; ;63; ;310"
              ],
              [
                "id"=> 192,
                "user_id"=> 80,
                "test_id"=> 6,
                "score"=> "56; ;280; ;60; ;295"
              ],
              
            
        ]);
    }
}
