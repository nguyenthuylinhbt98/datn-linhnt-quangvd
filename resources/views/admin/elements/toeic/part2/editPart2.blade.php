@extends('admin.master')
@section('content')
<!--main-->
<div class="container main exam">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Edit part II: {{$part2->name}}</h1>
        </div>
    </div> 
   @if(session('thongbao'))
       <div class="alert alert-success">
            {{ session('thongbao') }}
        </div>

    @endif
    <!--/.row-->
    <form action="/admin/toeic/part_2/{{$part2->id}}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="_method" value="put">
        <input type="hidden" name="id" value="{{$part2->id}}">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><i class="fas fa-user"></i>Edit part II: {{$part2->name}}</div>
                    <div class="panel-body">
                        <div class="row justify-content-center" style="margin-bottom:40px">
                            <div class="col-md-8 col-lg-8 col-lg-offset-2">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input value="{{$part2->name}}" type="text" name="name" placeholder="Name test" class="form-control" required>
                                    {{ ShowErrors($errors, 'name')}}
                                </div>
                                <div class="container">
                                    <h2>Exam Toeic</h2>
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#part1">Part II</a></li>
                                        
                                    </ul>
                                    <div class="tab-content">
                                        
                                        {{-- Part2 --}}
                                        <div id="part1" class="tab-pane fade in active">
                                            <h3><b>PART II</b></h3>
                                            <p> <b>Directions:</b> Listen to these questions and statements. After each question or statement, you will hear three responses. Select the most appropriate response. Mark your answer by clicking (A), (B), or (C). You will hear each question or statement, and the responses, only once.</p>
                                            <p><b>Example:</b></p>
                                            <textarea class="form-control" id="summary-ckeditor" type="text" name="example_test2"  placeholder="Guide to choose the answer" style="height: 200px;">{!!$part2->example_test2!!}</textarea>
                                            <br><b>Voice: </b><br>
                                            <audio src="../{{$part2->void_part2}}" autobuffer autoloop loop controls></audio>
                                            <input type="file" name="void_part2"><br>
                                            <label for="begin">
                                                Question begin: <input type="text" id="begin" name="begin" value={{$part2->begin}}>
                                            </label><br>
                                            <label for="end">
                                                Question end: <input type="text" id="end" name="end" value={{$part2->end}}>
                                            </label><br><hr>
                                            {{-- @dd($part2) --}}
                                            @for($i=0; $i <= $part2->end - $part2->begin; $i++)
                                                <b>Question {{$i + $part2->begin}}:</b>
                                                <div class="row">
                                                    <div class="col-md-1">
                                                        <p style="float: left; margin-right: 5px;">A</p> <input @if($part2['answer_question_'.$i]=='A') checked @endif type="radio" name="answer_question_{{$i}}" value="A">
                                                    </div>
                                                    <div class="col-md-1">
                                                        <p style="float: left; margin-right: 5px;">B</p> <input @if($part2['answer_question_'.$i]=='B') checked @endif type="radio" name="answer_question_{{$i}}" value="B">
                                                    </div>
                                                    <div class="col-md-1">
                                                        <p style="float: left; margin-right: 5px;">C</p> <input @if($part2['answer_question_'.$i]=='C') checked @endif type="radio" name="answer_question_{{$i}}" value="C">
                                                    </div>
                                                    
                                                </div>
                                                <hr>
                                            @endfor
                                        </div>                                       
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-lg-8 col-lg-offset-2 text-right"  style="margin-top: 20px;">
                                    <button class="btn btn-success"  type="submit">Edit</button>
                                    <button class="btn btn-danger" type="reset">Reset</button>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!--/.row-->
</div>
<!--end main-->
@endsection

@section('script')
<script src="./../ckeditor/ckeditor.js"></script>
<script>
    CKEDITOR.replace( 'summary-ckeditor');
</script>
@endsection