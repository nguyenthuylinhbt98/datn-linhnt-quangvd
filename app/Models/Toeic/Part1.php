<?php

namespace App\Models\Toeic;

use Illuminate\Database\Eloquent\Model;

class Part1 extends Model
{
    protected $table = 'toeic_part1';

    protected $guarded = [
        'id'
    ];
}
