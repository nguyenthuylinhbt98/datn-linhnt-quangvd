<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateToeicPart6Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('toeic_part6', function (Blueprint $table) {
            $table->id();
            $table->string('name', 191);
            $table->integer('begin');
            $table->integer('num_para');
            $table->integer('end');

            for($i=0; $i<5; $i++){
                $table->longText('paragraph_'.$i)->nullable();
            };

            for($i=0; $i<20; $i++){
                $table->longText('question_'.$i)->nullable();
            };

           
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('toeic_part6');
    }
}
