<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Auth;
use App\User;

class LoginAdminController extends Controller
{

    function getLogin(){

    	return view('admin.login');
    }

    function postLogin(Request $r){

    	$email = $r->email;
    	$password = $r->password;

    	if(Auth::attempt(['email'=>$email, 'password' => $password])){
            if($user->level == 1 || $user->level == 2) {
                return redirect('/admin');
            } else {
                return  redirect('/logout');
            }
    	}else{
    		return redirect()->back()->withErrors(['email'=>'Tài khoản hoặc mật khẩu không chính xác!!'])->withInput();
    	}	
    }

    function getLogout(){
        Auth::logout();
        return  redirect('/login');
    }
}
