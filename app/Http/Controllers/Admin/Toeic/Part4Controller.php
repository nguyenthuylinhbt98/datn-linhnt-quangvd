<?php

namespace App\Http\Controllers\Admin\Toeic;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Toeic\Part4;
use App\Http\Requests\Toeic\Part4\{AddPart4Request, EditPart4Request};
use App\Repositories\Admin\Toeic\Part4Repository;

class Part4Controller extends Controller
{
    private $part4Repository;

    public function __construct()
    {
        $this->part4Repository = new Part4Repository();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['part4'] = Part4::paginate(10);
        return view('admin.elements.toeic.part4.listPart4', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.elements.toeic.part4.addPart4');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddPart4Request $r)
    {
        $data = $r->all();
        $part_4 = $this->part4Repository->createPart4($data); 
        if($part_4){   
             return redirect('/admin/toeic/part_4')->with('thongbao', 'Success create new part IV test'); 
        }  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['part4'] = $this->part4Repository->editPart4($id); 
        return view('admin.elements.toeic.part4.showPart4', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['part4'] = $this->part4Repository->editPart4($id); 
        return view('admin.elements.toeic.part4.editPart4', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditPart4Request $r, $id)
    {
        $data = $r->all();
        $part_4 = $this->part4Repository->updatePart4($data, $id); 
        if($part_4){   
             return redirect('/admin/toeic/part_4')->with('thongbao', 'Success edit part IV'); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Part4::destroy($id);
        return redirect()->back()->with('thongbao', 'Đã xóa thành công!!!');
    }
}
