@extends('user.master')

@section('css')
	<link rel="stylesheet" href="assets/css/profile.css">
@endsection

@section('content')
<main>
    <!--? slider Area Start-->
    <section class="slider-area slider-area2">
        <div class="slider-active">
            <!-- Single Slider -->
            <div class="single-slider slider-height2">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-8 col-lg-11 col-md-12">
                            <div class="hero__caption hero__caption2">
                                <h1 data-animation="bounceIn" data-delay="0.2s">{{ Auth::user()->full }} Profile</h1>
                                <!-- breadcrumb Start-->
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                                        <li class="breadcrumb-item"><a href="/profile/{{ Auth::user()->id }}">Profile</a></li> 
                                    </ol>
                                </nav>
                                <!-- breadcrumb End -->
                            </div>
                        </div>
                    </div>
                </div>          
            </div>
        </div>
    </section>

    {{-- c------------ --}}
    <div class="container emp-profile">
        <form action="/profile/{{Auth::user()->id}}/edit" method="post" accept-charset="utf-8" enctype="multipart/form-data">
            @csrf
            <input type="hidden" name="id" value="{{Auth::user()->id}}">
            <div class="row">
                <div class="col-md-4">
                    <div class="profile-img">
                        <input id="for_img" type="file" name="avatar" class="form-control" onchange="changeImg(this)" style="display:none">
                        <div class="avatar_img">	
                            @if(isset(Auth::user()->avatar))				            	
                                <label for="for_img"> <img id="avatar" src="../{{ Auth::user()->avatar }}"></label> 
                            @else
                                <label for="for_img"> <img id="avatar" src="../upload/user_avatar/no_avatar.png"></label> 
                            @endif	
                            {{Auth::user()->email}}
					    </div>
                       
                       
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="row">

                        @if(session('thongbao'))
		                  <div class="alert alert-success">
		                      {{ session('thongbao') }}
		                  </div>
                        @endif
                        
                        <div class="col-md-12">
                             <b>User Name: </b><input class="single-input" type="" name="full" value="{{Auth::user()->full}}">
                            {{ ShowErrors($errors, 'full')}}
                        </div>

                        <div class="col-md-12" style="display: none">
                            <b>Email: </b>
                            <input  class="single-input" type="email" name="email" value="{{Auth::user()->email}}">
                        </div>

                        <div class="col-md-12">
                             <b>Address: </b><input class="single-input" type="" name="address" value="{{Auth::user()->address}}">

                        </div>

                        <div class="col-md-12">
                             <b>Password: </b><input class="single-input" type="password" name="password" value="" placeholder="">
                            {{ ShowErrors($errors, 'password')}}
                        </div>

                        <div class="col-md-12">
                             <b>Date of birth: </b><input class="single-input" type="date" name="date_of_birth" value="{{Auth::user()->date_of_birth}}">
                        </div>

                        <div class="col-md-12">
                             <b>Số điện thoại: </b><input class="single-input" type="" name="phone" value="{{Auth::user()->phone}}">
                            {{ ShowErrors($errors, 'phone')}}
                        </div>

                        <div class="col-md-12">
                             <b>Facebook: </b><input class="single-input" type="" name="facebook" value="{{Auth::user()->facebook}}" placeholder="Link Facebook">
                        </div>

                        <button class="genric-btn danger circle arrow" type="submit" style="margin: auto"> Edit Profile</button>
                        

                    </div>
                    
                </div>
                
            </div>
            
        </form>           
    </div>
	
</main>

@endsection

@section('script')
<script>
    function changeImg(input){
          //Nếu như tồn thuộc tính file, đồng nghĩa người dùng đã chọn file mới
          if(input.files && input.files[0]){
              var reader = new FileReader();
              //Sự kiện file đã được load vào website
              reader.onload = function(e){
                  //Thay đổi đường dẫn ảnh
                  $('#avatar').attr('src',e.target.result);
              }
              reader.readAsDataURL(input.files[0]);
          }
      }
      $(document).ready(function() {
          $('#avatar').click(function(){
              $('#for_img').click();
          });
      });
</script>
@endsection

