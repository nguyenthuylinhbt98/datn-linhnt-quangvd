@extends('user.master')

@section('css')
     <style>
        .blog_left_sidebar img{
             height: 310px;
             object-fit: cover
         }
     </style>
    
@endsection

@section('content')
<main>
    <!--? slider Area Start-->
    <section class="slider-area slider-area2">
        <div class="slider-active">
            <!-- Single Slider -->
            <div class="single-slider slider-height2">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-8 col-lg-11 col-md-12">
                            <div class="hero__caption hero__caption2">
                                <h1 data-animation="bounceIn" data-delay="0.2s">The latest information</h1>
                                <!-- breadcrumb Start-->
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                                        <li class="breadcrumb-item"><a>Blog</a></li> 
                                    </ol>
                                </nav>
                                <!-- breadcrumb End -->
                            </div>
                        </div>
                    </div>
                </div>          
            </div>
        </div>
    </section>
    <!--? Blog Area Start-->
    <section class="blog_area section-padding">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 mb-5 mb-lg-0">
                    <div class="blog_left_sidebar">
                        @foreach ($posts as $key=>$item)
                            <article class="blog_item" data-toggle="modal" data-target="#exampleModalLong{{ $key }}">
                                <div class="blog_details">
                                    <a class="d-inline-block">
                                        <h2 class="blog-head" style="color: #2d2d2d;">{{ $key+1 }}. {{ $item->title }}</h2>
                                    </a> </br>
                                    <small>{{ $item->updated_at }}</small>
                                </div>
                            </article>
                            <div class="modal fade" id="exampleModalLong{{ $key }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLongTitle" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" style="font-size: 20px" id="exampleModalLongTitle">{{ $item->title }}</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        {!! $item->content !!}
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" style="padding: 5px 10px; border-radius: 5px; border: 1px; background: #0072ff;"  data-dismiss="modal">Close</button>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <nav class="blog-pagination justify-content-center d-flex">
                            {{ $posts->links() }}
                        </nav>
                    </div>
                </div>
               
            </div>
        </div>
    </section>
    <!-- Blog Area End -->
</main>
@endsection