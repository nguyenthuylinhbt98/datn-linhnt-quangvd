@extends('user.master')

@section('css')
	<link rel="stylesheet" href="assets/css/test.css">
    <style>
        label{
            float: left;
        }
        select{
            max-width: 90%;
        }
    </style>
@endsection


@section('content')
    
<main class="aptis" id="aptis-grammar">
    <!--? slider Area Start-->
    <section class="slider-area slider-area2">
        <div class="slider-active">
            <!-- Single Slider -->
            <div class="single-slider slider-height2">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-8 col-lg-11 col-md-12">
                            <div class="hero__caption hero__caption2">
                                <h1 data-animation="bounceIn" data-delay="0.2s">{{$name}}</h1>
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                                        <li class="breadcrumb-item"><a href="/aptis">Aptis</a></li>
                                        <li class="breadcrumb-item"><a href="/aptis/test/{{ $id }}">{{$name}}</a></li>
                                        <li class="breadcrumb-item"><a>Grammar & Vocabulary</a></li>
                                    </ol>
                                </nav>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class=" test">
       <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-12 col-lg-12">
                <div class="section-tittle  mb-10 mt-20">
                   <p>
                        Welcome to the Aptis Grammar and Vocabulary Test.
                        The test consists of two sections: <br>
                        Grammar: 25 questions <br>
                        Vocabulary: 25 questions <br>
                        <b>Total Time: 25 minutes</b> 
                   </p>
                </div>
            </div>
            <div class="col-xl-12 col-lg-12">
                <form action="" method="POST">
                    @csrf
                    {{-- <input type="hidden" value="{{$gv->id}}" name="listen_id"> --}}
                    <div class="row">
                        <div class="col-md-8 col-lg-8">
                            <ul class="nav nav-pills nav-justified" id="pills-tab" role="tablist">
                                <li class="nav-item">
                                  <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#part1" role="tab" aria-controls="pills-home" aria-selected="true">Grammar</a>
                                </li>
                                <li class="nav-item">
                                  <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#part2" role="tab" aria-controls="pills-profile" aria-selected="false">Vocabulary</a>
                                </li>
                            </ul>
                            {{-- @dd($gv) --}}
                            <div class="tab-content" id="pills-tabContent">
                                <div class="tab-pane fade show active" id="part1" role="tabpanel" aria-labelledby="pills-home-tab">
                                    @for ($i = 0; $i < 25; $i++)
                                        <div class="question-listening question-{{$i}} {{ $i!=0 ? 'hidden' : 'block'}}">
                                            <b><u>Question {{ $i+1 }}:</u></b><br>
                                            <p>{{ $gv['question_grammar_'.$i][0] }}</p>
                                            <div class="choices">
                                                <input type="hidden" name="answer_gv_question_{{ $i+1 }}" value="null">
                                                <p>
                                                    <span class="my-radio" onclick="activeAns({{ $i+1 }})">
                                                        <input name="answer_gv_question_{{ $i+1 }}" type="radio" id="choice-{{ $i+1 }}-A" value="A">
                                                        <label for="choice-{{ $i+1 }}-A">A.{{$gv['question_grammar_'.$i][2]}}</label>
                                                    </span>
                                                </p>
                                                <p>
                                                    <span class="my-radio"  onclick="activeAns({{ $i+1 }})">
                                                        <input name="answer_gv_question_{{ $i+1 }}" type="radio" id="choice-{{ $i+1 }}-B" value="B">
                                                        <label for="choice-{{ $i+1 }}-B">B.{{$gv['question_grammar_'.$i][3]}}</label>
                                                    </span>
                                                </p>
                                                <p>
                                                    <span class="my-radio"  onclick="activeAns({{ $i+1 }})">
                                                        <input name="answer_gv_question_{{ $i+1 }}" type="radio" id="choice-{{ $i+1 }}-C" value="C">
                                                        <label for="choice-{{ $i+1 }}-C">C.{{$gv['question_grammar_'.$i][4]}}</label>
                                                    </span>
                                                </p>
                                                <p>
                                                    <span class="my-radio"  onclick="activeAns({{ $i+1 }})">
                                                        <input name="answer_gv_question_{{ $i+1 }}" type="radio" id="choice-{{ $i+1 }}-D" value="D">
                                                        <label for="choice-{{ $i+1 }}-D">D.{{$gv['question_grammar_'.$i][5]}}</label>
                                                    </span>
                                                </p>
                                            </div>
                                            <div  class="d-flex justify-content-center mb-3 btn-paginate">
                                                @if($i==0) <button type="button" class="btn btn-primary" onclick="showQuestion({{$i+1}})">Next</button>  @endif
                                                @if ($i==24) <button type="button" class="btn btn-primary" onclick="showQuestion({{$i-1}})">Pre</button> @endif
                                                @if ($i!=0 && $i!=24)
                                                <button type="button" class="btn btn-primary" onclick="showQuestion({{$i-1}})">Pre</button>
                                                <button type="button" class="btn btn-primary" onclick="showQuestion({{$i+1}})">Next</button>
                                                @endif
                                                
                                            </div>
                                        </div>
                                    @endfor
                                </div>
                                <div class="tab-pane fade show" id="part2" role="tabpanel" aria-labelledby="pills-home-tab">
                                    @for ($j = 26; $j < 31; $j++)
                                        <div class="question-vocabulary question-vocabulary-{{$j}} {{ $j!=26 ? '' : 'block'}}">
                                            <p>
                                                @php
                                                    switch( $j ) {
                                                        case 26: echo 'Smiller meaning'; break;
                                                        case 27: echo 'Opposite meaning'; break;
                                                        case 28: echo 'Complete sentence'; break;
                                                        case 29: echo 'Words matching'; break;
                                                        case 30: echo 'Word combinations'; break;
                                                    }
                                                @endphp
                                            </p>
                                            <table>
                                                @foreach($gv['p'.$j.'_list_question'] as $item)
                                                <tr>
                                                    <td>{{ $item }}</td>
                                                    <td>
                                                        <select name="answer_gv_question_{{$j}}[]" id="" onchange="activeAns({{ $j }})">
                                                            @foreach ($gv['p'.$j.'_list_answer'] as $answer)
                                                                <option value="{{ $answer }}">{{ $answer }}</option>
                                                            @endforeach
                                                        </select>
                                                    </td>
                                                </tr>
                                                @endforeach
                                            </table>

                                            <div  class="d-flex justify-content-center mb-3 btn-paginate">
                                                @if($j==26) <button type="button" class="btn btn-primary" onclick="showQuestionVocabulary({{$j+1}})">Next</button>  @endif
                                                @if ($j==30) <button type="button" class="btn btn-primary" onclick="showQuestionVocabulary({{$j-1}})">Pre</button> @endif
                                                @if ($j!=26 && $j!=30)
                                                <button type="button" class="btn btn-primary" onclick="showQuestionVocabulary({{$j-1}})">Pre</button>
                                                <button type="button" class="btn btn-primary" onclick="showQuestionVocabulary({{$j+1}})">Next</button>
                                                @endif
                                                
                                            </div>
                                        </div>
                                    @endfor
                                </div>
                            </div>
                            <div class="sub d-flex justify-content-center mb-3">
                                <input type="submit" class="genric-btn primary-border circle" value="Submit"/>
                            </div>
                        </div>
                        <div class="col-md-4 col-lg-4 ans-map">
                            <div class="time-ans">
                                <i class="far fa-clock"></i>
                                0<span id="h">0</span> :
                                <span id="m">00</span> :
                                <span id="s">00</span>
                            </div>
                            <div class="row">
                                @for($i=1; $i<=30; $i++)
                                    <div class="col-lg-2 col-md-2 choice-ques " >
                                        <div class="ques-{{$i}} ans-ques">{{$i}}</div>
                                    </div>
                                @endfor
                                <hr>
                            </div>
                            
                        </div>
                    </div>
                </form>
              
            </div>
        </div>
       </div>
    </section>
</main>

@endsection


@section('script')
<script>
    var h = 00; // Giờ
    var m = 25; // Phút
    var s = 01; // Giây
    var timeout = null;

    function start()
    {
        if (s === -1){
            m -= 1;
            s = 59;
        }

        if (m === -1){
            h -= 1;
            m = 59;
        }

        if (h == -1){
            clearTimeout(timeout);
            document.getElementById("testToeic").submit();
            return false;
        }

        document.getElementById('h').innerText = h.toString();
        document.getElementById('m').innerText = m.toString();
        document.getElementById('s').innerText = s.toString();

        timeout = setTimeout(function(){
            s--;
            start();
        }, 1000);
    }
    start();

    function activeAns(id){
        $(`.ques-${id}`).addClass('active')
    }
    
    function showQuestion(id){
        $(`.question-listening`).removeClass('block');
        $(`.question-${id}`).addClass('block');

    }
    function showQuestionVocabulary(id){
        $(`.question-vocabulary`).removeClass('block');
        $(`.question-vocabulary-${id}`).addClass('block');

    }
</script>
@endsection
