<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateToeicPart2Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('toeic_part2', function (Blueprint $table) {
            $table->id();
            $table->string('name', 191);
            $table->string('void_part2', 191)->nullable();
            $table->longText('example_test2')->nullable();
            $table->integer('begin');
            $table->integer('end');
            for($i=0; $i<=35; $i++){
                $table->string('answer_question_'.$i, 5)->nullable();
            }; 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('toeic_part2');
    }
}
