<?php

namespace App\Http\Controllers\Admin\Toeic;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Toeic\Part2;
use App\Http\Requests\Toeic\Part2\{AddPart2Request, EditPart2Request};
use App\Repositories\Admin\Toeic\Part2Repository;

class Part2Controller extends Controller
{
    private $part2Repository;

    public function __construct()
    {
        $this->part2Repository = new Part2Repository();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['part2'] = Part2::paginate(20);
        return view('admin.elements.toeic.part2.listPart2', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.elements.toeic.part2.addPart2');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddPart2Request $r)
    {
        $data = $r->all();
        $part_2 = $this->part2Repository->createPart2($data); 
        if($part_2){   
             return redirect('/admin/toeic/part_2')->with('thongbao', 'Success create new part II test'); 
        }   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data['part2'] = Part2::find($id);
        return view('admin.elements.toeic.part2.showPart2', $data);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['part2'] = Part2::find($id);
        return view('admin.elements.toeic.part2.editPart2', $data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditPart2Request $r, $id)
    {
        $data = $r->all();
        $part_2 = $this->part2Repository->updatePart2($data, $id); 
        if($part_2){   
             return redirect('/admin/toeic/part_2')->with('thongbao', 'Success edit part II test'); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Part2::destroy($id);
        return redirect()->back()->with('thongbao', 'Đã xóa thành công!!!');
    }
}
