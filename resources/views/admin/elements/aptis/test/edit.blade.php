@extends('admin.master')

@section('content')
     <!--main-->
    <div class="container add-test-toeic">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Edit Aptis test</h1>
            </div>
        </div>

        <!--/.row-->
    <form action="/admin/aptis/test/{{ $test->id }}" method="post" accept-charset="utf-8">
        @csrf    
        <input type="hidden" name="_method" value="put">
        <div id="wizard_verticle" class="form_wizard wizard_verticle">
            <ul class="list-unstyled wizard_steps">
                <li>
                    <a href="#step-11">
                    <span class="step_no">1</span>
                    </a>
                </li>
                <li>
                    <a href="#step-22">
                    <span class="step_no">2</span>
                    </a>
                </li>
               
            </ul>
            <div id="step-11">
                <h2 class="StepTitle">Step 1</h2>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3" for="first-name">Name Test <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6">
                        <input type="text" id="first-name2" required class="form-control col-md-7 col-xs-12" name="name_test" value="{{ old('name_test', $test->name_test)}}" placeholder="Name test">
                    </div>
                    <div style="clear: both;"></div>
                </div>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3">Role</label>
                    <div class="col-md-6 col-sm-6">
                        <div id="gender2" class="btn-group" data-toggle="buttons">
                            <label class="btn btn-primary  {{ $test->role == "free" ? "active" : "" }}">
                                <input type="radio" name="role" value="free" {{ $test->role == "free" ? "checked" : "" }}> Free 
                            </label>
                            <label class="btn btn-primary  {{ $test->role == "fee" ? "active" : "" }}">
                                <input type="radio" name="role" value="fee" {{ $test->role == "fee" ? "checked" : "" }}> Fee
                            </label>
                            
                        </div>
                    </div>
                </div>
                {{ ShowErrors($errors, 'role')}}
                <hr> <br>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3" for="first-name">Description<span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6">
                        <textarea name="description" class="form-control col-md-7 col-xs-12" style="height: 100px;"> {{ old('description', $test->description)}} </textarea>
                    </div>
                    <div style="clear: both;"></div>
                </div>
            </div>
            <div id="step-22">
                <h2 class="StepTitle">Step 2</h2>
                
                <div class="form-group" alight="center">
                    <label class="btn btn-primary">
                        <input type="radio" name="classify" value="practice" {{ $test->classify == "practice" ? "checked" : "" }}> Practice 
                    </label>
                    <label class="btn btn-primary">
                        <input type="radio" name="classify" value="test" {{ $test->classify == "test" ? "checked" : "" }}> Test
                    </label>
                </div>

                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label><b>Listening test</b></label>
                            <select class="form-control" name="listen_id">
                                <option value=""> Choose Listening test</option>
                                @foreach($listening as $item)
                                    <option value="{{$item->id}}" {{ $test->listen_id == $item->id ? "selected" : "" }}>{{$item->name}}</option>
                                @endforeach    
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <label><b>Reading test</b></label>
                            <select class="form-control" name="read_id">
                                <option value=""> Choose Reading test</option>
                                @foreach($reading as $item)
                                    <option value="{{$item->id}}" {{ $test->read_id == $item->id ? "selected" : "" }}>{{$item->name}}</option>
                                @endforeach    
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <label><b>Writing test</b></label>
                            <select class="form-control" name="write_id">
                                <option value="">Choose Writing test </option>
                                @foreach($writing as $item)
                                    <option value="{{$item->id}}" {{ $test->write_id == $item->id ? "selected" : "" }}>{{$item->name}}</option>
                                @endforeach    
                            </select>
                        </div>
                    </div>

                    <div class="col-md-1">
                    </div>

                    <div class="col-md-5">
                        <div class="form-group">
                            <label><b>Speaking test</b></label>
                            <select class="form-control" name="speak_id">
                                <option value=""> Choose Speaking test</option>
                                @foreach($speaking as $item)
                                    <option value="{{$item->id}}" {{ $test->speak_id == $item->id ? "selected" : "" }}>{{$item->name}}</option>
                                @endforeach    
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <label><b>Grammar & Vocabulary test</b></label>
                            <select class="form-control" name="gv_id">
                                <option value=""> Choose Grammar & Vocabulary test</option>
                                @foreach($gv as $item)
                                    <option value="{{$item->id}}" {{ $test->gv_id == $item->id ? "selected" : "" }}>{{$item->name}}</option>
                                @endforeach    
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </form>

        <!--/.row-->
    </div>

    <!--end main-->

@endsection

@section('script')
    <script src="vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
@endsection
