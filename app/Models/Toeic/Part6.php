<?php

namespace App\Models\Toeic;

use Illuminate\Database\Eloquent\Model;

class Part6 extends Model
{
    protected $table = 'toeic_part6';

    protected $guarded = [
        'id'
    ];
}
