<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Teacher;
use Auth;

class ClassController extends Controller
{
    public function index(){
        $id_teacher = Auth::user()->id;
        $class = Teacher::where('teacher_id', $id_teacher)->get();
        return view('user.elements.teacher.index', compact('class'));
    }

    public function createClass(Request $request){
        $data = $request->all();
        $data['teacher_id'] = $request->user()->id;
        Teacher::create($data);
        return redirect()->back()->with('success', 'Tao lop hoc thanh cong');
    }

    public function assignTeacher() {
        $user = Auth::user();
        $user->level = 2;
        $user = $user->update();
        return redirect()->back()->with('success', 'Bạn vừa chuyển đổi sang tài khoản giáo viên');
    }
}
                