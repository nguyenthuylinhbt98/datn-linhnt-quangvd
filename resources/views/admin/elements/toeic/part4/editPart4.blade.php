@extends('admin.master')
@section('content')
<!--main-->
<div class="container main exam">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Cập nhật đề thi</h1>
        </div>
    </div> 
    @if(session('thongbao'))
       <div class="alert alert-success">
            {{ session('thongbao') }}
        </div>

    @endif
    <!--/.row-->
    <form action="/admin/toeic/part_4/{{$part4->id}}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="_method" value="put">
        <input type="hidden" name="id" value="{{$part4->id}}">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><i class="fas fa-user"></i>Thêm đề thi mới</div>
                    <div class="panel-body">
                        <div class="row justify-content-center" style="margin-bottom:40px">
                            <div class="col-md-8 col-lg-8 col-lg-offset-2">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input value="{{$part4->name}}" type="text" name="name" placeholder="Name test" class="form-control" required>
                                    {{ ShowErrors($errors, 'name')}}
                                </div>
                                <div class="container">
                                    <h2>Exam Toeic</h2>
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#part1">Part IV</a></li>
                                        
                                    </ul>
                                    <div class="tab-content">
                                       

                                        {{-- Part_4 --}}
                                        <div id="part1" class="tab-pane fade in active">
                                             <h3><b>PART IV</b></h3>
                                             <p> <b>Directions:</b> You will hear some talks given by a single speaker. You will be asked to answer three questions about what the speaker says in each talk. Select the best response to each question and mark the letter (A), (B), (C), or (D) on your answer sheet. The talks will not be printed in your test book and will be spoken only one time.</p>
                                            <br><b>Voice: </b><br>
                                            <audio src="../{{$part4->void_part4}}" loop controls></audio>
                                            <input type="file" name="void_part4"><br>
                                            <label for="begin">
                                                Question begin: <input type="text" id="begin" name="begin" value={{$part4->begin}}>
                                            </label><br>
                                            @for($i=0; $i <= 100-$part4->begin; $i++)
                                                <b>Question {{$i+$part4->begin}}:</b>
                                                <br>
                                                <input class="form-control" type="text" value="{{$part4->{'question_'.$i}[0]}}" name="question_{{$i}}[]" placeholder="Question {{$i+$part4->begin}}"><br>
                                                <input class="form-control" type="text" value="{{$part4->{'question_'.$i}[1]}}" name="question_{{$i}}[]" placeholder="Answer A"><br>
                                                <input class="form-control" type="text" value="{{$part4->{'question_'.$i}[2]}}" name="question_{{$i}}[]" placeholder="Answer B"><br>
                                                <input class="form-control" type="text" value="{{$part4->{'question_'.$i}[3]}}" name="question_{{$i}}[]" placeholder="Answer C"><br>
                                                <input class="form-control" type="text" value="{{$part4->{'question_'.$i}[4]}}" name="question_{{$i}}[]" placeholder="Answer D">

                                                <b>Answer choice:</b>
                                                @if(isset($part4->{'question_'.$i}[5]))
	                                                <div class="row">
	                                                    <div class="col-md-1">
	                                                        <p style="float: left; margin-right: 5px;">A</p> <input @if($part4->{'question_'.$i}[5]=='A') checked @endif type="radio" name="question_{{$i}}[]" value="A">
	                                                    </div>
	                                                    <div class="col-md-1">
	                                                        <p style="float: left; margin-right: 5px;">B</p> <input @if($part4->{'question_'.$i}[5]=='B') checked @endif type="radio" name="question_{{$i}}[]" value="B">
	                                                    </div>
	                                                    <div class="col-md-1">
	                                                        <p style="float: left; margin-right: 5px;">C</p> <input @if($part4->{'question_'.$i}[5]=='C') checked @endif type="radio" name="question_{{$i}}[]" value="C">
	                                                    </div>
	                                                     <div class="col-md-1">
	                                                        <p style="float: left; margin-right: 5px;">D</p> <input @if($part4->{'question_'.$i}[5]=='D') checked @endif type="radio" name="question_{{$i}}[]" value="D">
	                                                    </div>
	                                                </div>
	                                            @else

	                                            	 <div class="row">
                                                        <div class="col-md-1">
                                                            <p style="float: left; margin-right: 5px;">A</p> <input type="radio" name="question_{{$i}}[]" value="A">
                                                        </div>
                                                        <div class="col-md-1">
                                                            <p style="float: left; margin-right: 5px;">B</p> <input type="radio" name="question_{{$i}}[]" value="B">
                                                        </div>
                                                        <div class="col-md-1">
                                                            <p style="float: left; margin-right: 5px;">C</p> <input type="radio" name="question_{{$i}}[]" value="C">
                                                        </div>
                                                         <div class="col-md-1">
                                                            <p style="float: left; margin-right: 5px;">D</p> <input type="radio" name="question_{{$i}}[]" value="D">
                                                        </div>
                                                    </div>
	                                            
	                                            @endif    
                                                <hr>
                                            @endfor
                                        </div>

                                        
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-lg-8 col-lg-offset-2 text-right"  style="margin-top: 20px;">
                                    <button class="btn btn-success"  type="submit">Cập nhật đề</button>
                                    <button class="btn btn-danger" type="reset">Huỷ bỏ</button>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!--/.row-->
</div>
<!--end main-->
@endsection

@section('script')
   
    <script src="/vendor/unisharp/laravel-ckeditor/ckeditor.js"></script>
    <script src="/vendor/unisharp/laravel-ckeditor/adapters/jquery.js"></script>
    <script>
        $('textarea').ckeditor();
    </script>
@endsection