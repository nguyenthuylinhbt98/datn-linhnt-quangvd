<?php

use Illuminate\Database\Seeder;

class ClassMemberSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('class_member')->delete();
        DB::table('class_member')->insert([             
            [
                "id"=> 1,
                "user_id"=> 278,
                "class_id"=> 1,
                "status"=> 0
              ],
              [
                "id"=> 2,
                "user_id"=> 63,
                "class_id"=> 1,
                "status"=> 0
              ],
              [
                "id"=> 3,
                "user_id"=> 29,
                "class_id"=> 1,
                "status"=> 0
              ],
              [
                "id"=> 4,
                "user_id"=> 92,
                "class_id"=> 1,
                "status"=> 1
              ],
              [
                "id"=> 5,
                "user_id"=> 72,
                "class_id"=> 1,
                "status"=> 0
              ],
              [
                "id"=> 6,
                "user_id"=> 76,
                "class_id"=> 1,
                "status"=> 1
              ],
              [
                "id"=> 7,
                "user_id"=> 338,
                "class_id"=> 1,
                "status"=> 0
              ],
              [
                "id"=> 8,
                "user_id"=> 254,
                "class_id"=> 1,
                "status"=> 0
              ],
              [
                "id"=> 9,
                "user_id"=> 202,
                "class_id"=> 1,
                "status"=> 0
              ],
              [
                "id"=> 10,
                "user_id"=> 399,
                "class_id"=> 1,
                "status"=> 1
              ],
              [
                "id"=> 11,
                "user_id"=> 19,
                "class_id"=> 1,
                "status"=> 0
              ],
              [
                "id"=> 12,
                "user_id"=> 113,
                "class_id"=> 1,
                "status"=> 0
              ],
              [
                "id"=> 13,
                "user_id"=> 4,
                "class_id"=> 1,
                "status"=> 0
              ],
              [
                "id"=> 14,
                "user_id"=> 360,
                "class_id"=> 1,
                "status"=> 1
              ],
              [
                "id"=> 15,
                "user_id"=> 311,
                "class_id"=> 1,
                "status"=> 1
              ],
              [
                "id"=> 16,
                "user_id"=> 326,
                "class_id"=> 1,
                "status"=> 1
              ],
              [
                "id"=> 17,
                "user_id"=> 73,
                "class_id"=> 1,
                "status"=> 0
              ],
              [
                "id"=> 18,
                "user_id"=> 275,
                "class_id"=> 1,
                "status"=> 1
              ],
              [
                "id"=> 19,
                "user_id"=> 356,
                "class_id"=> 1,
                "status"=> 1
              ],
              [
                "id"=> 20,
                "user_id"=> 274,
                "class_id"=> 1,
                "status"=> 1
              ],
              [
                "id"=> 21,
                "user_id"=> 42,
                "class_id"=> 1,
                "status"=> 0
              ]
              
            
        ]);
    }
}
