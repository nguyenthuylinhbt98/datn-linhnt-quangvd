<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateToeicPart7Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('toeic_part7', function (Blueprint $table) {
            $table->id();
            $table->string('name', 191);
            $table->integer('begin');
            $table->integer('num_para');

            for($i=0; $i<15; $i++){
                $table->longText('paragraph_'.$i)->nullable();
            };

            for($i=0; $i<55; $i++){
                $table->longText('question_'.$i)->nullable();
            };
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('toeic_part7');
    }
}
