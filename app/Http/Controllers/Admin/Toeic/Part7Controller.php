<?php

namespace App\Http\Controllers\Admin\Toeic;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Toeic\Part7;
use App\Http\Requests\Toeic\Part7\{AddPart7Request, EditPart7Request};
use App\Repositories\Admin\Toeic\Part7Repository;

class Part7Controller extends Controller
{
    private $part7Repository;

    public function __construct()
    {
        $this->part7Repository = new Part7Repository();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['part7'] = Part7::paginate(10);
        return view('admin.elements.toeic.part7.listPart7', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.elements.toeic.part7.addPart7');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddPart7Request $r)
    {
        $data = $r->all();
        $part_7 = $this->part7Repository->createPart7($data); 
        if($part_7){   
             return redirect('/admin/toeic/part_7')->with('thongbao', 'Success create new part7 test'); 
        }  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['part7'] = $this->part7Repository->editPart7($id); 
        return view('admin.elements.toeic.part7.editPart7',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditPart7Request $r, $id)
    {
        $part_7 = $this->part7Repository->updatePart7($r->all(), $id); 
        if($part_7){   
             return redirect('/admin/toeic/part_7')->with('thongbao', 'Success edit part VII'); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Part7::destroy($id);
        return redirect()->back()->with('thongbao', 'Đã xóa thành công!!!');
    }
}
