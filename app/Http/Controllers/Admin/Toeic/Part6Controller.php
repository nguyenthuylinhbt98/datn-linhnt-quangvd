<?php

namespace App\Http\Controllers\Admin\Toeic;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Toeic\Part6;
use App\Http\Requests\Toeic\Part6\{AddPart6Request, EditPart6Request};
use App\Repositories\Admin\Toeic\Part6Repository;

class Part6Controller extends Controller
{
    private $part6Repository;

    public function __construct()
    {
        $this->part6Repository = new Part6Repository();
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data['part6'] = Part6::paginate(10);
        return view('admin.elements.toeic.part6.listPart6', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.elements.toeic.part6.addPart6');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AddPart6Request $r)
    {
        $part_6 = $this->part6Repository->createPart6($r->all()); 
        if($part_6){   
             return redirect('/admin/toeic/part_6')->with('thongbao', 'Success create new part VI test'); 
        }  
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $data['part6'] = $this->part6Repository->editPart6($id); 
        return view('admin.elements.toeic.part6.editPart6',$data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(EditPart6Request $r, $id)
    {
        $part_6 = $this->part6Repository->updatePart6($r->all(), $id); 
        if($part_6){   
             return redirect('/admin/toeic/part_6')->with('thongbao', 'Success edit part VI'); 
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Part6::destroy($id);
        return redirect()->back()->with('thongbao', 'Đã xóa thành công!!!');
    }
}
