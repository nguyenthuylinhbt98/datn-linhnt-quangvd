<?php 

	Route::get('/', 'HomeController@getIndex');
	Route::resource('post', 'PostController');

	Route::group(['namespace'=>'Users'], function() {
		Route::resource('user', 'UserController');
	    
	});

	Route::group(['prefix' => 'toeic', 'namespace'=>'Toeic'], function() {
		Route::resource('part_1', 'Part1Controller');
		Route::resource('part_2', 'Part2Controller');
		Route::resource('part_3', 'Part3Controller');
		Route::resource('part_4', 'Part4Controller');
		Route::resource('part_5', 'Part5Controller');
		Route::resource('part_6', 'Part6Controller');
		Route::resource('part_7', 'Part7Controller');
		Route::resource('test', 'TestController');
		Route::resource('transcript', 'TranscriptController');
	});

	Route::group(['prefix' => 'aptis', 'namespace'=>'Aptis'], function() {
		Route::resource('listening', 'ListeningController');
		Route::resource('reading', 'ReadingController');
		Route::resource('writing', 'WritingController');
		Route::resource('speaking', 'SpeakingController');
		Route::resource('gv', 'GVController');
		Route::resource('test', 'AptisTestController');
	});

