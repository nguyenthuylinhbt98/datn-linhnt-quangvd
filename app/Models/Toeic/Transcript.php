<?php

namespace App\Models\Toeic;

use Illuminate\Database\Eloquent\Model;

class Transcript extends Model
{
    protected $table = 'toeic_transcript';

    protected $guarded = [
        'id'
    ];
}
