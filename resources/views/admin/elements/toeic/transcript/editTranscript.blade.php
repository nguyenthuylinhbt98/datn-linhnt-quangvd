@extends('admin.master')

@section('content')
     <!--main-->
    <div class="container transcript-table">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Bảng quy đổi điểm thi Toeic</h1>
            </div>
        </div>

        @if(session('thongbao'))
           <div class="alert alert-success">
                {{ session('thongbao') }}
            </div>

        @endif

        <!--/.row-->
        <form action="/admin/toeic/transcript/{{$transcript->id}}" method="post" accept-charset="utf-8">
            @csrf
            <input type="hidden" name="_method" value="put">
            <div class="row">
                <div class="col-xs-12 col-md-12 col-lg-12">
                        <div class="panel panel-primary">
                            <div class="panel-heading"><i class="fas fa-user"></i> Bảng điểm Toeic mới nhất</div>
                            <div class="panel-body">
                                <div class="row justify-content-center" style="margin-bottom:40px">

                                    <div class="col-md-8 col-lg-8 col-lg-offset-2">
                                        <table>
                                                <thead>
                                                    <tr>
                                                        <th>Số câu nghe</th>
                                                        <th>Điểm nghe</th>
                                                        <th>Số câu đọc</th>
                                                        <th>Điểm đọc</th>  
                                                    </tr>
                                                </thead>
                                                <tbody>
                                                    @for($i=1; $i<=100; $i++)
                                                        <tr style="text-align: center;">
                                                            <td>{{$i}}</td>
                                                            <td> <input style="text-align: center;" type="number" name="diem_nghe[]" value="{{$diem_nghe[$i-1]}}"></td>
                                                            <td>{{$i}}</td>
                                                            <td> <input  style="text-align: center;" type="number" name="diem_doc[]" value="{{$diem_doc[$i-1]}}"></td>
                                                        </tr>

                                                    @endfor
                                                    
                                                </tbody>
                                            </table>                                                                  
                                    </div>  
                                        
                                    </div>
                                    <div class="row">
                                        <div class="col-md-8 col-lg-8 col-lg-offset-2 text-right">
                                          
                                            <button class="btn btn-success"  type="submit">Tạo bảng điểm</button>
                                            <button class="btn btn-danger" type="reset">Huỷ bỏ</button>
                                        </div>
                                    </div>
                                   

                                </div>
                            
                                <div class="clearfix"></div>
                            </div>
                        </div>

                </div>
            </div>
        </form>

        <!--/.row-->
    </div>

    <!--end main-->

@endsection
