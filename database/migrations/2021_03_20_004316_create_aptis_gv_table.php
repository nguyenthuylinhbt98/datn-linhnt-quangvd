<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAptisGvTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('aptis_gv', function (Blueprint $table) {
            $table->id();
            $table->string('name', 191)->nullable();
            for($i=0; $i<26; $i++){
                $table->longText('question_grammar_'.$i)->nullable();
            };
            for($i=26; $i<=30; $i++){
                $table->longText('p'.$i.'_list_answer')->nullable();
                $table->longText('p'.$i.'_list_question')->nullable();
                $table->longText('p'.$i.'_list_correct_answer')->nullable();
            };
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('aptis_gv');
    }
}
