<?php

namespace App\Models\Toeic;

use Illuminate\Database\Eloquent\Model;

class Part7 extends Model
{
    protected $table = 'toeic_part7';

    protected $guarded = [
        'id'
    ];
}
