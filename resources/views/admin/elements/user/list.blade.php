@extends('admin.master')
@section('admin', 'class=active')
@section('content')
<!--main-->
<div class="container main">
    <div class="">
        <div class="page-title">
            <div class="title_left">
                <a href="/admin/user/create" class="btn btn-primary" style="margin-bottom: 20px;">Add new user</a>
                @if(session('thongbao'))
                  <div class="alert alert-success">
                      {{ session('thongbao') }}
                  </div>
                @endif
                @if(session('canhbao'))
                  <div class="alert alert-danger">
                      {{ session('canhbao') }}
                  </div>
                @endif
            </div>
        </div>
        <div class="clearfix"></div>

        <div class="row">
            <div class="col-md-12">
                <div class="x_panel">
                    <div class="x_content">
                      <h3>List Admins</h3><hr>
                        <div class="row">
                            @foreach($admins as $admin)
                              <div class="col-md-4 col-sm-4 col-xs-12 profile_details">
                                  <div class="well profile_view">
                                      <div class="col-sm-12">
                                          <h4 class="brief"><i>Admin</i></h4>
                                           
                                          <div class="left col-xs-7">
                                              <h2>{{ $admin->full }}</h2>

                                              <p><strong>Học thuật: </strong>  </p>
                                              <ul class="list-unstyled">
                                                  <li><i class="fa fa-building"></i> Address: {{ $admin->address }}</li>
                                                  <li><i class="fa fa-phone"></i> Phone: {{ $admin->phone }}</li>
                                              </ul>
                                          </div>
                                          <div class="right col-xs-5 text-center">
                                            @if(isset($admin->avatar))
                                              <img src="../{{ $admin->avatar }}" alt="" class="img-circle img-responsive" style="width: 130px; height: 130px">
                                            @else
                                              <img src="../upload/user_avatar/no_avatar.png" alt="" class="img-circle img-responsive" style="width: 130px; height: 130px">
                                            @endif  
                                          </div>
                                      </div>
                                      <div class="col-xs-12 bottom text-center">
                                          <div class="col-xs-12 col-sm-6 emphasis">
                                          </div>
                                          <div class="col-xs-12 col-sm-6 emphasis">
                                              <a href="/admin/user/{{ $admin->id }}/edit" type="button" class="btn btn-success btn-xs"><i class="fa fa-comments-o"></i> Edit </a>

                                              <a href="/admin/user/{{ $admin->id }}" type="button" class="btn btn-primary btn-xs"><i class="fa fa-user"> </i> Chi tiết</a>
                                              
                                          </div>
                                      </div>
                                  </div>
                              </div>
                            @endforeach  
                            
                        </div>
                    </div>
                </div>
            </div>
        </div>

         <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>List Teacher <small></small></h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
               
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              
              <table id="datatable" class="table table-striped table-bordered" style="text-align: center;">
                <thead>
                  <tr style="background: #daeaf2" >
                    <th style="text-align: center;">ID</th>
                    <th style="text-align: center;">Full name</th>
                    <th style="text-align: center;">Avatar</th>
                    <th style="text-align: center;">Email</th>
                    <th style="text-align: center;">Phonenumber</th>
                    <th style="text-align: center;">Address</th>
                    
                    <th width="12%" style="text-align: center;">Tùy chọn</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($teacher as $user)

                    <tr>
                      <td>{{$user->id}}</td>
                      <td>{{ $user->full }}</td>
                      <td>
                        @if(isset($user->avatar))
                          <img src="../{{$user->avatar}}" alt="" style="width: 50px; height: 50px; border-radius: 50%">
                        @else
                          <img src="../upload/user_avatar/no_avatar.png" alt="" style="width: 50px; height: 50px; border-radius: 50%">
                        @endif
                        
                      </td>
                      <td>{{ $user->email }}</td>
                      <td>{{ $user->phone }}</td>
                      <td>{{ $user->address }}</td>
                      <td>
                        <a href="/admin/user/{{ $user->id }}/edit" class="btn btn-round btn-primary btn-xs"><i class="fa fa-pencil" aria-hidden="true"></i> Sửa</a>
                        <a href="#" class="btn btn-round btn-danger btn-xs" data-toggle="modal" data-target="#exampleModal{{$user->id}}"><i class="fa fa-trash" aria-hidden="true"></i> Xóa</a>

                            <div class="modal fade" id="exampleModal{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                      <div class="modal-header">
                                          <h5 class="modal-title" id="exampleModalLabel">Deleter user</h5>
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                          </button>
                                      </div>
                                      
                                      <form action="/admin/user/{{ $user->id }}" method="POST" accept-charset="utf-8">
                                          <div class="modal-body">
                                              @csrf
                                              <input type="hidden" name="_method" value="delete">
                                              <h4>Xóa <b style="color: red">{{ $user->full }}</b> khỏi danh sách thành viên </h4>
                                          </div>
                                          <div class="modal-footer">
                                              <button type="button" class="btn btn-default" data-dismiss="modal">Quay lại</button>
                                              <button type="submit" class="btn btn-danger">Xóa</button>
                                          </div>
                                      </form>
                                  </div>
                              </div>
                            </div>
                      </td>
                    </tr>
                  @endforeach
                  
                </tbody>

              </table>              
            </div>
          </div>
        </div>

        <div class="col-md-12 col-sm-12 col-xs-12">
          <div class="x_panel">
            <div class="x_title">
              <h2>List Users  <small></small></h2>
              <ul class="nav navbar-right panel_toolbox">
                <li><a class="collapse-link"><i class="fa fa-chevron-up"></i></a>
                </li>
               
              </ul>
              <div class="clearfix"></div>
            </div>
            <div class="x_content">
              
              <table id="datatable" class="table table-striped table-bordered" style="text-align: center;">
                <thead>
                  <tr style="background: #daeaf2" >
                    <th style="text-align: center;">ID</th>
                    <th style="text-align: center;">Full name</th>
                    <th style="text-align: center;">Avatar</th>
                    <th style="text-align: center;">Email</th>
                    <th style="text-align: center;">Phonenumber</th>
                    <th style="text-align: center;">Address</th>
                    
                    <th width="12%" style="text-align: center;">Tùy chọn</th>
                  </tr>
                </thead>
                <tbody>
                  @foreach($users as $user)

                    <tr>
                      <td>{{$user->id}}</td>
                      <td>{{ $user->full }}</td>
                      <td>
                        @if(isset($user->avatar))
                          <img src="../{{$user->avatar}}" alt="" style="width: 50px; height: 50px; border-radius: 50%">
                        @else
                          <img src="../upload/user_avatar/no_avatar.png" alt="" style="width: 50px; height: 50px; border-radius: 50%">
                        @endif
                        
                      </td>
                      <td>{{ $user->email }}</td>
                      <td>{{ $user->phone }}</td>
                      <td>{{ $user->address }}</td>
                      <td>
                        <a href="/admin/user/{{ $user->id }}/edit" class="btn btn-round btn-primary btn-xs"><i class="fa fa-pencil" aria-hidden="true"></i> Sửa</a>
                        <a href="#" class="btn btn-round btn-danger btn-xs" data-toggle="modal" data-target="#exampleModal{{$user->id}}"><i class="fa fa-trash" aria-hidden="true"></i> Xóa</a>

                            <div class="modal fade" id="exampleModal{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                              <div class="modal-dialog" role="document">
                                  <div class="modal-content">
                                      <div class="modal-header">
                                          <h5 class="modal-title" id="exampleModalLabel">Deleter user</h5>
                                          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                          <span aria-hidden="true">&times;</span>
                                          </button>
                                      </div>
                                      
                                      <form action="/admin/user/{{ $user->id }}" method="POST" accept-charset="utf-8">
                                          <div class="modal-body">
                                              @csrf
                                              <input type="hidden" name="_method" value="delete">
                                              <h4>Xóa <b style="color: red">{{ $user->full }}</b> khỏi danh sách thành viên </h4>
                                          </div>
                                          <div class="modal-footer">
                                              <button type="button" class="btn btn-default" data-dismiss="modal">Quay lại</button>
                                              <button type="submit" class="btn btn-danger">Xóa</button>
                                          </div>
                                      </form>
                                  </div>
                              </div>
                            </div>
                      </td>
                    </tr>
                  @endforeach
                  
                </tbody>

              </table>
              <div align="center">
                {{$users->links()}}
              </div>
              
            </div>
          </div>
        </div>
        
    </div>
</div>
<!--end main-->
@endsection