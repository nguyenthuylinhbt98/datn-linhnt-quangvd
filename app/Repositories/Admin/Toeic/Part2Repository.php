<?php

namespace App\Repositories\Admin\Toeic;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Models\Toeic\Part2;
//use Your Model

/**
 * Class Part2Repository.
 */
class Part2Repository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Part2::class;
    }

    public function createPart2(array $data)
    {
    	$part_2['example_test2'] = $data['example_test2'];
        $part_2['name'] = $data['name'];
        $part_2['begin'] = $data['begin'];
        $part_2['end'] = $data['end'];

        if(isset($data['void_part2'])){
            $part_2['void_part2'] = UploadFile($data['void_part2'], 'mp3/toeic_part2');
        }

        for ($i=0; $i <= 35; $i++) { 
            if(isset($data['answer_question_'.$i])){
                $part_2['answer_question_'.$i]=$data['answer_question_'.$i];
            }else{
                $part_2['answer_question_'.$i]='null';
            }
        }

        Part2::create($part_2);        
        return true;
    }


    public function updatePart2(array $data, int $id)
    {
        $part2 = self::getById($id);
      
        $part_2['example_test2'] = $data['example_test2'];
        $part_2['name'] = $data['name'];
        $part_2['begin'] = $data['begin'];
        $part_2['end'] = $data['end'];

        if(isset($data['void_part2'])){
            $part_2['void_part2'] = UploadFile($data['void_part2'], 'mp3/toeic_part2');
        }

        for ($i=0; $i <= 35; $i++) { 
            if(isset($data['answer_question_'.$i])){
                $part_2['answer_question_'.$i]=$data['answer_question_'.$i];
            }else{
                $part_2['answer_question_'.$i]='null';
            }
        }
        $part2->update($part_2);        
        return true;
    }
}
