<?php

namespace App\Http\Requests\Toeic\Part7;

use Illuminate\Foundation\Http\FormRequest;

class AddPart7Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'name'=>'required|unique:toeic_part7,name',
        ];
    }
    public function messages()
    {
        return [
            'name.required'=>'Tên Part7 không được để trống!',
            'name.unique'=>'Tên Part7 đã tồn tại!',
        ];
    }
}
