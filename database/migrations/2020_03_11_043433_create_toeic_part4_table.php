<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateToeicPart4Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('toeic_part4', function (Blueprint $table) {
            $table->id();
            $table->string('name', 191);
            $table->string('void_part4', 191)->nullable();
            $table->integer('begin');
            for($i=0; $i<40; $i++){
                $table->longText('question_'.$i)->nullable();
            };
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('toeic_part4');
    }
}
