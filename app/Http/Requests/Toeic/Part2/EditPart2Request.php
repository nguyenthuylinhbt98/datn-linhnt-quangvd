<?php

namespace App\Http\Requests\Toeic\Part2;

use Illuminate\Foundation\Http\FormRequest;

class EditPart2Request extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
           'name'=>'required|unique:toeic_part2,name,'.$this->id,
        ];
    }
    public function messages()
    {
        return [
            'name.required'=>'Tên Part2 không được để trống!',
            'name.unique'=>'Tên Part2 đã tồn tại!',
        ];
    }
}
