<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateToeicPart1Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('toeic_part1', function (Blueprint $table) {
            $table->id();
            $table->string('name', 191);
            $table->string('void_part1', 191)->nullable();
            $table->string('example_img', 191)->nullable();
            $table->longText('example_test1')->nullable();
            $table->integer('number_ques');

            for($i=1; $i<=10; $i++){
                $table->string('img_question_'.$i, 191)->nullable();
                $table->string('answer_question_'.$i, 25)->nullable();
            }; 
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('toeic_part1');
    }
}
