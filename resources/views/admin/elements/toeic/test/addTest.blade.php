@extends('admin.master')

@section('content')
     <!--main-->
    <div class="container add-test-toeic">
        <div class="row">
            <div class="col-lg-12">
                <h1 class="page-header">Create new test toeic</h1>
            </div>
        </div>

        <!--/.row-->
    <form action="/admin/toeic/test" method="post" accept-charset="utf-8">
        @csrf    

        <div id="wizard_verticle" class="form_wizard wizard_verticle">
            <ul class="list-unstyled wizard_steps">
                <li>
                    <a href="#step-11">
                    <span class="step_no">1</span>
                    </a>
                </li>
                <li>
                    <a href="#step-22">
                    <span class="step_no">2</span>
                    </a>
                </li>
                <li>
                    <a href="#step-33">
                    <span class="step_no">3</span>
                    </a>
                </li>
               
            </ul>
            <div id="step-11">
                <h2 class="StepTitle">Step 1</h2>

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3" for="first-name">Name Test <span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6">
                        <input type="text" id="first-name2" required class="form-control col-md-7 col-xs-12" name="name_test" placeholder="Name test">
                    </div>
                    <div style="clear: both;"></div>
                </div>
                {{-- level --}}
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3">Level</label>
                    <div class="col-md-6 col-sm-6">
                        <div id="gender2" class="btn-group" data-toggle="buttons">
                            <label class="btn btn-default">
                                <input type="radio" name="level" value="350" checked="">350+ 
                            </label>
                            <label class="btn btn-default">
                                <input type="radio" name="level" value="450" >450+ 
                            </label>
                            <label class="btn btn-default">
                                <input type="radio" name="level" value="550" > 550+
                            </label>
                            <label class="btn btn-default">
                                <input type="radio" name="level" value="750" > 750+
                            </label>
                        </div>
                    </div>
                    <div style="clear: both;"></div>
                </div>
                {{ ShowErrors($errors, 'level')}}

                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3">Role</label>
                    <div class="col-md-6 col-sm-6">
                        <div id="gender2" class="btn-group" data-toggle="buttons">
                            <label class="btn btn-primary">
                                <input type="radio" name="role" value="free" checked=""> Free 
                            </label>
                            <label class="btn btn-primary">
                                <input type="radio" name="role" value="fee" > Fee
                            </label>
                            
                        </div>
                    </div>
                </div>
                {{ ShowErrors($errors, 'role')}}

                
            </div>
            <div id="step-22">
                <h2 class="StepTitle">Step 2</h2>
                <div class="form-group" alight="center">
                    <label class="btn btn-primary">
                        <input type="radio" name="classify" value="practice" checked=""> Practice 
                    </label>
                    <label class="btn btn-primary">
                        <input type="radio" name="classify" value="test" > Test
                    </label>
                </div>
                <div class="form-group">
                    <label class="control-label col-md-3 col-sm-3" for="first-name">Description<span class="required">*</span>
                    </label>
                    <div class="col-md-6 col-sm-6">
                        <textarea name="description" class="form-control col-md-7 col-xs-12" style="height: 100px;"></textarea>
                        
                    </div>
                    <div style="clear: both;"></div>
                </div>
                
            </div>
            <div id="step-33">
                <h2 class="StepTitle">Step 3</h2>
                <div class="row">
                    <div class="col-md-5">
                        <div class="form-group">
                            <label><b>Part I</b></label>
                            <select class="form-control" name="id_part[]">
                                <option value="0"> Chọn Part I</option>
                                option
                                @foreach($part_1 as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach    
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <label><b>Part II</b></label>
                            <select class="form-control" name="id_part[]">
                                <option value="0"> Chọn Part II</option>

                                @foreach($part_2 as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach    
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <label><b>Part III</b></label>
                            <select class="form-control" name="id_part[]">
                                <option value="0"> Chọn Part III</option>

                                @foreach($part_3 as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach    
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <label><b>Part IV</b></label>
                            <select class="form-control" name="id_part[]">
                                <option value="0"> Chọn Part IV</option>

                                @foreach($part_4 as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach    
                            </select>
                        </div>
                    </div>

                    <div class="col-md-1">
                        
                    </div>

                    <div class="col-md-5">
                        <div class="form-group">
                            <label><b>Part V</b></label>
                            <select class="form-control" name="id_part[]">
                                <option value="0"> Chọn Part V</option>

                                @foreach($part_5 as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach    
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <label><b>Part VI</b></label>
                            <select class="form-control" name="id_part[]">
                                <option value="0"> Chọn Part VI</option>

                                @foreach($part_6 as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach    
                            </select>
                        </div>
                        
                        <div class="form-group">
                            <label><b>Part VII</b></label>
                            <select class="form-control" name="id_part[]">
                                <option value="0"> Chọn Part VII</option>

                                @foreach($part_7 as $item)
                                    <option value="{{$item->id}}">{{$item->name}}</option>
                                @endforeach    
                            </select>
                        </div>
                    </div>
                    
                </div>

                
            </div>
           
        </div>
    </form>

        <!--/.row-->
    </div>

    <!--end main-->

@endsection

@section('script')
    <script src="vendors/jQuery-Smart-Wizard/js/jquery.smartWizard.js"></script>
@endsection
