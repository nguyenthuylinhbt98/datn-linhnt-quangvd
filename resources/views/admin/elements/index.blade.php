@extends('admin.master')
@section('admin', 'class=active')

@section('content')
<!--main-->
	<div class="container main">
		
		<!-- top tiles -->
          <div class="row tile_count">
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Thành viên</span>
              <div class="count">{{ count($users) }}</div>
              <span class="count_bottom"><i class="green">4% </i> From last Week</span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-clock-o"></i> Toeic</span>
              <div class="count">{{ $toeic }}</div>
              <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>3% </i> From last Week</span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Aptis</span>
              <div class="count green">{{ $aptis }}</div>
              <span class="count_bottom"><i class="green"><i class="fa fa-sort-asc"></i>34% </i> From last Week</span>
            </div>
            <div class="col-md-2 col-sm-4 col-xs-6 tile_stats_count">
              <span class="count_top"><i class="fa fa-user"></i> Class</span>
              <div class="count">{{ $class }}</div>
              <span class="count_bottom"><i class="red"><i class="fa fa-sort-desc"></i>12% </i> From last Week</span>
            </div>
            
          </div>
          <!-- /top tiles -->
          <div class="row">
            <div  class="col-xs-12 col-md-12 col-lg-12" >
              <canvas id="myChart" style="max-height: 400px"></canvas>
            </div>
            <div class="col-xs-12 col-md-12 col-lg-12">
                <h2>Toeic exam statistics ({{ count($read_point) }})  </h2>
                <div class="row">
                  <div class="col-xs-12 col-md-12 col-lg-6">
                    <div align='center'>
                      <ul class="pagination" style="margin: 0">
                          {{$user_toeic->links()}}
                      </ul>
                      </div>
                      <table class="table table-bordered">
                          <thead>
                              <tr class="bg-primary">
                                  <th>ID</th>
                                  <th>User</th>
                                  <th>Listening</th>
                                  <th>Reading</th>
                                  <th>Total</th>
                              </tr>
                          </thead>
                          <tbody>
                              @foreach($user_toeic as $item)
                                  <tr>
                                      <td>{{$item->id}}</td>
                                      <td>{{$item->user}}</td>
                                      <td>{{$item->listen}}</td>
                                      <td>{{$item->read}}</td>
                                      <td><b>{{$item->total}}</b></td>
                                  </tr>
                              @endforeach
                          </tbody>
                        </table>
                  </div>
                  <div class="col-xs-12 col-md-12 col-lg-6">
                          <canvas id="chartToeic" style="max-height: 400px"></canvas>
                  </div>
                   <div class="col-xs-12 col-md-12 col-lg-12">
                    <canvas id="chartToeic1" style="max-height: 400px"></canvas>
                  </div>
                </div>
            </div>
            {{-- <div class="col-xs-12 col-md-12 col-lg-12">
              <h2>Aptis exam statistics</h2>
              <div class="row">
                <div class="col-xs-6 col-md-6 col-lg-6">
                  <div align='center'>
                    <ul class="pagination" style="margin: 0">
                        {{$user_toeic->links()}}
                    </ul>
                    </div>
                    <table class="table table-bordered">
                        <thead>
                            <tr class="bg-primary">
                                <th>ID</th>
                                <th>User</th>
                                <th>Listening</th>
                                <th>Reading</th>
                                <th>Total</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($user_toeic as $item)
                                <tr>
                                    <td>{{$item->id}}</td>
                                    <td>{{$item->user}}</td>
                                    <td>{{$item->listen}}</td>
                                    <td>{{$item->read}}</td>
                                    <td>{{$item->total}}</td>
                                </tr>
                            @endforeach
                        </tbody>
                      </table>
                </div>
                <div class="col-xs-6 col-md-6 col-lg-6"></div>
              </div>
              
            </div> --}}


          </div>

	</div>
	<!--end main-->

@endsection

@section('script')
  <script src="https://cdn.jsdelivr.net/npm/chart.js"></script>
  <script>
    var ctx = document.getElementById('myChart').getContext('2d');
    var myChart = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: [
                  @foreach($chart_user as $key=>$value)
                      "M{{$key}} ({{ $value }})",
                  @endforeach
            ],
            datasets: [{
                label: 'Number of users over time',
                data: [
                  @foreach($chart_user as $value)
                      {{$value}},
                  @endforeach
                ],
                backgroundColor: [
                    'rgba(255, 99, 13, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)',
                    'rgba(255, 99, 13, 0.2)',
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)',
                    'rgba(255, 99, 132, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });

    var ctx2 = document.getElementById('chartToeic').getContext('2d');
    var char2 = new Chart(ctx2, {
      type: 'polarArea',
      data: {

          labels: [
              @foreach($chart_toeic as $key=>$value)
                  "{{$key}} ({{ $value }})",
              @endforeach
          ],
          datasets: [{
              label: 'Grading of test scores',
              fillColor : "rgba(48, 164, 255, 0.2)",
              strokeColor : "rgba(48, 164, 255, 1)",
              pointColor : "rgba(48, 164, 255, 1)",
              data: [
                @foreach($chart_toeic as $value)
                  {{$value}},
                @endforeach
              ],
              backgroundColor: [
                  'rgba(75, 192, 192, 0.2)',
                  'rgba(255, 237, 158, 1)',
                  'rgba(255, 158, 158, 1)',
                  'rgba(158, 255, 168, 1)',
                  'rgba(158, 255, 218, 1)',
                  'rgba(158, 213, 255, 1)'
              ],
              borderColor: [
                  'rgba(153, 102, 255, 1)',
                  'rgba(153, 102, 255, 1)',
                  'rgba(153, 102, 255, 1)',
                  'rgba(153, 102, 255, 1)',
                  'rgba(153, 102, 255, 1)',
              ],
              borderWidth: 1
          }]
      },
      options: {
          scales: {
              yAxes: [{
                  ticks: {
                      beginAtZero: true
                  }
              }]
          }
      }
    });

   var canvas = document.getElementById('chartToeic1');
    new Chart(canvas, {
      type: 'line',
      data: {
        labels: [
            @foreach($listen_point as $key=>$value)
                "{{$key}}",
            @endforeach
        ],
        datasets: [{
          label: 'Listening',
          yAxisID: 'y',
          borderColor:'rgba(255, 99, 132, 1)',
                    
          backgroundColor:'rgba(255, 99, 13, 0.2)',
                    
          data: [
              @foreach($listen_point as $key=>$value)
                  {{ $value }},
              @endforeach
          ]
        }, {
          label: 'Reading',
          yAxisID: 'y1',
          borderColor:'rgba(54, 162, 235, 1)',
          backgroundColor:'rgba(54, 162, 235, 0.2)',
          data: [
            @foreach($read_point as $key=>$value)
                  {{ $value }},
              @endforeach
          ]
        }]
      },
      
    });
  </script>
@endsection