<!DOCTYPE html>
<html lang="en">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <!-- Meta, title, CSS, favicons, etc. -->
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="icon" href="images/favicon.ico" type="image/ico" />

    <title>@yield('title') </title>
    <base href="{{ asset('admin_assets')}}/">

    <!-- Bootstrap -->
    <link href="vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="build/css/custom.min.css" rel="stylesheet">
    <link href="build/css/style.css" rel="stylesheet">
    @yield('css')

  </head>
  <body class="nav-md">
    <div class="container body">
      <div class="main_container">


        @include('admin.shared.sidebar')


        <!-- top navigation -->
       @include('admin.shared.header')
        <!-- /top navigation -->

        <!-- page content -->

        <div class="right_col" role="main" style="background: white;">
            
                 @yield('content')

        </div>


        <!-- /page content -->

        <!-- footer content -->
        @include('admin.shared.footer')
        <!-- /footer content -->


      </div>
    </div>

    <!-- jQuery -->
    {{-- <script src="vendors/jquery/dist/jquery.min.js"></script> --}}
    <script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
    <!-- Bootstrap -->
    <script src="vendors/bootstrap/dist/js/bootstrap.min.js"></script>
   
    <!-- Chart.js -->
    <script src="vendors/Chart.js/dist/Chart.min.js"></script>
  
    <!-- bootstrap-progressbar -->
    <script src="vendors/bootstrap-progressbar/bootstrap-progressbar.min.js"></script>
    <!-- iCheck -->
    <script src="vendors/iCheck/icheck.min.js"></script>


    <!-- Custom Theme Scripts -->
    <script src="build/js/custom.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/numeral.js/2.0.6/numeral.min.js"></script>

    
    
    @yield('script')
    
  </body>
</html>
