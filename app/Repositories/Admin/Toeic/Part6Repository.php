<?php

namespace App\Repositories\Admin\Toeic;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Models\Toeic\Part6;

//use Your Model

/**
 * Class Part6Repository.
 */
class Part6Repository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return Part6::class;
    }

    public function createPart6(array $data)
    {
        $part_6['name'] = $data['name'];
        $part_6['num_para'] = $data['num_para'];
        $part_6['begin'] = $data['begin'];
        $part_6['end'] = $data['end'];
        // dd($data);
        for ($i=0; $i<5; $i++) {
            if(!empty($data['paragraph_'.$i])){
                $part_6['paragraph_'.$i]=implode('; ;',(array)$data['paragraph_'.$i]);
            }
        };

        for ($i=0; $i<20; $i++){
            if(!empty($data['question_'.$i])){
                $part_6['question_'.$i]=implode('; ;',(array)$data['question_'.$i]);
            }
        }
        // dd($part_6);
        Part6::create($part_6);        
        return true;
    }
    // ---

    public function editPart6(int $id)
    {
        $part_6 = self::getById($id);  
        for ($i=0; $i<5; $i++) {
            (array)$part_6['paragraph_'.$i]=explode('; ;',$part_6->{'paragraph_'.$i});
        };
        
        for ($i=0; $i<20; $i++){
            (array)$part_6['question_'.$i]=explode('; ;',$part_6->{'question_'.$i});
        }
        
        return $part_6;
    }

    //-------------
    

    public function updatePart6(array $data, int $id){
        $part6 = self::getById($id);

        $part_6['name'] = $data['name'];
        $part_6['num_para'] = $data['num_para'];
        $part_6['begin'] = $data['begin'];
        $part_6['end'] = $data['end'];
        // dd($data);
        for ($i=0; $i<5; $i++) {
            if(!empty($data['paragraph_'.$i])){
                $part_6['paragraph_'.$i]=implode('; ;',(array)$data['paragraph_'.$i]);
            }
        };

        for ($i=0; $i<20; $i++){
            if(!empty($data['question_'.$i])){
                $part_6['question_'.$i]=implode('; ;',(array)$data['question_'.$i]);
            }
        }
        $part6->update($part_6);        
        return true;
    } 
}
