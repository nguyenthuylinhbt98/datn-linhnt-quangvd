<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Notifications\ResetPassword;
use App\User;
use Str;
use Hash;

class ResetPasswordController extends Controller
{
    public function forgotPassword(){
        dd(123);
        return view('user.form.reset_password');
    }

    public function sendMail(Request $r){
        $user = User::where("email", $r->email)->first();
        if($user){
            $token = Str::random(60);
            $user["remember_token"] = $token;
            $user->update();
            $user->notify(new ResetPasswordRequest($token));
            return redirect("/login")->with("success", "Please check your email to reset password!!");
        }
        return redirect()->back()->with("erros", "Email does not exists!!");
    }    
}
