<?php

namespace App\Http\Middleware;

use Closure;
use Auth;

class isAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if (!Auth::check()) {
            return redirect()->back()->with('thongbao', 'Vui lòng đăng nhập để tiếp tục!!!');
        }

        if (!(Auth::user()->level == 1 || Auth::user()->level == 2)) {
            return redirect('/login')->with('thongbao', 'Bạn không có quyền truy cập tác vụ này!!!');
        }

        return $next($request);
    }
}
