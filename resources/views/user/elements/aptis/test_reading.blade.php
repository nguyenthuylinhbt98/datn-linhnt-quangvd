@extends('user.master')

@section('css')
	<link rel="stylesheet" href="assets/css/test.css">
    <style>
        label{
            float: left;
        }
        select{
            max-width: 90%;
        }
    </style>
@endsection


@section('content')
    
<main class="aptis" id="aptis-reading">
    <!--? slider Area Start-->
    <section class="slider-area slider-area2">
        <div class="slider-active">
            <!-- Single Slider -->
            <div class="single-slider slider-height2">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-8 col-lg-11 col-md-12">
                            <div class="hero__caption hero__caption2">
                                <h1 data-animation="bounceIn" data-delay="0.2s">{{$name}}</h1>
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                                        <li class="breadcrumb-item"><a href="/aptis">Aptis</a></li>
                                        <li class="breadcrumb-item"><a href="/aptis/test/{{ $id }}">{{$name}}</a></li>
                                        <li class="breadcrumb-item"><a>Reading</a></li>
                                    </ol>
                                </nav>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class=" test">
       <div class="container">
        <div class="row justify-content-center">
            <div class="col-xl-9 col-lg-10">
                <div class="section-tittle  mb-10 mt-20">
                   <p>
                        Welcome to the Aptis Reading Test. <br>
                        The test has four parts. <br>
                        You have 35 minutes to complete the test. <br>
                   </p>
                </div>
            </div>
            <div class="col-xl-3 col-lg-2">
                <div class="time-ans">
                    <i class="far fa-clock"></i>
                    0<span id="h">0</span> :
                    <span id="m">00</span> :
                    <span id="s">00</span>
                </div>
            </div>
            <div class="col-xl-12 col-lg-12">
                <form action="" method="POST">
                    @csrf
                    <input type="hidden" value="{{$read->id}}" name="listen_id">
                    <ul class="nav nav-pills nav-justified" id="pills-tab" role="tablist">
                        <li class="nav-item">
                            <a class="nav-link active" id="pills-home-tab" data-toggle="pill" href="#part1" role="tab" aria-controls="pills-home" aria-selected="true">Part I</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-profile-tab" data-toggle="pill" href="#part2" role="tab" aria-controls="pills-profile" aria-selected="false">Part II</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#part3" role="tab" aria-controls="pills-contact" aria-selected="false">Part III</a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#part4" role="tab" aria-controls="pills-contact" aria-selected="false">Part IV</a>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <hr>
                        <div class="tab-pane fade show active" id="part1" role="tabpanel" aria-labelledby="pills-home-tab">
                            <p>Choose one word from the list for each gap. The first one is done for you.</p>
                            <div class="row">
                                <div class="col-md-12 col-lg-12" style="margin-bottom: 20px">
                                    <p>{!! $read->p1_para !!}</p>
                                    <label for="ques1">Example: 0. </label>
                                    <select name="" id="" >
                                        <option value="">{{ $read->example }}</option>
                                    </select>
                                </div>
                                @for($i=1; $i< 8; $i++)
                                    @if ( !empty($read->{'p1_ques'.$i}) )
                                        <div class="col-md-3 col-lg-3 col-sm-3 d-flex">
                                            <label for="ques1"><b>{{$i}}.</b></label>
                                            <select name="answer_reading_part1[]" id="">
                                                @foreach ($read->{'p1_ques'.$i} as $item)
                                                    <option value="{{ $item }}">{{ $item }}</option>
                                                @endforeach
                                            </select>
                                        </div>
                                        <hr>
                                    @endif
                                @endfor
                            </div>
                        </div>
                        <div class="tab-pane fade show" id="part2" role="tabpanel" aria-labelledby="pills-home-tab">
                            <p>The sentences below are from a report. Put the sentences in the right order. The first sentence is done for you.</p>
                            @for ($i = 1; $i < 3; $i++)
                                <div class="question-reading question-{{$i}} {{ $i==1 ? 'block' : ''}}">
                                    <b>Paragraph {{ $i }}</b> <br>
                                    {{ $read->{'p2_ques'.$i}[0] }} <br>
                                    {!! $read->{'p2_ques'.$i}[1] !!} <br>
                                    <label for="answer_part2"><b> Your Answer:</b></label>
                                    <input name="answer_reading_part2[]" id="answer_part2" class="form-control" type="text" placeholder="answer" style="padding: 20px; font-size: 20px">
                                    <small>Eg: 1,2,3,4,5,6,...</small>
                                    <div  class="d-flex justify-content-center mb-3 btn-paginate">
                                        @if($i==1) <button type="button" class="btn btn-primary" onclick="showQuestion({{$i+1}})">Next</button>  @endif
                                        @if ($i==2) <button type="button" class="btn btn-primary" onclick="showQuestion({{$i-1}})">Pre</button> @endif
                                    </div>
                                    
                                </div>
                            @endfor
                        </div>
                        <div class="tab-pane fade show" id="part3" role="tabpanel" aria-labelledby="pills-home-tab">
                            <div class="row">
                                <div class="col-md-7">
                                    {!! $read->p3_para !!}
                                </div>
                                <div class="col-md-5">
                                    <p>Four people respond in the comments section of an online magazine article about education and work. Read their comments and answer the questions below.</p>
                                    @for ($i = 1; $i < 9; $i++)
                                        <div class="p3_ques d-flex" style="margin-bottom: 5px">
                                            <label for=""><b>{{ $i }}.</b></label>
                                            <select name="answer_reading_part3[]" id="">
                                                @for($j=1; $j<5; $j++)
                                                    <option value="{{ $read->{'p3_ans'}[$j-1] }}">{{ $read->{'p3_ans'}[$j-1] }}</option>
                                                @endfor
                                            </select>
                                        </div>
                                    @endfor
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane fade show" id="part4" role="tabpanel" aria-labelledby="pills-home-tab">
                            <div class="row">
                                <div class="col-md-7">
                                    {{ $read->p4_title }} <br>
                                    {!! $read->p4_para !!}
                                </div>
                                <div class="col-md-5">
                                    <p>Read the passage quickly. Choose a heading for each numbered paragraph (1-7) from the drop-down box. There is one more heading than you need.</p>
                                    @for ($j = 1; $j < 8; $j++)
                                        <div class="p4_ques d-flex" style="margin-bottom: 5px">
                                            <label for=""><b>{{ $j }}.</b></label>
                                            <select name="answer_reading_part4[]" id="">
                                                @for($i=1; $i<10; $i++)
                                                    @if( !empty($read->p4_ans[$i-1]) )
                                                        <option value="{{ $i }}">{{ $read->p4_ans[$i-1] }}</option>
                                                    @endif
                                                @endfor
                                            </select>
                                        </div>
                                    @endfor
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="sub d-flex justify-content-center mb-3">
                        <input type="submit" class="genric-btn primary-border circle" value="Submit"/>
                    </div>
                </form>
              
            </div>
        </div>
       </div>
    </section>
</main>

@endsection


@section('script')
<script>
    var h = 00; // Giờ
    var m = 35; // Phút
    var s = 01; // Giây
    var timeout = null;

    function start()
    {
        if (s === -1){
            m -= 1;
            s = 59;
        }

        if (m === -1){
            h -= 1;
            m = 59;
        }

        if (h == -1){
            clearTimeout(timeout);
            document.getElementById("testToeic").submit();
            return false;
        }

        document.getElementById('h').innerText = h.toString();
        document.getElementById('m').innerText = m.toString();
        document.getElementById('s').innerText = s.toString();

        timeout = setTimeout(function(){
            s--;
            start();
        }, 1000);
    }
    start();

    function activeAns(id){
        $(`.ques-${id}`).addClass('active')
    }
    
    function showQuestion(id){
        $(`.question-reading`).removeClass('block');
        $(`.question-${id}`).addClass('block');

    }
</script>
@endsection
