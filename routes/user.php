<?php

    Route::get('/', 'HomeController@getIndex');
    Route::get('/about', 'AboutController@getAbout');
    Route::get('/contact', 'ContactController@getContact');
    // Blog
    Route::get('/q&a', 'BlogController@getBlog');
    Route::get('q&a/detail/{id}', 'BlogController@getDetail');

    // Login
    Route::get('/singin', 'UserController@getLogin');
    Route::post('/singin', 'UserController@postLogin');

    // Register
    Route::get('/singup', 'UserController@getRegister');
    Route::post('/singup', 'UserController@postRegister');

    // Logout
    Route::get('/logout', 'UserController@getLogout');

    // Forgot Password
    Route::get('/forgot-password', 'ResetPasswordController@forgotPassword');

    // Toeic
    Route::group(['prefix' => 'toeic', 'namespace'=>'Toeic'], function () {
        Route::get('/', 'ToeicController@getList');
        Route::get('/test/{id}', 'ToeicController@getTest')->middleware('isUser');
        Route::post('/test/{id}', 'ToeicController@postTest')->middleware('isUser');
    });

    // Aptis
    Route::group(['prefix' => 'aptis', 'namespace'=>'Aptis'], function () {
        Route::get('/', 'AptisController@getList');
        Route::get('/test/{id}', 'AptisController@getTest')->middleware('isUser');
        Route::get('/test/{id}/listening', 'AptisController@getListening')->middleware('isUser');
        Route::post('/test/{id}/listening', 'AptisController@postListening')->middleware('isUser');
        Route::get('/test/{id}/reading', 'AptisController@getReading')->middleware('isUser');
        Route::post('/test/{id}/reading', 'AptisController@postReading')->middleware('isUser');
        Route::get('/test/{id}/writing', 'AptisController@getWriting')->middleware('isUser');
        Route::post('/test/{id}/writing', 'AptisController@postWriting')->middleware('isUser');
        Route::get('/test/{id}/grammar', 'AptisController@getGrammar')->middleware('isUser');
        Route::post('/test/{id}/grammar', 'AptisController@postGrammar')->middleware('isUser');
        Route::get('/test/{id}/speaking', 'AptisController@getSpeaking')->middleware('isUser');
        Route::post('/test/{id}/speaking', 'AptisController@postSpeaking')->middleware('isUser');
        // Check results
        Route::get('/test/{id}/results', 'AptisController@checkResults')->middleware('isUser');
        
    });

    // Profile
    Route::middleware('isUser')->group(function () {
        Route::get('/profile/{id}', 'ProfileController@getProfile');
        Route::get('/profile/{id}/edit', 'ProfileController@getEditProfile');
        Route::post('/profile/{id}/edit', 'ProfileController@postEditProfile');
    }); 

    // Teacher
    Route::get('/assign-to-be-a-teacher', 'ClassController@assignTeacher');
    Route::group(['prefix' => 'for-teacher'], function () {
        Route::get('/', 'ClassController@index');
        Route::post('/', 'ClassController@createClass');
         // Class
        Route::group(['prefix' => 'class'], function () {
            Route::get('/', 'ClassroomController@index');
            Route::get('/{id}', 'ClassroomController@getClass');
            Route::post('/{id}/join', 'ClassroomController@joinClass');
            Route::get('/{class_id}/approval/{user_id}', 'ClassroomController@approvalMember');
            Route::get('/{class_id}/ban/{user_id}', 'ClassroomController@banMember');
        });
    });

    // Note
    Route::post('note', 'HomeController@saveNote');

   