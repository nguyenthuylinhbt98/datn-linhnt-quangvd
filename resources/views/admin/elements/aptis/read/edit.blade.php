@extends('admin.master')
@section('content')
<!--main-->
<div class="container main exam">
    <div class="row">
        <div class="col-lg-12">
            <h1 class="page-header">Edit reading test</h1>
        </div>
    </div> 
   @if(session('thongbao'))
       <div class="alert alert-success">
            {{ session('thongbao') }}
        </div>

    @endif
    <!--/.row-->
    <form action="/admin/aptis/reading/{{$read->id}}" method="post" accept-charset="utf-8" enctype="multipart/form-data">
        @csrf
        <input type="hidden" name="_method" value="put">
        <div class="row">
            <div class="col-xs-12 col-md-12 col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading"><i class="fas fa-user"></i>Add reading test</div>
                    <div class="panel-body">
                        <div class="row justify-content-center" style="margin-bottom:40px">
                            <div class="col-md-8 col-lg-8 col-lg-offset-2">
                                <div class="form-group">
                                    <label>Name</label>
                                    <input value="{{ $read->name }}" type="text" name="name" placeholder="Name test" class="form-control" required>
                                    {{ ShowErrors($errors, 'name')}}
                                </div>
                                <div class="container">
                                    <h2>Exam </h2>
                                    <ul class="nav nav-tabs">
                                        <li class="active"><a data-toggle="tab" href="#part1">Part I</a></li>
                                        <li><a data-toggle="tab" href="#part2">Part II</a></li>
                                        <li><a data-toggle="tab" href="#part3">Part III</a></li>
                                        <li><a data-toggle="tab" href="#part4">Part IV</a></li>
                                        
                                    </ul>
                                    <div class="tab-content">
                                        <div id="part1" class="tab-pane fade in active">
                                            <textarea name="p1_para" class="ckeditor" >{!! $read->p1_para !!}</textarea>
                                            Example: <input class="form-control" type="text" name="example" value="{{ $read->example }}">
                                            @for($i=1; $i<9; $i++)
                                                <div class="col-md-3">
                                                    <label for="ques1">{{$i}}.</label>
                                                    @for($j=0; $j<4; $j++)
                                                        @if(isset($read->{'p1_ques'.$i}[$j]))
                                                            <input class="form-control" type="text" name="p1_ques{{$i}}[]" value="{{$read->{'p1_ques'.$i}[$j]}}"><br>
                                                        @else
                                                            <input class="form-control" type="text" name="p1_ques{{$i}}[]"><br>

                                                        @endif
                                                    @endfor
                                                </div>
                                            @endfor
                                        </div>    
                                        <div id="part2" class="tab-pane fade ">
                                            @for($i=1; $i<3; $i++)
                                                <p>{{$i}}.</p>
                                                <input class="form-control" type="text" name="p2_ques{{$i}}[]" placeholder="title" value="{{$read->{'p2_ques'.$i}[0]}}"><br>
                                                <textarea name="p2_ques{{$i}}[]" class="ckeditor" >{{$read->{'p2_ques'.$i}[1]}}</textarea>
                                                <input class="form-control" type="text" name="p2_ques{{$i}}[]" placeholder="Answer" value="{{$read->{'p2_ques'.$i}[2]}}"> <br>
                                            @endfor
                                            
                                        </div>                                       
                                        <div id="part3" class="tab-pane fade ">
                                            <textarea name="p3_para" class="ckeditor" >{{ $read->p3_para }}</textarea><br>
                                            <input class="form-control" type="text" name="p3_title" value="{{ $read->p3_title }}">
                                            <p>List Answer:</p>
                                            @for($i=1; $i<5; $i++)
                                                @if(isset($read->{'p3_ans'}[$i-1]))
                                                    <input class="form-control" type="text" name="p3_ans[]" value="{{$read->{'p3_ans'}[$i-1]}}"><br>
                                                @else 
                                                    <input class="form-control" type="text" name="p3_ans[]"><br>
                                                @endif
                                            @endfor
                                            <p>Ques_Answer:</p>
                                            <table width="100%">
                                                @for($i=0; $i<18; $i+=2)
                                                    <tr>
                                                        <td>
                                                            <input class="form-control" type="text" name="p3_ques[]" value="{{ $read->p3_ques[$i] }}">
                                                        </td>
                                                        <td>
                                                            <input class="form-control" type="text" name="p3_ques[]" value="{{ $read->p3_ques[$i+1] }}">
                                                        </td>
                                                    </tr>
                                                @endfor
                                            </table>
                                            
                                        </div>                                       
                                        <div id="part4" class="tab-pane fade ">
                                            <textarea name="p4_para" class="ckeditor" >{{ $read->p4_para }}</textarea><br>
                                            <input class="form-control" type="text" name="p4_title" value="{{$read->p4_title}}">                                           
                                            <p>List Answer:</p>
                                            @for($i=1; $i<10; $i++)
                                                <div class="col-md-4">
                                                    {{$i}}.<input class="form-control" type="text" name="p4_ans[]" value="{{ $read->p4_ans[$i-1] }}"><br>
                                                </div>
                                            @endfor
                                            <br> Correct answer
                                            <input class="form-control" type="text" name="p4_ans[]" value="{{$read->p4_ans[9]}}">
                                           
                                            
                                        </div>                                       
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-8 col-lg-8 col-lg-offset-2 text-right"  style="margin-top: 20px;">
                                    <button class="btn btn-success"  type="submit">Cập nhật đề</button>
                                    <button class="btn btn-danger" type="reset">Huỷ bỏ</button>
                                </div>
                            </div>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                </div>
            </div>
        </div>
    </form>
    <!--/.row-->
</div>
<!--end main-->
@endsection

@section('script')
    <script src="./../ckeditor/ckeditor.js"></script>
@endsection