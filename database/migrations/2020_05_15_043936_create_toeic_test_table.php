<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateToeicTestTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('toeic_test', function (Blueprint $table) {
            $table->id();
            $table->string('name_test');
            $table->string('level');
            $table->string('role');
            $table->string('classify');
            $table->string('id_part');
            $table->integer('view')->default(0);
            $table->integer('test')->default(0);
            $table->longText('description')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('toeic_test');
    }
}
