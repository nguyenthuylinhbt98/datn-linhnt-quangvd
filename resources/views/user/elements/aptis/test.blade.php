@extends('user.master')

@section('css')
	<link rel="stylesheet" href="assets/css/test.css">
@endsection


@section('content')
    
<main class="aptis">
    <!--? slider Area Start-->
    <section class="slider-area slider-area2">
        <div class="slider-active">
            <!-- Single Slider -->
            <div class="single-slider slider-height2">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-8 col-lg-11 col-md-12">
                            <div class="hero__caption hero__caption2">
                                <h1 data-animation="bounceIn" data-delay="0.2s">{{$test->name_test}}</h1>
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                                        <li class="breadcrumb-item"><a href="/aptis">Aptis</a></li>
                                        <li class="breadcrumb-item"><a href="/#">Test</a></li>
                                    </ol>
                                </nav>
                            </div>
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class=" test">
       <div class="container">
        <div class="row justify-content-center">
            {{-- @dd(!\Session::get('aptis_complete')) --}}
            @if( !\Session::get('aptis_complete') )
                <div class="col-xl-7 col-lg-8">
                    <div class="section-tittle text-center mb-10 mt-20">
                        <h2>Select the exam part</h2>
                    </div>
                </div>
                <div class="col-xl-7 col-lg-8">
                    @php
                        $session = \Session::get('aptis')
                    @endphp
                    <div class="section-tittle text-center mb-55">
                        @if( empty($session['listening_answer']) )  <a class="btn btn-dark btn-lg" href="/aptis/test/{{$test->id}}/listening">Start Listening </a> <br> @endif
                        @if( empty($session['reading_answer']) )  <a class="btn btn-dark btn-lg" href="/aptis/test/{{$test->id}}/reading">Start Reading</a> <br> @endif
                        @if( empty($session['writing_answer']) )  <a class="btn btn-dark btn-lg" href="/aptis/test/{{$test->id}}/writing">Start Writing </a> <br> @endif
                        @if( empty($session['grammar_answer']) )  <a class="btn btn-dark btn-lg" href="/aptis/test/{{$test->id}}/grammar">Start Grammar & Vocabulary</a> <br> @endif
                        @if( empty($session['speaking_answer']) )  <a class="btn btn-dark btn-lg" href="/aptis/test/{{$test->id}}/speaking">Start Speaking </a> @endif
                    </div>
                </div>
            @else
                <div class="col-xl-7 col-lg-8">
                    <div class="section-tittle text-center mb-10 mt-20">
                        <h2>Congratulations on completing the aptis exam</h2>
                        <a class="border-btn border-btn2" href="/aptis/test/{{$test->id}}/results">Check results</a>
                    </div>
                </div>
            @endif
        </div>
       </div>
    </section>
</main>

@endsection


