<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->delete();
        DB::table('users')->insert([
            ['id'=>1,
            'email'=>'nguyenthuylinhbt98@gmail.com',
            'password'=>bcrypt('123'),
            'full'=>'Linh NT',
            'phone'=>'0339164022',
            'level'=>1],

            ['id'=>2,
            'email'=>'quang.vd@gmail.com',
            'password'=>bcrypt('123'),
            'full'=>'Quang VD',
            'phone'=>'0339281818',
            'level'=>1],
            
            ['id'=>3,
            'email'=>'user1@gmail.com',
            'password'=>bcrypt('123'),
            'full'=>'User demo',
            'phone'=>'0339281818',
            'level'=>0],
            
            ['id'=>4,
            'email'=>'user2@gmail.com',
            'password'=>bcrypt('123'),
            'full'=>'User demo',
            'phone'=>'0339281818',
            'level'=>0],
            
            ['id'=>5,
            'email'=>'user3@gmail.com',
            'password'=>bcrypt('123'),
            'full'=>'User demo',
            'phone'=>'0339281818',
            'level'=>0],

            
            ['id'=>6,
            'email'=>'user4@gmail.com',
            'password'=>bcrypt('123'),
            'full'=>'User demo',
            'phone'=>'0339281818',
            'level'=>0],
            
        ]);
    }
}
