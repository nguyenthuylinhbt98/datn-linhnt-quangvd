<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateToeicPart5Table extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('toeic_part5', function (Blueprint $table) {
            $table->id();
            $table->string('name', 191);
            $table->integer('end');

            for($i=0; $i<40; $i++){
                $table->longText('question_'.$i)->nullable();
            };
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('toeic_part5');
    }
}
