<?php

namespace App\Http\Controllers\User\Aptis;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Aptis\{Test, Listen, GV, Read, Speak, Write};
use App\Models\Users\User_And_Aptis;
use App\Repositories\Admin\Aptis\TestRepository;
use App\Repositories\Admin\Aptis\ListenRepository;
use App\Repositories\Admin\Aptis\ReadRepository;
use App\Repositories\Admin\Aptis\GVRepository;
use App\Repositories\Admin\Aptis\SpeakRepository;
use App\Repositories\Admin\Aptis\WriteRepository;
use Auth;
use App\Models\Teacher;
use Session;

class AptisController extends Controller
{
    private $testRepository;
    private $listenRepository;
    private $readRepository;
    private $speakRepository;
    private $writeRepository;
    private $gvRepository;

    public function __construct()
    {
        $this->testRepository = new TestRepository();
        $this->listenRepository = new ListenRepository();
        $this->readRepository = new ReadRepository();
        $this->speakRepository = new SpeakRepository();
        $this->gvRepository = new GVRepository();
        $this->writeRepository = new WriteRepository();
    }

    public function getList(){
        $data['class'] = Teacher::where('course', 'Toeic')->get();
        $data['test'] = Test::orderBy('id', 'ASC')->paginate(6);
        return view('user.elements.aptis.list', $data);
    }

    public function getTest($id){
        \Session::put('aptis_complete', false);
        $test = Test::find($id);
        $test->view = $test['view'] + 1;
        $test->save();
        $data = \Session::get('aptis');
        if( !empty($data['writing_answer']) && !empty($data['listening_answer']) && !empty($data['speaking_answer']) && !empty($data['grammar_answer']) &&  !empty($data['reading_answer'])){
            $data['user_id'] = Auth::user()->id;
            $data['aptis_id'] = $id;
            User_And_Aptis::create($data);
            \Session::forget('aptis');
            \Session::put('aptis_complete', true);
        }
        return view('user.elements.aptis.test', compact('test'));
    }

    public function getListening($id){
        $test = Test::find($id);
        $data['id'] = $id;
        $data['name'] = $test->name_test;
        $data['listen'] = $this->listenRepository->edit($test->listen_id); 
        return view('user.elements.aptis.test_listening', $data);
    }

    public function postListening(Request $r, $id_aptis){
        $ans = [];
        for($i=0; $i<25; $i++){
            array_push($ans,  $r->{'answer_listening_question_'.$i});
        }
        $data = \Session::get('aptis');
        $data['listening_answer']=implode('; ;',(array)$ans);
        \Session::put('aptis', $data);
        return redirect('/aptis/test/'.$id_aptis)->with('thongbao', 'Success edit Grammar & Vocabulary test');
    }

    public function getReading($id){
        $test = Test::find($id);
        $data['id'] = $id;
        $data['name'] = $test->name_test;
        $data['read'] = $this->readRepository->editRead($test->listen_id); 
        return view('user.elements.aptis.test_reading', $data);
    }

    public function postReading(Request $r, $id_aptis){
        $ans = $r->all();
        for($i=1; $i<5; $i++){
            $ans['answer_reading_part'.$i] = implode('; ;',(array)$ans['answer_reading_part'.$i]);
        }
        $data = \Session::get('aptis');
        $data['reading_answer'] = $ans['answer_reading_part1'].'___'.$ans['answer_reading_part2'].'___'.$ans['answer_reading_part3'].'___'.$ans['answer_reading_part4'];
        \Session::put('aptis', $data);
        return redirect('/aptis/test/'.$id_aptis)->with('thongbao', 'Success edit Grammar & Vocabulary test');
    }

    public function getWriting($id){
        $test = Test::find($id);
        $data['id'] = $id;
        $data['name'] = $test->name_test;
        $data['write'] = $this->writeRepository->editWrite($test->write_id); 
        return view('user.elements.aptis.test_writing', $data);
    }

    public function postWriting(Request $r, $id_aptis)
    {
        $data =  \Session::get('aptis');
        $data['writing_answer'] = implode("; ;", (array)$r->answer_writing);
        \Session::put('aptis', $data);
        return redirect('/aptis/test/'.$id_aptis)->with('thongbao', 'Success edit Grammar & Vocabulary test');
    }

    public function getGrammar($id){
        $test = Test::find($id);
        $data['id'] = $id;
        $data['name'] = $test->name_test;
        $data['gv'] = $this->gvRepository->edit($test->gv_id); 
        return view('user.elements.aptis.test_grammar', $data);
    }

    public function postGrammar(Request $r, $id_aptis)
    {
        $data =  \Session::get('aptis');
        $ans = $r->all();
        for($i=26; $i<31; $i++){
            $ans['answer_gv_question_'.$i] = implode("__", (array) $ans['answer_gv_question_'.$i]);
        }
        for($i=1; $i<31; $i++){

            $data['grammar_answer'] = implode("; ;", (array)$ans['answer_gv_question_'.$i]);
        }
        \Session::put('aptis', $data);
        return redirect('/aptis/test/'.$id_aptis)->with('thongbao', 'Success edit Grammar & Vocabulary test');
    }

    public function getSpeaking($id){
        $test = Test::find($id);
        $data['id'] = $id;
        $data['name'] = $test->name_test;
        $data['speak'] = $this->speakRepository->editSpeak($test->speak_id); 
        return view('user.elements.aptis.test_speaking', $data);
    }

    public function postSpeaking(Request $r, $id_aptis){
        $data =  \Session::get('aptis');
        $data['speaking_answer'] = "done";
        \Session::put('aptis', $data);
        return redirect('/aptis/test/'.$id_aptis)->with('thongbao', 'Congratulations you have completed the speaking test');
    }

    public function checkResults($id_aptis)
    {
        $results = User_And_Aptis::where('user_id', Auth::user()->id)
                                ->where('aptis_id', $id_aptis)
                                ->get();
        // get user results
        $data['results'] = $results[count($results)-1];
        $data['results']['listening_answer'] = explode('; ;', $data['results']->listening_answer);
        $data['results']['reading_answer'] = explode('___', $data['results']->reading_answer);
        for($i=1; $i<5; $i++){
            $data['results']['reading_answer_p'.$i] = explode('; ;', $data['results']['reading_answer'][$i-1]);
        }
        $data['results']['writing_answer'] = explode('; ;', $data['results']->writing_answer);
        $data['results']['grammar_answer'] = explode('; ;', $data['results']->grammar_answer);

        // dd($data);
        // get correct answer
        $data['listen'] = $this->listenRepository->edit($data['results']->test->listen_id); 
        $data['read'] = $this->readRepository->editRead($data['results']->test->read_id); 
        $data['speak'] = $this->speakRepository->editSpeak($data['results']->test->speak_id); 
        $data['write'] = $this->writeRepository->editWrite($data['results']->test->write_id); 
        $data['gv'] = $this->gvRepository->edit($data['results']->test->gv_id); 

        return view('user.elements.aptis.aptis_results', $data);

    }
}
