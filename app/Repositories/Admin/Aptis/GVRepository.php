<?php

namespace App\Repositories\Admin\Aptis;

use JasonGuru\LaravelMakeRepository\Repository\BaseRepository;
use App\Models\Aptis\GV;

/**
 * Class GVRepository.
 */
class GVRepository extends BaseRepository
{
    /**
     * @return string
     *  Return the model
     */
    public function model()
    {
        return GV::class;
    }

    public function create($data){
        // dd($data);
        for ($i=0; $i <= 25; $i++) {
            if(!empty($data['question_grammar_'.$i])){
                $data['question_grammar_'.$i]=implode('; ;',(array)$data['question_grammar_'.$i]);
            };
        }
        for($i=26; $i<=30; $i++) {
            $data['p'.$i.'_list_answer'] = implode('; ;',(array)$data['p'.$i.'_list_answer']);
            $data['p'.$i.'_list_question'] = implode('; ;',(array)$data['p'.$i.'_list_question']);
            $data['p'.$i.'_list_correct_answer'] = implode('; ;',(array)$data['p'.$i.'_list_correct_answer']);
        }
        GV::create($data);
        return true;
    }

    public function edit($id){
        $gv = self::getById($id);
        $data['name'] = $gv['name'];
        $data['id'] = $gv['id'];
        for ($i=0; $i < 25; $i++) {
            (array)$data['question_grammar_'.$i]=explode('; ;',$gv->{'question_grammar_'.$i});
        }
        for ($i=26; $i <= 30; $i++) {
            (array)$data['p'.$i.'_list_answer']=explode('; ;',$gv->{'p'.$i.'_list_answer'});
            (array)$data['p'.$i.'_list_question']=explode('; ;',$gv->{'p'.$i.'_list_question'});
            (array)$data['p'.$i.'_list_correct_answer']=explode('; ;',$gv->{'p'.$i.'_list_correct_answer'});
        }
        return $data;
    }

    public function update($data, $id){
        $gv = self::getById($id);
        for ($i=0; $i <= 25; $i++) {
            if(!empty($data['question_grammar_'.$i])){
                $data['question_grammar_'.$i]=implode('; ;',(array)$data['question_grammar_'.$i]);
            };
        }
        for($i=26; $i<=30; $i++) {
            $data['p'.$i.'_list_answer'] = implode('; ;',(array)$data['p'.$i.'_list_answer']);
            $data['p'.$i.'_list_question'] = implode('; ;',(array)$data['p'.$i.'_list_question']);
            $data['p'.$i.'_list_correct_answer'] = implode('; ;',(array)$data['p'.$i.'_list_correct_answer']);
        }
        $gv->update($data);
        return true;

    }
}
