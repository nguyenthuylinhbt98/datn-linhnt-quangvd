@extends('user.master')

@section('css')
     <style>
        .blog_left_sidebar img{
             height: 310px;
             object-fit: cover;
             
         }
        .form-group label {
            font-size: 20px;
            font-weight: 700;
            width: 200px;
             padding-top: 8px;
        }
        .form {
            background: url('../../default/create.png')
        }
     </style>
    
@endsection

@section('content')
<main>
    <!--? slider Area Start-->
    <section class="slider-area slider-area2">
        <div class="slider-active">
            <!-- Single Slider -->
            <div class="single-slider slider-height2">
                <div class="container">
                    <div class="row">
                        <div class="col-xl-8 col-lg-11 col-md-12">
                            <div class="hero__caption hero__caption2">
                                <h1 data-animation="bounceIn" data-delay="0.2s">Category for teachers</h1>
                                <!-- breadcrumb Start-->
                                <nav aria-label="breadcrumb">
                                    <ol class="breadcrumb">
                                        <li class="breadcrumb-item"><a href="/">Home</a></li>
                                        <li class="breadcrumb-item"><a>For Teacher</a></li> 
                                    </ol>
                                </nav>
                                <!-- breadcrumb End -->
                            </div>
                        </div>
                    </div>
                </div>          
            </div>
        </div>
    </section>
    <div class="courses-area section-padding40 fix">
        <div class="container">
            <div class="row justify-content-center">
                @if(Auth::user() && Auth::user()->level !=2)
                    <div class="col-xl-12 col-sm-12 col-lg-12 d-flex justify-content-center mb-3">
                        <button data-toggle="modal" data-target="#exampleModalAssign" type="button" class="btn-note btn btn-info">Assign to be a Teacher</button>
                        <div class="modal fade" id="exampleModalAssign" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true" style="margin-top: 30px; font-size: 50px">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                <div class="modal-header">
                                    <h3 class="modal-title" id="exampleModalLabel">Assign to be a teacher</h3>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                    <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body" style="font-size: 20px">
                                    Benefits of registering as a teacher:<br>
                                    - Can participate in creating their own exam questions<br>
                                    - Join the development team for the non-profit community<br>
                                    - ...<br>
                                </div>
                                <div class="modal-footer">
                                    <a href="/assign-to-be-a-teacher" class="btn btn-primary">Join and experience now</a>
                                </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endif

                @if(Auth::user() && Auth::user()->level ==2)
                    <div class="col-xl-4 col-lg-4">
                        <div class="section-tittle text-center mb-55">
                            <h2>Your class <span>*</span></h2>
                        </div>
                        <div class="blog_right_sidebar">
                            <aside class="single_sidebar_widget search_widget">
                                <form action="#">
                                    <div class="form-group">
                                        <div class="input-group mb-3">
                                            <input type="text" class="form-control" placeholder='Search Keyword'
                                            onfocus="this.placeholder = ''"
                                            onblur="this.placeholder = 'Search Keyword'">
                                            <div class="input-group-append">
                                                <button class="btns" type="button"><i class="ti-search"></i></button>
                                            </div>
                                        </div>
                                    </div>
                                    <ul>
                                        @foreach($class as $key=>$item)
                                            <li>
                                                <a style="color: black" href="/for-teacher/class/{{ $item->id }}">{{ $key+1 }}. {{ $item->course }} / {{ $item->time }}</a>
                                            </li>
                                        @endforeach
                                        
                                    </ul>
                                </form>
                            </aside>
                        </div>
                    </div>
                    <div class="col-xl-8 col-lg-8">
                        <div class="section-tittle text-center mb-55">
                            <h2>Sign up to create your own class</h2>
                        </div>
                        <div class="section-tittle text-center mb-55">
                            <form class="form-contact contact_form" action="/for-teacher" method="post">
                                @csrf
                                <div class="row">
                                    <div class="col-sm-12">
                                        <div class="form-group d-flex">
                                            <label>Select course</label>
                                            <select style="width: 100%" class="form-control" name="course">
                                                <option>Select course</option>
                                                <option>Toeic</option>
                                                <option>Aptis</option>
                                                <option>Ilets</option>
                                                <option>Math</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group d-flex">
                                            <label>Select time</label>
                                            <select style="width: 100%" class="form-control" name="time">
                                                <option>Select time</option>
                                                <option>2-4-6</option>
                                                <option>3-5-7</option>
                                                <option>Inform later</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-sm-12">
                                        <div class="form-group d-flex">
                                            <label>Start date</label>
                                            <input class="form-control" name="start_date" id="subject" type="date" onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enter Subject'" placeholder="Enter Subject">
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group mt-3">
                                    <button type="submit" class="button button-contactForm boxed-btn">Create class</button>
                                </div>
                            </form>
                            
                        </div>
                    </div>
                @endif
            </div>
            
        </div>
    </div>
</main>
@endsection

@section('script')
    <script>
        $('.button-contactForm').on('click', function(){
            $('form').submit();
        })
    </script>
@endsection